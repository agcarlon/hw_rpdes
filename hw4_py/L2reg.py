import numpy as np
import matplotlib.pyplot as plt
from numpy.polynomial.legendre import Legendre
from itertools import product
from time import time

from IPython.core.debugger import set_trace


class DiscreteL2():
    def __init__(self, fun, dim, deg, type='TD', distr='uniform',
                 samp_rel='linear', samp_const=1, domain=(0, 1), verb=False):
        self.fun = fun
        self.dim = dim
        self.deg = deg
        self.type = type
        self.distr = distr
        self.samp_const = samp_const
        self.samp_rel = samp_rel
        self.domain = domain
        self.poly = Legendre(np.ones(deg + 1), domain=domain)
        self.verb = verb
        if verb:
            self.print = print
        else:
            self.print = lambda x: None

    def eval(self, samp, likelihood):
        fs = self.fun(samp)
        v = np.zeros(len(samp))
        for i in range(samp.shape[0]):
            v[i] = self.v_compute(samp[i])
        err = np.mean(likelihood * (fs - v) ** 2)
        # rel_err = np.mean(likelihood * (fs - v) ** 2 / fs**2)
        rel_err = err / np.mean(fs**2)
        self.print(f'error: {err}, rel. error: {rel_err}')
        return v, err, rel_err

    def v_compute(self, y):
        v = 0
        for i in range(self.p_dim):
            prod = 1
            for j in range(self.dim):
                prod = prod * self.Legendre_poly(self.p[i, j], y[j])
    #        print('i prod', prod)
            v = v + self.coeffs[i] * prod
        return v

    def fit(self):
        self.print('Starting Discrete L2 regression')
        self.print(f'type:{self.type}, distr:{self.distr}, ',
                   f'samp_rel:{self.samp_rel}, domain:{self.domain}')
        self.print(f'dim: {self.dim}, degrees: {self.deg}')
        self.poly_space()
        self.print('Polynomial space dimension: ', self.p_dim)
        self.samp_size = self.get_samp_size()
        self.print(f'M: {self.samp_size}')
        self.sample, self.likelihood = self.sampler(self.samp_size)
        # self.print('Polynomial space basis (indexes): \n', self.p)
        self.D = self.assemble_D()
        # self.W = self.get_weights()
        # DWD = self.D.T @ self.W @ self.D
        DW = (self.D.T*self.likelihood)
        DWD = DW @ self.D

        # Ms = np.logspace(1, np.log10(self.samp_size), 20).astype('int')
        # DD = [1 / M * self.D[:M].T /self.likelihood[:M] @ self.D[:M] for M in Ms]
        # Norms = [np.linalg.norm(DD_ - np.eye(15)) for DD_ in DD]
        # plt.loglog(Ms, Norms)
        # plt.savefig('norms_conv_cheb.pdf')
        cond_num_D = np.linalg.cond(DWD)
        self.print(f'Cond. number: {cond_num_D}')
        self.fs = self.fun(self.sample)
        # self.coeffs = np.linalg.solve(DWD, self.D.T @ self.W @ self.fs)
        self.coeffs = np.linalg.solve(DWD, DW @ self.fs)

    # First implementation

    # def poly_space(self):
    #     if self.type == 'TD':
    #
    #         p_new = []
    #         for i in range(0, self.deg + 1):
    #             j = 0
    #             while i + j <= self.deg:
    #                 p_new.append([i, j])
    #                 j = j + 1
    #     elif self.type == 'HC':
    #         p_new = []
    #         for i in range(0, self.deg + 1):
    #             j = 0
    #             while (i + 1) * (j + 1) <= self.deg + 1:
    #                 p_new.append([i, j])
    #                 j = j + 1
    #     self.p = np.asarray(p_new)
    #     self.p_dim = self.p.shape[0]
    #     # fig, ax = plt.subplots(figsize=(5, 5))
    #     # ax.plot(*self.p.T, 'o')
    #     # ax.set_xlabel(r'$p_1$')
    #     # ax.set_ylabel(r'$p_2$')
    #     # ax.grid()
    #     # ax.set_title(f'${self.type}, N={self.deg}$')
    #     # fig.tight_layout()
    #     # fig.savefig(f'{self.type}_{self.deg}.pdf')
    #     return

    # Second implementation

    # def poly_space(self):
    #     t0 = time()
    #     polys = np.arange(self.deg+1)
    #     combs = product(polys, repeat=self.dim)
    #     if self.type == 'TD':
    #         filtered = filter(lambda x: np.sum(x) <= self.deg, combs)
    #     elif self.type == 'HC':
    #         filtered = filter(lambda x: np.prod(np.array(x)+1) <= (self.deg + 1), combs)
    #     self.p = np.vstack([*filtered])
    #     self.p_dim = self.p.shape[0]
    #     self.print(f'Time to build Lambda: {time() - t0}')
    #     # fig, ax = plt.subplots(figsize=(5, 5))
    #     # ax.plot(*self.p.T, 'o')
    #     # ax.set_xlabel(r'$p_1$')
    #     # ax.set_ylabel(r'$p_2$')
    #     # ax.grid()
    #     # ax.set_title(f'${self.type}, w={self.deg}$')
    #     # fig.tight_layout()
    #     # fig.savefig(f'{self.type}_{self.deg}.pdf')
    #     return

    def poly_space(self):
        t0 = time()
        if self.type == 'TD':
            cond = lambda x: np.sum(x, axis=1) <= self.deg
        elif self.type == 'HC':
            cond = lambda x: np.prod(x + 1, axis=1) <= (self.deg + 1)
        ps = np.zeros((1, self.dim))
        for i in range(1, self.dim+1):
            ps_ = np.copy(ps)
            ps_new = []
            for j in range(self.deg):
                ps_[:, -i] += 1
                ps_new.append(ps_[cond(ps_)])
            ps = np.vstack([ps, *ps_new])
        self.p = ps
        self.p_dim = self.p.shape[0]
        self.print(f'Time to build Lambda: {time() - t0}')
        # fig, ax = plt.subplots(figsize=(5, 5))
        # ax.plot(*self.p.T, 'o')
        # ax.set_xlabel(r'$p_1$')
        # ax.set_ylabel(r'$p_2$')
        # ax.grid()
        # ax.set_title(f'${self.type}, w={self.deg}$')
        # fig.tight_layout()
        # fig.savefig(f'{self.type}_{self.deg}.pdf')
        return

    def get_samp_size(self):
        if self.samp_rel == 'linear':
            samp_size = self.samp_const * self.p_dim
        elif self.samp_rel == 'quad':
            samp_size = self.samp_const * self.p_dim ** 2
        return samp_size

    def sampler(self, samp_size):
        if self.distr == 'uniform':
            #        np.random.seed(0)
            sample = np.random.uniform(*self.domain, (samp_size, self.dim))
            likelihood = np.ones(samp_size)
        elif self.distr == 'Cheb':
            y = np.random.uniform(-1, 1, (samp_size, self.dim))
            # y = 2 / np.pi * np.arcsin(y)
            y = 2 * np.sin(y*np.pi/2)**2 - 1
            likelihood = ((.5) / (1 / np.pi * (1 / np.sqrt(1 - y**2)))).prod(axis=1)
            sample = self.map(y, (-1, 1), self.domain)
            # likelihood = (.5) / (1 / np.pi * (1 / np.sqrt(1 - sample**2))).prod(axis=1)
        return sample, likelihood

    def assemble_D(self):
        # D = np.zeros((self.samp_size, self.p_dim))
        # for i in range(self.samp_size):
        #     for j in range(self.p_dim):
        #         prod = 1
        #         for k in range(self.dim):
        #             prod = prod * self.Legendre_poly(self.p[j, k], self.sample[i, k])
        #         D[i, j] = prod
        t0 = time()
        all = []
        for i in range(self.deg+1):
            evals = self.Legendre_poly(i, self.sample)
            all.append(evals)

        all = np.transpose(all, (2, 0, 1))
        D = np.ones((self.p_dim, self.samp_size))
        for k in range(self.dim):
            P = self.p[:, k].astype('int')
            D *= all[k][P]
        D = D.T
        self.print(f'Time to assemble D:{time() - t0}')

        # t0 = time()
        # D = np.zeros((self.samp_size, self.p_dim))
        # for j in range(self.p_dim):
        #     # print(j)
        #     prod = 1
        #     for k in range(self.dim):
        #         prod = prod * self.Legendre_poly(self.p[j, k], self.sample[:, k])
        #     D[:, j] = prod
        # self.print(f'Time to assemble D:{time() - t0}')
        return D

    def get_weights(self):
        W = np.diag(self.likelihood)
        return W

    def map(self, x, orig, to):
        scale = (to[1] - to[0])/(orig[1] - orig[0])
        out = to[0] + scale*(x-orig[0])
        return out

    # One dimensional Legendre poly
    def Legendre_poly(self, deg, y):
        # L = Legendre(np.ones(self.deg + 1), domain=self.domain)
        return self.poly.basis(deg, domain=self.domain)(y) * np.sqrt(2 * deg + 1)


if __name__ == '__main__':
    def fun(x,):
        f = 0
        c_n = 9 / 2
        w_1 = 0.5
        f = np.cos(2 * np.pi * w_1 + np.sum(c_n * x, axis=1))
        return f

    np.random.seed(0)
    reg = DiscreteL2(fun, dim=2, deg=7, type='TD', distr='uniform', samp_rel='linear', verb=True)
    reg.fit()
    test_size = 100
    test_sample, test_lkl = reg.sampler(test_size)
    v, err, rel_err = reg.eval(test_sample, test_lkl)
    # v_coeffs7, v7, v_error7, p7, p_dim7, M7, w7, D7, cond_num_D7 = get_coeffs(
    #     N=2, w=7, type='TD', distr='uniform', rel='linear')
    # print('Coefficients: ', v_coeffs7)
    # print('Error: ', v_error7)
