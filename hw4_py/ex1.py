from L2reg import DiscreteL2
import numpy as np
import matplotlib.pyplot as plt

from IPython.core.debugger import set_trace


def exact_f(x, N, num):
    f = 0
    if num == 1:
        c_n = 9 / N
        w_1 = 0.5
        f = np.cos(2 * np.pi * w_1 + np.sum(c_n * x, axis=1))
    if num == 2:
        c_n = 7.25 / N
        w_n = 0.5
        f = np.prod((c_n ** (-2) + (x - w_n) ** 2) ** (-1),  axis=1)
    if num == 3:
        c_n = 1.85 / N
        f = (1 + np.sum(c_n * x, axis=1)) ** (-N + 1)
    if num == 4:
        c_n = 7.03 / N
        w_n = 0.5
        f = np.exp(-np.sum(c_n ** 2 * (x - w_n) ** 2, axis=1))
    if num == 5:
        c_n = 2.04 / N
        w_n = 0.5
        f = np.exp(-np.sum(c_n * np.abs(x - w_n), axis=1))
    if num == 6:
        N = np.shape(x)[1]
        c_i = 4.3 / N
        w_1 = np.pi / 4
        w_2 = np.pi / 5
        mask = (x[:, 0] > w_1) | (x[:, 1] > w_2)
        f = np.exp(c_i * np.sum(x, axis=1))
        f[mask] = 0
    return f


def exercise(N, num, type, distr, samp_rel, degrees, samp_const=1):
    np.random.seed(0)
    def fun(x): return exact_f(x, N, num)
    reg = DiscreteL2(fun, dim=N, deg=degrees[0], type=type, distr=distr,
                     samp_rel=samp_rel, verb=True, samp_const=samp_const)
    reg.fit()
    samp_sizes = [reg.samp_size]
    test_size = 10000
    test_sample, test_lkl = reg.sampler(test_size)
    v, err, rel_err = reg.eval(test_sample, test_lkl)
    errs = [err]
    for w in degrees[1:]:
        reg = DiscreteL2(fun, dim=N, deg=w, type=type, distr=distr,
                         samp_rel=samp_rel, verb=True, samp_const=samp_const)
        reg.fit()
        v, err, rel_err = reg.eval(test_sample, test_lkl)
        errs.append(err)
        samp_sizes.append(reg.samp_size)
    fig, ax = plt.subplots(figsize=(5, 5))
    ax.semilogy(samp_sizes, errs, 'o--', ms=3, label='Error')
    ax.set_xlabel('Sample size')
    ax.set_ylabel('MSE')
    ax.set_title(f'Function:{num}, N:{N}, index set:{type}, distr.:{distr}')
    # ax.legend()
    ax.grid()
    fig.tight_layout()
    fig.savefig(f'rel_err_{num}_{N}_{type}_{distr}.pdf')


for N in [2, 5, 10]:
    for num in range(1, 7):
        degrees = np.arange(1, 5).astype('int')**2
        exercise(N=N, num=num, type='HC', distr='uniform',
                 samp_rel='quad', degrees=degrees)
        degrees = np.arange(1, 5).astype('int')**2
        exercise(N=N, num=num, type='HC', distr='Cheb',
                 samp_rel='linear', samp_const=10 * 2**N, degrees=degrees)
        degrees = np.arange(1, 5).astype('int')
        exercise(N=N, num=num, type='TD', distr='uniform',
                 samp_rel='quad', degrees=degrees)
        exercise(N=N, num=num, type='TD', distr='Cheb',
                 samp_rel='linear', samp_const=2 * 2**N, degrees=degrees)

# for N in [2, 5, 10]:
    # for num in range(1, 7):
