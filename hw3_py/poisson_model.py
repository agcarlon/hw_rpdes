import numpy as np
from numpy.linalg import eig, norm, det
# import matplotlib.pyplot as plt
# import sobol_seq as ss
# import qmcpy as qp
from scipy.sparse import diags as sp_diags
from scipy.sparse.linalg import spsolve
from joblib import Parallel, delayed
import matplotlib.pyplot as plt
from scipy.sparse.linalg import inv

from IPython.core.debugger import set_trace


class Poisson():
    def __init__(self, force, nu=2.5, max_mesh_param=1000, domain=[0, 1],
                 kl_contr=.95, n_jobs=8, plot=False):
        self.force = force
        self.nu = nu
        self.max_mesh_param = max_mesh_param
        self.domain = domain
        self.kl_contr = kl_contr
        self.n_jobs = n_jobs
        self.counter = 0
        self.grads = 0
        self.basis = None
        self.plot = plot
        self.kappa = None

    def solve(self, sample, mesh_param):
        self.counter += 1
        h = (self.domain[1] - self.domain[0]) / mesh_param
        if self.eig_vecs is None:
            self.get_karhunen_loeve_basis()
        xs = np.linspace(self.domain[0], self.domain[1], mesh_param + 1)
        eig_vecs = [np.interp(xs[:-1] + h / 2, self.xs, bs) for bs in self.eig_vecs]
        eig_vecs = np.vstack(eig_vecs)
        # eig_vecs = (eig_vecs.T / np.sqrt(h*(eig_vecs**2).sum(axis=1))).T
        # basis = np.sqrt(self.eig_vals)*eig_vecs
        basis = [np.sqrt(val) * vec for val, vec in zip(self.eig_vals, eig_vecs)]
        basis = np.vstack(basis)
        self.kappa = sample @ basis
        coefs = np.exp(self.kappa)

        fv = np.array([self.force(x) for x in xs[1:-1]])
        u = np.zeros((len(coefs), mesh_param + 1))

        # Parallel
        with Parallel(n_jobs=self.n_jobs) as par:
            u_ = par(delayed(solve_system)(coefs_, h, fv) for coefs_ in coefs)
        # u_ = [solve_system(coefs_, h, fv) for coefs_ in coefs]

        u[:, 1:-1] = np.vstack(u_)

        if self.plot:
            fig, ax = plt.subplots(figsize=(5, 5))
            ax.plot(xs, u[0])
            fig.savefig('u.pdf')

        Qs = h * np.sum(u, axis=1)
        return Qs

    def get_karhunen_loeve_basis(self):
        xs = np.linspace(
            self.domain[0], self.domain[1], self.max_mesh_param + 1)
        h = (self.domain[1] - self.domain[0]) / self.max_mesh_param
        self.xs = xs[:-1] + h/2
        cov_matrix = self.covariance_matrix(self.xs, nu=self.nu)
        eig_vals, eig_vecs = eig(cov_matrix)
        # eig_vecs = eig_vecs / np.sqrt(h*(eig_vecs**2).sum(axis=0))
        weights = (eig_vals**.5).real * np.max(np.abs(eig_vecs), axis=0)
        weights[np.isnan(weights)] = 0

        idxs = np.flip(np.argsort(weights))
        weights_ = weights[idxs] / np.nansum(weights)
        self.n_basis = np.where(np.cumsum(weights_) > self.kl_contr)[0][0] + 1
        eig_vals = eig_vals[idxs[:self.n_basis]].real
        eig_vecs = eig_vecs[:, idxs[:self.n_basis]].real.T
        # self.eigvecs = [np.sqrt(val) * vec for val,
        #                 vec in zip(eig_vals, eig_vecs)]
        # self.eigvecs = np.vstack(self.eigvecs)
        self.eig_vecs = eig_vecs
        self.eig_vals = eig_vals
        if self.plot:
            fig, ax = plt.subplots(figsize=(5, 5))
            ax.plot(self.xs, self.eig_vecs[0])
            ax.plot(self.xs, self.eig_vecs[1])
            fig.savefig(f'KL_basis_{self.max_mesh_param}.pdf')

    def covariance_matrix(self, xs, nu=1.5, sigma2=2, rho=.1):
        aux = np.abs(xs[:, np.newaxis] - xs[np.newaxis, :]) / rho
        if nu == .5:
            C = sigma2 * np.exp(-aux)
        elif nu == 1.5:
            C = sigma2 * (1 + np.sqrt(3) * aux) * np.exp(-np.sqrt(3) * aux)
        elif nu == 2.5:
            C = sigma2 * (1 + np.sqrt(5) * aux + np.sqrt(3) * aux**2) * \
                np.exp(-np.sqrt(5) * aux)
        if nu == 'inf':
            C = sigma2 * np.exp(-aux**2 / 2)
        return C

    def gradient(self, sample, mesh_param):
        self.grads += 1
        h = (self.domain[1] - self.domain[0]) / mesh_param
        if self.eig_vecs is None:
            self.get_karhunen_loeve_basis()
        xs = np.linspace(self.domain[0], self.domain[1], mesh_param + 1)
        fv = np.array([self.force(x) for x in xs[1:-1]])

        eig_vecs = [np.interp(xs[:-1] + h / 2, self.xs, bs) for bs in self.eig_vecs]
        eig_vecs = np.vstack(eig_vecs)
        # eig_vecs = (eig_vecs.T / np.sqrt(h*(eig_vecs**2).sum(axis=1))).T
        # basis = np.sqrt(self.eig_vals)*eig_vecs
        basis = [np.sqrt(val) * vec for val, vec in zip(self.eig_vals, eig_vecs)]
        basis = np.vstack(basis)
        self.kappa = sample @ basis
        coefs = np.exp(self.kappa)[0]

        del_a = coefs*basis
        off = -coefs[1:-1] / h**2
        mid = np.convolve(coefs, [1, 1])[1:-1] / h**2
        A = sp_diags([off, mid, off], [-1, 0, 1])
        inv_A = inv(A.tocsc())
        del_Q = []
        for del_a_ in del_a:
            off = -del_a_[1:-1] / h**2
            mid = np.convolve(del_a_, [1, 1])[1:-1] / h**2
            del_A = sp_diags([off, mid, off], [-1, 0, 1])
            del_u = -inv_A @ del_A @ inv_A @ fv
            del_Q.append(h * np.sum(del_u))

        # Validation with FD
        # dx = 1e-4
        # samples = np.tile(sample, (self.n_basis+1, 1))
        # samples[1:] = samples[1:] + dx*np.eye(self.n_basis)
        # Qs = self.solve(samples, mesh_param)
        # del_Q_fd = (Qs[1:] - Qs[0])/dx
        # set_trace()
        return np.array(del_Q)


def solve_system(coefs, h, fv):
    off = -coefs[1:-1] / h**2
    mid = np.convolve(coefs, [1, 1])[1:-1] / h**2
    A_ = sp_diags([off, mid, off], [-1, 0, 1])
    return spsolve(A_.tocsr(), fv)


if __name__ == '__main__':
    samp_size = 10000
    kl_mesh_param = 2048
    nu = 'inf'

    def force(x):
        return 4 * np.pi**2 * np.cos(2 * np.pi * x)

    model = Poisson(force, nu=nu, max_mesh_param=kl_mesh_param, n_jobs=78, kl_contr=.999)
    model.get_karhunen_loeve_basis()
    print(model.n_basis)

    mesh_params = [1024, 512, 256, 128, 64, 32, 16, 8]
    samp = np.random.randn(samp_size, model.n_basis)
    # for mesh_param in mesh_params:
    #     h = (model.domain[1] - model.domain[0]) / mesh_param
    #     xs = np.linspace(model.domain[0], model.domain[1], mesh_param + 1)
    #     xs = xs[:-1] + h/2
    #     cov = model.covariance_matrix(xs, nu=nu)
    #     sss = np.ceil(np.logspace(1, np.log10(samp_size), 10)).astype('int')
    #     cov_ = []
    #     err = []
    #     for ss in sss:
    #         cov_.append(np.cov(model.kappa[:ss].T, ddof=1))
    #         err.append(np.max(np.abs(cov_[-1] - cov)))
    #
    #     fig, ax = plt.subplots(figsize=(5, 5))
    #     ax.loglog(sss, err)
    #     ax.grid()
    #     ax.set_xlabel('sample size')
    #     ax.set_ylabel('error')
    #     fig.savefig(f'rf_sampling_test_{mesh_param}.pdf')

    qois = []
    for mesh_param in mesh_params:
        qois.append(model.solve(samp, mesh_param))
    # estim = np.mean(qois, axis=1)

    qois = np.array(qois)
    diff = qois[1:] - qois[:-1]

    set_trace()

    weak = np.abs(np.mean(diff, axis=1))
    strong = np.mean(diff**2, axis=1)

    hs = 1 / np.array(mesh_params)

    weak_ = 1.1*weak[0]*hs[:-1]**2 / hs[0]**2
    strong_ = 1.1*strong[0]*hs[:-1]**4 / hs[0]**4

    fig, axs = plt.subplots(2, 1, figsize=(5, 5), sharex=True)
    axs[0].loglog(hs[:-1], weak, 'o--')
    axs[0].loglog(hs[:-1], weak_, 'k--', label=r'$\mathcal{O}(h^2)$')
    axs[0].set_ylabel(r'weak conv.')
    axs[0].grid()
    axs[0].legend()
    axs[1].loglog(hs[:-1], strong, 'o--')
    axs[1].loglog(hs[:-1], strong_, 'k--', label=r'$\mathcal{O}(h^4)$')
    axs[1].set_ylabel(r'strong conv.')
    axs[1].set_xlabel(r'$h$')
    axs[1].grid()
    axs[1].legend()
    fig.tight_layout()
    plt.savefig('weak_strong.pdf')
    set_trace()

    # plt.show()
