import numpy as np
from functools import partial
from mlmc import MultiLevel, MonteCarlo
from poisson_model import Poisson
from scipy.stats import norm
from scipy.optimize import minimize
import matplotlib.pyplot as plt

from IPython.core.debugger import set_trace

# K = -20
# nu = 2.5

def mlmc_is_problem(K, nu, sample_size):
    def force(x):
        return 4 * np.pi**2 * np.cos(2 * np.pi * x)


    model = Poisson(force, nu=nu, max_mesh_param=80, n_jobs=75)
    model.get_karhunen_loeve_basis()


    def qoi(sample, mesh_param):
        p_m = model.solve(sample, mesh_param) < K
        return p_m

    # def sampler(n):
    #     return np.random.randn(n, model.n_basis)


    class Sampler():
        def __init__(self, mu=0, std=1):
            self.mu = mu
            self.std = std

        def sample(self, n):
            return self.mu + self.std * np.random.randn(n, model.n_basis)

        def __call__(self, n):
            return self.sample(n)

        def likelihood(self, x):
            return norm.pdf(x).prod(axis=1) \
                / (1 / self.std * norm.pdf((x - self.mu) / self.std)).prod(axis=1)


    sampler = Sampler()


    def get_is_mean(K=-5, nu=2.5, mu=0, mesh=8):
        def objf_fun(w):
            p_m = qoi([w], mesh)
            objf = p_m*norm.pdf(w).prod()
            return -objf[0]

        np.random.seed(0)

        def get_starting_point(mu=0):
            mc_size = 128
            k = 0
            while True:
                # sobol = qp.Sobol(N, seed=k)
                # normal = qp.Gaussian(sobol, covariance=1)
                # w = normal.gen_samples(mc_size)
                w = sampler(mc_size)
                Qs = model.solve(w, mesh)
                if np.any(Qs < K):
                    idx = np.argmin(Qs)
                    x = w[idx]
                    break
                k += 1
            return x
        x0 = get_starting_point(mu=mu)
        # x0 = np.array([-0.782,  1.446, -1.536, -0.148, -1.938])
        res = minimize(objf_fun, x0, method='Nelder-Mead')
        print(f'Finding mu*: {res.success}')
        # res = minimize(objf_fun, x0)
        mu = res.x
        return mu

    # mu_8 = get_is_mean(K=K, nu=nu, mesh=8)
    mu = get_is_mean(K=K, nu=nu, mesh=20)
    # print(f'K={K}, nu={nu}, mu={mu[:4]}')
    # mu_32 = get_is_mean(K=K, nu=nu, mesh=32)
    # mu_64 = get_is_mean(K=K, nu=nu, mesh=64)

    # mu = np.zeros(model.n_basis)
    # if nu == 2.5:
    #     if K == -5:
    #         # mu[:4] = np.array([-1.452, -0.004, -1.342, -0.219])
    #         # mu[:4] = np.array([1.148807, 0.40883358, -1.64000834, 0])
    #         # mu[:4] = np.array([-1.38445804,  0.59592335, -1.31027396, -0.68933119])
    #         # mu[:4] = np.array([-1.36526474,  0.69725196, -1.45419447, -1.06027723])
    #         mu[:4] = np.array([-1.60227065,  0.48839168, -1.22987965, -0.76775705])
    #     if K == -10:
    #         mu[:4] = np.array([-1.841, -0.01, -1.92, -0.578])
    #     if K == -20:
    #         # mu[:4] = np.array([-2.102, -0.609, -2.706, 0.743])
    #         mu[:4] = np.array([-2.18150989,  0.12172546, -2.96710763,  0.65092737])
    # elif nu == 'inf':
    #     if K == -5:
    #         mu[:4] = np.array([-1.406, -0.05, -1.161, -0.023])
    #     if K == -10:
    #         mu[:4] = np.array([-2.227, 0.054, -1.198, -0.113])
    #     if K == -20:
    #         mu[:4] = np.array([-2.63, -0.147, -1.593, 1.321])

    mu[1:] = 0
    sampler_is = Sampler(mu=mu)

    # stds = []
    # for i in range(len(mu)):
    #     mu_ = np.copy(mu)
    #     mu_[-i:] = 0
    #     sampler_is = Sampler(mu=mu_)
    #
    #     samp = sampler_is(10000)
    #     Qs = model.solve(samp, mesh_param=128)
    #     Ps = (Qs<K)*sampler_is.likelihood(samp)
    #     stds.append(np.std(Ps, ddof=1))
    #     print(np.mean(Ps))
    #     print(stds[-1])

    tol = 0.05
    err_split = 0.5
    # meshes = [16, 32, 64]
    # meshes = [5, 10, 20, 40]

    # estim = MonteCarlo(model.solve, sampler)
    name = f'nu{nu}_K{-K}/IS_mlmc_is_nu{nu}_K{-K}'
    mlmc_is = MultiLevel(qoi=qoi, estim=MonteCarlo, sampler=sampler_is, err_split=err_split,
                        init_samp_size=sample_size, rel_tol=tol, verb=False, plot=False,
                        name=name, min_mesh_param=1., coarsest_mesh=5, mesh_conv_samp_size=100000)

    # mlmc_is.eval(meshes=meshes)
    mlmc_is.eval()
    print(f'nu{nu}_K{-K}: MLMC-IS: {mlmc_is.estimate}')

    name = f'nu{nu}_K{-K}/mlmc_is_nu{nu}_K{-K}'
    mlmc = MultiLevel(qoi=qoi, estim=MonteCarlo, sampler=sampler, err_split=err_split,
                      init_samp_size=sample_size, rel_tol=tol, verb=True, plot=False,
                      name=name, mesh_conv_samp_size=10000)
    # mlmc.eval(meshes=meshes)  # mlmc_is.meshes
    mlmc.eval(meshes=mlmc_is.meshes)  # mlmc_is.meshes
    print(f'nu{nu}_K{-K}: MLMC: {mlmc.estimate}')

    fig, ax = plt.subplots(figsize=(5, 5))
    ax.axhline(np.sqrt(tol**2/2), c='k', ls='--', label=r'tol')
    ax.loglog(mlmc.works, np.sqrt(mlmc.rel_vars), 'o--', label='MLMC')
    ax.loglog(mlmc_is.works, np.sqrt(mlmc_is.rel_vars), 'o--', label='MLMC-IS')
    ax.legend()
    ax.grid()
    ax.set_xlabel('work')
    ax.set_ylabel('rel. std. error')
    fig.tight_layout()
    fig.savefig(f'is_nu{nu}_K{-K}_std_err.pdf')

    mlmc_samp = np.array(mlmc.samp_sizes[1:])
    mlmc_is_samp = np.array(mlmc_is.samp_sizes[1:])
    # set_trace()

    # %%
    fig, ax = plt.subplots(figsize=(5, 5))
    colors = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5'][:len(mlmc.samp_sizes[0])]
    for i, smp in enumerate(mlmc_samp.T):
        ax.semilogy(smp, 'o--', c=colors[i])
    for i, smp in enumerate(mlmc_is_samp.T):
        ax.semilogy(smp, 's-.', c=colors[i])

    plts = [
        plt.semilogy([], [], 'ko--', label='MLMC'),
        plt.semilogy([], [], 'ks-.', label='MLMC-IS')
    ]

    plts_cs = [
        plt.semilogy([], [], f'{C}', label=f'$M_{i}$') for i, C in enumerate(colors)
    ]

    # legend1 = plt.legend(plts, loc=1)
    ax.legend()
    # ax.add_artist(legend1)

    ax.legend()
    ax.grid()
    ax.set_xlabel('steps')
    ax.set_ylabel('sample sizes')
    fig.tight_layout()
    fig.savefig(f'is_nu{nu}_K{-K}_samp_sizes.pdf')
    plt.close('all')


mlmc_is_problem(K=-5, nu=2.5, sample_size=2**5)
mlmc_is_problem(K=-10, nu=2.5, sample_size=2**5)
mlmc_is_problem(K=-20, nu=2.5, sample_size=2**5)

mlmc_is_problem(K=-5, nu='inf', sample_size=2**5)
mlmc_is_problem(K=-10, nu='inf', sample_size=2**5)
mlmc_is_problem(K=-20, nu='inf', sample_size=2**5)
