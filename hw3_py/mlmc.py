import numpy as np
from numpy.linalg import eig, norm, det, solve
from scipy.stats import norm
import matplotlib.pyplot as plt
from time import time
from warnings import warn

from IPython.core.debugger import set_trace


class Sampler():
    def sample(self, n):
        pass

    def __call__(self, n):
        return self.sample(n)

    def likelihood(x):
        pass


class MonteCarlo():
    def __init__(self, qoi, sampler, init_samp_size=64, verb=False, find_mesh=True,
                 mesh_param=10, rel_tol=0.05, err_split=0.5,
                 max_iters=100, clt_conf=0.9, seed=0):
        self.qoi = qoi
        self.sampler = sampler
        self.init_samp_size = init_samp_size
        self.samp_size = 0
        self.find_mesh = find_mesh
        self.mesh_param = mesh_param
        self.rel_tol = rel_tol
        self.err_split = err_split
        self.max_iters = max_iters
        self.clt_conf = clt_conf
        self.clt_const = norm.ppf(1 / 2 + clt_conf / 2)
        self.seed = seed
        self.sample = []
        self.evaluations = np.array([])
        self.estimate = None
        self.verb = verb
        if verb:
            self.print = print
        else:
            self.print = lambda x: None

    def eval(self):
        np.random.seed(self.seed)
        if self.find_mesh:
            self.def_mesh_param()
        else:
            self.get_sample(self.init_samp_size)
        for k in range(self.max_iters):
            self.compute_std_err()
            self.rel_err = np.abs(self.std_err / self.estimate)
            if self.clt_const * self.rel_err < self.err_split * self.rel_tol:
                break
            samp_size = self.get_sample_size()
            to_sample = np.minimum(
                samp_size - self.samp_size, self.samp_size).astype('int')
            to_sample = np.maximum(to_sample, np.ceil(
                self.samp_size / 10)).astype('int')
            self.get_sample(to_sample)
        return self.estimate

    def compute_std_err(self):
        self.std_dev = np.std(self.evaluations, ddof=1)
        self.std_err = self.std_dev / self.samp_size**.5

    def get_sample_size(self):
        samp_size = (self.std_dev * self.clt_const /
                     (self.estimate * self.err_split * self.rel_tol))**2
        samp_size = np.ceil(samp_size)
        return samp_size

    def get_sample(self, to_sample):
        if to_sample > 0:
            self.sample.append(self.sampler(to_sample))
            self.evaluations = np.hstack(
                [self.evaluations, self.qoi(self.sample[-1], self.mesh_param)])
            self.estimate = np.mean(self.evaluations)
            self.samp_size += to_sample

    def def_mesh_param(self):
        # self.sample = [self.sampler(self.init_samp_size)]
        # evaluations = [self.qoi(self.sample[0], self.mesh_param)]
        self.get_sample(self.init_samp_size)
        estimate = [np.mean(self.evaluations)]
        bias_decr = []
        for k in range(1, self.max_iters):
            self.evaluations = self.qoi(self.sample[0], self.mesh_param * 2**k)
            estimate.append(np.mean(self.evaluations))
            bias_decr.append(np.abs(estimate[-1] - estimate[-2]))
            bias_tol = np.abs(
                self.rel_tol * (1 - self.err_split) * estimate[-1])
            if bias_decr[-1] < bias_tol:
                self.mesh_param = self.mesh_param * 2**k
                # self.evaluations = self.evaluations[-1]
                self.estimate = estimate[-1]
                self.samp_size = self.init_samp_size
                break
                # bias_decr = np.array(bias_decr)
                # bias_decr[1:]*4-bias_decr[:-1]
        return


# class QuasiMonteCarlo(MonteCarlo):
#     def __init__(self, qoi, sampler, outer_sample=10, **kwargs):
#         super().__init__(qoi, sampler, **kwargs)
#         self.outer_sample = outer_sample
#
#     def compute_std_err(self):
#         self.std_dev = np.std(self.evaluations, ddof=1)
#         self.std_err = self.std_dev / self.samp_size**.5
#
#     def get_sample(self, to_sample):
#         if to_sample > 0:
#             samp = [self.sampler(to_sample) for i in range()]
#
#             self.sample.append()
#             set_trace()
#             self.evaluations = np.hstack(
#                 [self.evaluations, self.qoi(self.sample[-1], self.mesh_param)])
#             self.estimate = np.mean(self.evaluations)
#             self.samp_size += to_sample


class MultiLevel():
    def __init__(self, qoi, estim, sampler, init_samp_size=64, coarsest_mesh=4,
                 rel_tol=0.1, err_split=0.5, max_iters=1000, samp_incr_param=.5,
                 cost_rate=3, weak_rate=2, strong_rate=4, mesh_conv_samp_size=1024,
                 verb=False, min_mesh_param=0.5, plot=False, name=''):
        self._qoi = lambda samp, mesh: (qoi(samp, mesh)
                                        * sampler.likelihood(samp))
        # self._qoi = qoi
        self.estim = estim
        self.sampler = sampler
        self.init_samp_size = init_samp_size
        self.mesh_conv_samp_size = mesh_conv_samp_size
        self.coarsest_mesh = coarsest_mesh
        self.min_mesh_param = min_mesh_param
        self.rel_tol = rel_tol
        self.err_split = err_split
        self.mesh_param = coarsest_mesh
        self.max_iters = max_iters
        self.samp_incr_param = samp_incr_param
        self.weak_params = [0, weak_rate]
        self.strong_params = [0, strong_rate]
        self.cost_params = [0, cost_rate]
        self.times = {}
        self.evals_mesh = {}
        self.verb = verb
        self.plot = plot
        self.name = name
        if verb:
            self.print = print
        else:
            self.print = lambda x: None

    def qoi(self, samp, mesh):
        t0 = time()
        qoi = self._qoi(samp, mesh)
        self.times[mesh].append(time() - t0)
        self.evals_mesh[mesh].append(len(samp))
        return qoi

    def qoi_delta(self, sample, mesh_param):
        return self.qoi(sample, mesh_param) - self.qoi(sample, int(mesh_param / 2))

    def eval(self, seed=0, meshes=[]):
        self.print('Evaluating Multilevel estimator')
        np.random.seed(seed)
        if not len(meshes):
            self.def_mesh_param()
        else:
            self.meshes = np.array(meshes)
        self.times = {mesh: [] for mesh in self.meshes}
        self.evals_mesh = {mesh: [] for mesh in self.meshes}
        L = len(self.meshes)
        seeds = np.random.randint(1e8, size=L)
        lvls = [self.estim(self.qoi, self.sampler,
                           mesh_param=self.meshes[0], seed=seeds[0])]
        for mesh, seed in zip(self.meshes[1:], seeds):
            lvls.append(self.estim(self.qoi_delta, self.sampler,
                                   mesh_param=mesh, seed=seed))

        estims = []
        samp_sizes = [np.zeros(L, dtype='int')]
        to_sample = np.ones(L, dtype='int') * self.init_samp_size
        vs = np.zeros(L)
        cs = self.meshes**3
        rel_vars = []
        works = []
        var_tol = self.err_split**2 * self.rel_tol**2 / 1.65**2
        while True:
            estims.append([])
            samp_sizes.append(np.copy(samp_sizes[-1]))
            for i, lvl in enumerate(lvls):
                lvl.get_sample(to_sample[i])
                samp_sizes[-1][i] += to_sample[i]
                lvl.compute_std_err()
                vs[i] = lvl.std_dev**2
                estims[-1].append(lvl.estimate)
            vs[vs == 0.] = np.max(vs)
            works.append(np.sum(samp_sizes[-1] * cs))
            rel_vars.append(np.sum(vs / samp_sizes[-1])
                            / np.abs(np.sum(estims[-1]))**2)
            # self.print(f'M: {samp_sizes[-1]}')
            # self.print(
            #     f'var. estim.: {rel_vars[-1]:.3e}, var. tol: {var_tol:.3e}')
            if rel_vars[-1] < var_tol:
                break
            samp_sizes_opt = var_tol**-1 * np.sum(estims[-1])**-2 * \
                np.sum(np.sqrt(vs * cs)) * np.sqrt(vs / cs)
            samp_sizes_opt = np.ceil(samp_sizes_opt).astype('int')
            to_sample = np.minimum(samp_sizes_opt - samp_sizes[-1],
                                   np.ceil(self.samp_incr_param*samp_sizes[-1])).astype('int')
            mask = to_sample > 0
            to_sample[~mask] = 0
            to_sample[mask] = np.maximum(to_sample[mask], np.ceil(
                samp_sizes[-1][mask] / 10)).astype('int')
        self.estimate = np.sum(estims[-1])
        self.print('Multilevel estimator evaluation finished')
        self.print(f'Final M: {samp_sizes[-1]}')
        self.print(f'Final Vls: {vs}')
        self.print(
            f'var. estim.: {rel_vars[-1]:.3e}, var. tol: {var_tol:.3e}')
        self.print(
            f'Work of MLMC estimator: {(cs * samp_sizes[-1]).sum():.3e}')
        self.works = works
        self.rel_vars = rel_vars
        self.samp_sizes = samp_sizes
        self.vl = vs
        if self.plot:
            aggr_estims = np.sum(estims, axis=1)
            rel_err = np.abs(
                (aggr_estims[:-1] - aggr_estims[-1]) / aggr_estims[-1])
            fig, ax = plt.subplots(figsize=(5, 5))
            ax.axhline(np.sqrt(var_tol), c='k', ls='--', label=r'tol')
            ax.loglog(works[:-1], rel_err, 'o--', ms=4, label='rel. error')
            ax.loglog(works, np.sqrt(rel_vars), 'o--',
                      ms=4, label='rel. std. error')
            ax.legend()
            ax.grid()
            ax.set_xlabel('work')
            ax.set_ylabel('rel. error')
            fig.tight_layout()
            fig.savefig(self.name + '_std_err.pdf')
        # self.compute_rates(estims[-1], lvls, vs, cs)

    def def_mesh_param(self):
        self.print('Defining mesh hierarchy')
        sample = [self.sampler(self.mesh_conv_samp_size)]
        evaluations = [self._qoi(sample[0], self.coarsest_mesh)]
        estimate = [np.mean(evaluations[-1])]
        bias_decr = []
        rel_std_lvl = []
        mesh_params = [self.coarsest_mesh]
        for k in range(1, self.max_iters):
            mesh_params.append(self.coarsest_mesh * 2**k)
            evaluations.append(
                self._qoi(sample[0], mesh_params[-1]))
            estimate.append(np.mean(evaluations[-1]))
            rel_std_lvl.append(np.std(evaluations[-1] - evaluations[-2])
                               / np.std(evaluations[-2]))
            bias_decr.append(
                np.abs((estimate[-1] - estimate[-2]) / estimate[-1]))
            bias_tol = np.abs(
                self.rel_tol * (1 - self.err_split))
            self.print(
                f'bias est.: {bias_decr[-1]:.3e}, tolerance: {bias_tol:.3e}')
            if bias_decr[-1] < bias_tol:
                # self.evaluations = evaluations[-1]
                # self.estimate = estimate[-1]
                # self.samp_size = self.init_samp_size
                break
        # bias_decr = np.array(bias_decr)
        # rel_bias = np.abs((bias_decr[1:] - bias_decr[:-1] / 4) / bias_decr[1:])
        # lvls_ = np.where(rel_bias < self.min_mesh_param)[0]
        # lvls = [lvls_[-1]]
        # for lvl in np.flip(lvls_[:-1]):
        #     if lvl + 1 in lvls:
        #         lvls.append(lvl)
        #     else:
        #         break
        # lvls = np.flip(lvls)

        meshes = 1 / np.array(mesh_params[1:])

        # z = np.polyfit(np.log(meshes), np.log(bias_decr), 1)
        # bias_rate = z[0]
        # bias_const = np.exp(z[1])
        # self.print(f'Bias conv.:{bias_const:.3e} h^{bias_rate:3f}')

        for i, lvl in enumerate(rel_std_lvl):
            if lvl < self.min_mesh_param:
                lvl0 = i + 1
                break
        else:
            raise ValueError(
                'The higher level does not satisfy min_mesh_param')

        self.meshes = np.array(mesh_params[lvl0:])
        lvls = np.arange(lvl0, len(mesh_params))

        evaluations = np.array(evaluations)
        # diff = evaluations[:-1] - evaluations[-1]
        diff = evaluations[1:] - evaluations[:-1]

        weak = np.abs(np.mean(diff, axis=1))
        strong = np.mean(np.abs(diff)**2, axis=1)

        # z = np.polyfit(np.log(meshes), np.log(weak), 1)
        # weak_rate = z[0]
        # weak_const = np.exp(z[1])
        # self.print(f'Weak conv.:{weak_const:.3e} h^{weak_rate:3f}')
        #
        # z = np.polyfit(np.log(meshes), np.log(strong), 1)
        # strong_rate = z[0]
        # strong_const = np.exp(z[1])
        # self.print(f'Strong conv.:{strong_const:.3e} h^{strong_rate:3f}')

        if len(self.meshes) >= 2:

            self.strong_rate = np.log2(strong[-2]/strong[-1])
            self.weak_rate = np.log2(weak[-2]/weak[-1])

            if self.plot:
                fig, ax = plt.subplots(figsize=(5, 5))
                ax.loglog(meshes, weak, 'o--')
                # bias_reg = 1.1*weak[0]*((1/2)**self.weak_rate)**np.arange(len(meshes))
                weak_reg = 1.1*weak[-1]*(meshes/meshes[-1])**self.weak_rate
                ax.loglog(meshes, weak_reg, 'k--', label=rf'$\mathcal{{O}}(h^{{ {self.weak_rate:.2f} }})$', lw=1)
                ax.legend()
                ax.set_xlabel(r'$h$')
                ax.set_ylabel(r'$|\mathbb{E}[g_{h} - g_{h/2}]|$')
                ax.grid()
                fig.tight_layout()
                fig.savefig(self.name + '_weak_rate.pdf')

                fig, ax = plt.subplots(figsize=(5, 5))
                ax.loglog(meshes, strong, 'o--')
                # bias_reg = 1.1*strong[0]*((1/2)**self.strong_rate)**np.arange(len(meshes))
                strong_reg = 1.1*strong[-1]*(meshes/meshes[-1])**self.strong_rate
                ax.loglog(meshes, strong_reg, 'k--', label=rf'$\mathcal{{O}}(h^{{ {self.strong_rate:.2f} }})$', lw=1)
                ax.legend()
                ax.set_xlabel(r'$h$')
                # ax.set_ylabel('strong rate')
                ax.set_ylabel(r'$\mathbb{E}[(g_{h} - g_{h/2})^2]$')
                ax.grid()
                fig.tight_layout()
                fig.savefig(self.name + '_strong_rate.pdf')

                fig, ax = plt.subplots(figsize=(5, 5))
                ax.loglog(meshes, rel_std_lvl, 'o--')
                for i, lvl in enumerate(lvls):
                    ax.text(meshes[lvl - 1] * 1.05,
                            rel_std_lvl[lvl - 1], fr'$h_{{ {i} }}$')
                ax.axhline(self.min_mesh_param, c='k',
                           ls='--', label=r'$h_0$ tolerance')
                ax.legend()
                ax.set_xlabel(r'$h$')
                ax.set_ylabel(
                    r'$\sigma(g_{h/2} - g_{h})/\sigma(g_h)$')
                ax.grid()
                fig.tight_layout()
                fig.savefig(self.name + '_rel_std_bias.pdf')
        self.print('Mesh hierarchy defined')
        self.print(f'L: {len(self.meshes)}, num. of elements.:{self.meshes}')
        return

    def compute_rates(self, estims, lvls, vs, cs):
        # weak rate of conv.

        # biases = np.abs(np.mean(levels[1:] - g_estim, axis=1))
        if len(self.meshes) > 1:
            hs = 1 / self.meshes
            biases = np.abs(estims[1:])
            z = np.polyfit(np.log(hs[1:]), np.log(biases), 1)
            # alpha
            weak_rate = z[0]
            # C_w
            weak_const = np.exp(z[1])
            # self.print(f'Weak conv.:{weak_const:.3e} h^{weak_rate:3f}')
            self.weak_params = [weak_const, weak_rate]

            # strong rate of conv.
            diff_squared = [lvl.evaluations**2 for lvl in lvls[1:]]
            strong = [np.mean(sec) for sec in diff_squared]
            # stat_err = np.mean((levels[1:] - g_estim)**2, axis=1)
            z = np.polyfit(np.log(hs[1:]), np.log(strong), 1)
            # beta
            strong_rate = z[0]
            # C_s
            strong_const = np.exp(z[1])
            # self.print(f'Strong conv.:{strong_const:.3e} h^{strong_rate:3f}')
            self.strong_params = [strong_const, strong_rate]

            # Cost analysis
            evals = [np.sum(it) for it in self.evals_mesh.values()]
            times = [np.sum(it) for it in self.times.values()]
            times = np.divide(times, evals)
            z = np.polyfit(np.log(hs), np.log(times), 1)
            # -d gamma
            cost_rate = z[0]
            # C_c
            cost_const = np.exp(z[1])
            self.cost_params = [cost_const, cost_rate]
            # self.print(f'Cost conv.:{cost_const:.3e} h^{cost_rate:3f}')
            # if self.plot:
                # fig, axs = plt.subplots(2, 1, figsize=(6, 6), sharex=True)
                # axs[0].loglog(hs[1:], biases, 'o-', label='weak conv.')
                # axs[0].loglog(hs[1:], weak_const * hs[1:] **
                #               weak_rate, '-.', label='Regression')
                # axs[0].set_ylabel(r'$E[|Q_h - Q|]$')
                # axs[0].grid()
                # axs[0].legend()
                # axs[1].loglog(hs[1:], strong, 'o-', label='strong conv.')
                # axs[1].loglog(hs[1:], strong_const * hs[1:] **
                #               strong_rate, '-.', label='Regression')
                # axs[1].set_ylabel(r'$E[(Q_h - Q)^2]$')
                # axs[1].grid()
                # axs[1].legend()
                # # axs[2].loglog(hs, times, 'o-', label='cost conv.')
                # # axs[2].loglog(hs, cost_const * hs**cost_rate,
                # #               '-.', label='Regression')
                # # axs[2].set_ylabel(r't (s)')
                # # axs[2].grid()
                # # axs[2].legend()
                # axs[1].set_xlabel(r'$h$')
                # fig.tight_layout()
                # fig.savefig(self.name + '_conv_rates.pdf')

                # works = []
                # tols = np.array([0.2, 0.1, 0.05]) * np.sum(estims)
                # for tol in tols:
                #     samp_sizes_opt = (tol**2*self.err_split)**-1 * \
                #         np.sum(np.sqrt(vs * cs)) * np.sqrt(vs / cs)
                #     samp_sizes_opt = np.ceil(samp_sizes_opt).astype('int')
                #     works.append((samp_sizes_opt*cs).sum())
                #
                # # rate_O2 = 1.1*works[0]*4**np.arange(len(tols))
                # # rate_O3 = 1.1*works[0]*6**np.arange(len(tols))
                # if strong_rate < 3:
                #     rate = -2-(3-strong_rate)/weak_rate
                # else:
                #     rate = -2
                # reg = 1.1*works[0]*(.5**rate)**np.arange(len(tols))
                # reg_O2 = 1.1*works[0]*(.5**-2)**np.arange(len(tols))
                # fig, ax = plt.subplots(figsize=(5, 5))
                # ax.loglog(tols, works, 'o--')
                # ax.loglog(tols, reg, 'o--', label=rf'$\mathcal{{O}}(TOL^{{ {rate:.2f} }})$')
                # # ax.loglog(tols, reg_O2, 'o--', label=rf'$\mathcal{{O}}(TOL^{{ {-2} }})$')
                # ax.set_title(rf'$\alpha={weak_rate:.2f}, \beta={strong_rate:.2f}, \gamma=3$')
                # ax.grid()
                # ax.legend()
                # ax.set_xlabel('TOL')
                # ax.set_ylabel('work')
                # fig.tight_layout()
                # fig.savefig(self.name + 'work_tol.pdf')
        # else:
            # warn('Strong and Weak convergence rates can not be '
            #               'computed because the hierarchy has size 1.')


# class MultiLevel():
#     def __init__(self, estim, sampler, covar_matrix, kl_contr=.9, rel_tol=0.1,
#                  bias_fraction=.5, h0=1 / 32, pilot_samp=100000, verb=False):
#         self.estim = estim
#         self.sampler = sampler
#         self.covar_matrix = covar_matrix
#         self.kl_contr = kl_contr
#         self.rel_tol = rel_tol
#         self.bias_fraction = bias_fraction
#         self.n_lvls = 6
#         self.levels = []
#         self.h0 = h0
#         self.samples = []
#         self.hs = []
#         self.levels = []
#         self.vs = []
#         self.ms = []
#         self.cs = []
#         self.weak_consts = []
#         self.strong_consts = []
#         self.cost_consts = []
#         self.pilot_samp = pilot_samp
#         self.domain = [0, 1]
#         self.verb = verb
#         if verb:
#             self.print = print
#         else:
#             self.print = lambda x: None
#
#     def __call__(self):
#         self.pilot_run()
#         # self.n_lvls =
#
#     def pilot_run(self):
#         np.random.seed(0)
#         hs = [self.h0 * 2**-(self.n_lvls - 1)]
#         n_el = int(np.ceil((self.domain[1] - self.domain[0]) / hs[0]))
#         samples = [self.sampler(self.pilot_samp, n_el)]
#         xs = np.linspace(self.domain[0], self.domain[1], n_el + 1)
#         C = self.covar_matrix(xs[:-1] + hs[0] / 2)
#         coefs = self.karhunen_loeve(C, samples[-1], n_el, self.pilot_samp)
#         t0 = time()
#         levels = [self.estim(samples[-1], h=hs[0], coefs=coefs)]
#         times = [(time() - t0) / self.pilot_samp]
#         for i in range(self.n_lvls - 2, -1, -1):
#             # samples.append(self.sampler(self.pilot_samp))
#             hs.append(self.h0 * 2**-i)
#             # xs = np.linspace(self.domain[0], self.domain[1], n_el*2**i+1)
#             # C = self.covar_matrix(xs[:-1]+hs[-1]/2)
#             # coefs = self.karhunen_loeve(C, samples[-1], n_el*2**i, self.pilot_samp)
#             stride = 2**(self.n_lvls - 1 - i)
#             t0 = time()
#             levels.append(self.estim(
#                 samples[-1], h=hs[-1], coefs=coefs[:, ::stride]))
#             times.append((time() - t0) / self.pilot_samp)
#
#         g_estim = np.mean(levels[0])
#         # weak rate of conv.
#         biases = np.abs(np.mean(levels[1:] - g_estim, axis=1))
#         z = np.polyfit(np.log(hs[1:]), np.log(biases), 1)
#         # alpha
#         weak_rate = z[0]
#         # C_w
#         weak_const = np.exp(z[1])
#         self.print(f'Weak conv.:{weak_const:.3e} h^{weak_rate:3f}')
#         self.weak_consts = [weak_const, weak_rate]
#
#         # strong rate of conv.
#         set_trace()
#         stat_err = np.mean((levels[1:] - g_estim)**2, axis=1)
#         z = np.polyfit(np.log(hs[1:]), np.log(stat_err), 1)
#         # beta
#         strong_rate = z[0]
#         # C_s
#         strong_const = np.exp(z[1])
#         self.print(f'Strong conv.:{strong_const:.3e} h^{strong_rate:3f}')
#         self.strong_consts = [strong_const, strong_rate]
#
#         # Cost analysis
#         z = np.polyfit(np.log(hs), np.log(times), 1)
#         # -d gamma
#         cost_rate = z[0]
#         # C_c
#         cost_const = np.exp(z[1])
#         self.cost_consts = [cost_const, cost_rate]
#         self.print(f'Cost conv.:{cost_const:.3e} h^{cost_rate:3f}')
#         fig, axs = plt.subplots(3, 1, figsize=(6, 6), sharex=True)
#         axs[0].loglog(hs[1:], biases, 'o-', label='weak conv.')
#         axs[0].loglog(hs[1:], weak_const * hs[1:] **
#                       weak_rate, '-.', label='Regression')
#         axs[0].set_ylabel(r'$E[|Q_h - Q|]$')
#         axs[0].grid()
#         axs[0].legend()
#         axs[1].loglog(hs[1:], stat_err, 'o-', label='strong conv.')
#         axs[1].loglog(hs[1:], strong_const * hs[1:] **
#                       strong_rate, '-.', label='Regression')
#         axs[1].set_ylabel(r'$E[(Q_h - Q)^2]$')
#         axs[1].grid()
#         axs[1].legend()
#         axs[2].loglog(hs, times, 'o-', label='cost conv.')
#         axs[2].loglog(hs, cost_const * hs**cost_rate, '-.', label='Regression')
#         axs[2].set_ylabel(r't (s)')
#         axs[2].grid()
#         axs[2].legend()
#         axs[2].set_xlabel(r'$h$')
#         fig.tight_layout()
#         fig.savefig('conv_rates.pdf')
#         set_trace()
#
#     def karhunen_loeve(self, C, w, n_el, mc_size):
#         eig_vals, eig_vecs = eig(C)
#
#         # N = np.minimum(N, np.sum(eig_vals > 0))
#
#         weights = (eig_vals**.5).real * np.max(np.abs(eig_vecs), axis=0)
#         weights[np.isnan(weights)] = 0
#
#         idxs = np.flip(np.argsort(weights))
#         weights_ = weights[idxs] / np.nansum(weights)
#         N = np.where(np.cumsum(weights_) > self.kl_contr)[0][0] + 1
#
#         eig_vals = eig_vals[idxs[:N]].real
#         eig_vecs_ = eig_vecs[:, idxs[:N]].real
#         # set_trace()
#         kappa = np.zeros((mc_size, n_el))
#         for i in range(mc_size):
#             for n in range(N):
#                 kappa[i, :] += eig_vals[n]**.5 * w[i, n] * eig_vecs_[:, n]
#         coefs = np.exp(kappa)
#         # cov = np.cov(coefs.T)
#         # cov_ = np.cov(kappa.T)
#         # print(w)
#         # print(kappa)
#         # plt.plot(eig_vecs_[:, 0])
#         # plt.show()
#         return coefs
