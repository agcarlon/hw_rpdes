import numpy as np
from functools import partial
from mlqmc import QuasiMonteCarlo, MultiLevelQuasi
from poisson_model import Poisson
import qmcpy as qp
from scipy.stats import norm

from IPython.core.debugger import set_trace


def force(x):
    return 4*np.pi**2*np.cos(2*np.pi*x)


model = Poisson(force, nu=2.5, max_mesh_param=80, n_jobs=78, kl_contr=.95)
model.get_karhunen_loeve_basis()


# def sampler(n, S=10):
#     n_ = 2**(np.ceil(np.log2(n)))
#     sobol = qp.Sobol(model.n_basis)
#     w = sobol.gen_samples(n_)[:int(n)]
#     xi = np.random.rand(S, model.n_basis)
#     X = [np.mod(w + xi_, 1) for xi_ in xi]
#     X = np.stack(X)
#     Y = norm.ppf(X)
#     # normal = qp.Gaussian(sobol, covariance=1)
#     # w = normal.gen_samples(n_)[:int(n)]
#     return Y


class Sampler():
    def __init__(self, mu=0, std=1, rands=10, seed=0):
        self.mu = mu
        self.std = std
        self.rands = rands
        self.seed = seed

    def sample(self, n):
        n_ = 2**(np.ceil(np.log2(n)))
        sobol = qp.Sobol(model.n_basis, seed=self.seed)
        w = sobol.gen_samples(n_)[:int(n)]
        xi = np.random.rand(self.rands, model.n_basis)
        X = [np.mod(w + xi_, 1) for xi_ in xi]
        X = np.stack(X)
        Y = norm.ppf(X)
        return self.mu + self.std * Y

    def __call__(self, n):
        return self.sample(n)

    def likelihood(self, x):
        return norm.pdf(x).prod(axis=1) \
            / (1 / self.std * norm.pdf((x - self.mu) / self.std)).prod(axis=1)

# samp = sampler(1024)
# Qs = model.solve(samp, mesh_param=128)

# sampler = Sampler(rands=10)

estimator = QuasiMonteCarlo(model.solve, Sampler, init_samp_size=256, outer_sample=10,
                           mesh_param=5, verb=True)
estimate = estimator.eval()
set_trace()
