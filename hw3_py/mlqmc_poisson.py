import numpy as np
from functools import partial
from mlqmc import MultiLevelQuasi, QuasiMonteCarlo
from poisson_model import Poisson
import qmcpy as qp
from scipy.stats import norm


from IPython.core.debugger import set_trace


nu = 2.5


def force(x):
    return 4 * np.pi**2 * np.cos(2 * np.pi * x)


model = Poisson(force, nu=nu, max_mesh_param=512)
model.get_karhunen_loeve_basis()


class Sampler():
    def __init__(self, mu=0, std=1, rands=10, seed=0):
        self.mu = mu
        self.std = std
        self.rands = rands
        self.seed = seed

    def sample(self, n):
        n_ = 2**(np.ceil(np.log2(n)))
        sobol = qp.Sobol(model.n_basis, seed=self.seed)
        w = sobol.gen_samples(n_)[:int(n)]
        xi = np.random.rand(self.rands, model.n_basis)
        X = [np.mod(w + xi_, 1) for xi_ in xi]
        X = np.stack(X)
        Y = norm.ppf(X)
        return self.mu + self.std * Y

    def __call__(self, n):
        return self.sample(n)

    def likelihood(self, x):
        return norm.pdf(x).prod(axis=1) \
            / (1 / self.std * norm.pdf((x - self.mu) / self.std)).prod(axis=1)

# samp = sampler(1024)
# Qs = model.solve(samp, mesh_param=128)

tol = 0.01
mesh_0 = 4
err_split = 0.5

np.random.seed(0)

name = f'mlqmc_nu{nu}_{int(tol*100)}'

mlmc = MultiLevelQuasi(qoi=model.solve, estim=QuasiMonteCarlo, sampler=Sampler,
                  init_samp_size=128, rel_tol=tol, verb=True, plot=True,
                  coarsest_mesh=5, err_split=err_split, min_mesh_param=0.8,
                  name=name, outer_sample=100)

# mlmc = MultiLevel(qoi=model.solve, estim=MonteCarlo, sampler=sampler,
#                   init_samp_size=64, rel_tol=tol, verb=True, plot=True)
mlmc.eval()
set_trace()
