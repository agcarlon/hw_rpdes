import numpy as np
from functools import partial
from mlmc import MultiLevel, MonteCarlo
from poisson_model import Poisson

from IPython.core.debugger import set_trace


def force(x):
    return 4*np.pi**2*np.cos(2*np.pi*x)


model = Poisson(force, nu=2.5, max_mesh_param=512)
model.get_karhunen_loeve_basis()


def sampler(n):
    return np.random.randn(n, model.n_basis)


samp = sampler(1024)
# Qs = model.solve(samp, mesh_param=128)

estimator = MonteCarlo(model.solve, sampler, init_samp_size=256)
estimate = estimator.eval()
set_trace()
