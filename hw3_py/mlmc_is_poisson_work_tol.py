import numpy as np
from functools import partial
from mlmc import MultiLevel, MonteCarlo
from poisson_model import Poisson
from scipy.stats import norm
from scipy.optimize import minimize
from scipy.optimize import NonlinearConstraint

import matplotlib.pyplot as plt

from IPython.core.debugger import set_trace

from time import time

# K = -20
# nu = 2.5


def mlmc_is_problem(K, nu, sample_size):
    def force(x):
        return 4 * np.pi**2 * np.cos(2 * np.pi * x)

    model = Poisson(force, nu=nu, max_mesh_param=320, n_jobs=78, kl_contr=.999)
    model.get_karhunen_loeve_basis()
    print(model.n_basis)

    def qoi(sample, mesh_param):
        p_m = model.solve(sample, mesh_param) < K
        return p_m

    # def sampler(n):
    #     return np.random.randn(n, model.n_basis)

    class Sampler():
        def __init__(self, mu=0, std=1):
            self.mu = mu
            self.std = std

        def sample(self, n):
            return self.mu + self.std * np.random.randn(n, model.n_basis)

        def __call__(self, n):
            return self.sample(n)

        def likelihood(self, x):
            return norm.pdf(x).prod(axis=1) \
                / (1 / self.std * norm.pdf((x - self.mu) / self.std)).prod(axis=1)

    sampler = Sampler()

    # np.random.seed(0)
    # samp = sampler(5)
    # model.solve(samp, mesh_param=160)

    # def get_is_mean_(K=-5, nu=2.5, mu=0, mesh=8):
    #     def objf_fun(w):
    #         p_m = qoi([w], mesh)
    #         objf = p_m * norm.pdf(w).prod()
    #         return -objf[0]
    #
    #     np.random.seed(0)
    #
    #     def get_starting_point(mu=0):
    #         mc_size = 128
    #         k = 0
    #         while True:
    #             # sobol = qp.Sobol(N, seed=k)
    #             # normal = qp.Gaussian(sobol, covariance=1)
    #             # w = normal.gen_samples(mc_size)
    #             w = sampler(mc_size)
    #             Qs = model.solve(w, mesh)
    #             if np.any(Qs < K):
    #                 idx = np.argmin(Qs)
    #                 x = w[idx]
    #                 print(k)
    #                 break
    #             k += 1
    #         return x
    #     x0 = get_starting_point(mu=mu)
    #     # x0 = np.array([-0.782,  1.446, -1.536, -0.148, -1.938])
    #     res = minimize(objf_fun, x0, method='Nelder-Mead')
    #     print(f'Finding mu*: {res.success}')
    #     # res = minimize(objf_fun, x0)
    #     mu = res.x
    #     return mu
    #
    #
    # def get_is_mean(K=-5, nu=2.5, mu=0, mesh=8):
    #     def objf_fun(w):
    #         p_m = qoi([w], mesh)
    #         objf = p_m*norm.pdf(w).prod()
    #         return -objf[0]
    #
    #     np.random.seed(0)

        # def get_starting_point(mu=0):
        #     k = 0
        #     step = 1 / 10
        #     w = sampler(1)
        #     Qs = [model.solve(w, mesh)]
        #     # print(Qs[-1])
        #     while True:
        #         # sobol = qp.Sobol(N, seed=k)
        #         # normal = qp.Gaussian(sobol, covariance=1)
        #         # w = normal.gen_samples(mc_size)
        #         # Qs = model.solve(w, mesh)
        #         dQ = model.gradient(w, mesh)
        #         w = w - step*dQ
        #         Qs.append(model.solve(w, mesh))
        #         # print(Qs[-1])
        #         if np.any(Qs[-1] < K):
        #             x = w
        #             break
        #         k += 1
        #     return x[0], Qs
        # x0, Qs = get_starting_point(mu=mu)
        # x0 = np.array([-0.782,  1.446, -1.536, -0.148, -1.938])

        class Log():
            def __init__(self):
                self.xs = []

            def __call__(self, xk):
                self.xs.append(xk)

        log = Log()
        x0 = sampler(1)[0]

        const_fun = lambda x: model.solve([x], mesh)
        const_jac = lambda x: model.gradient([x], mesh)
        const = NonlinearConstraint(const_fun, lb=-np.inf, ub=K, jac=const_jac)
        # objf_ = lambda x: -np.log(norm.pdf(x).prod())
        objf = lambda x: np.sum(x**2)
        objf_grad = lambda x: 2*x

        res = minimize(objf, x0, jac=objf_grad, constraints=const, callback=log)
        # res = minimize(objf_fun, x0, method='Nelder-Mead')
        print(f'Finding mu*: {res.success}')
        # xs = [x0] + log.xs
        xs = log.xs
        fs = np.array([np.linalg.norm(x) for x in xs])
        Qs = np.array([const_fun(x) for x in xs])

        fig, axs = plt.subplots(2, 1, figsize=(6, 5), sharex=True)
        axs[0].plot(Qs, 'o--', ms=3, label='Q')
        axs[0].axhline(K, ls='--', color='k', label='K')
        axs[0].set_title(rf'Finding $\mu^\star$, $\nu={nu}$, $K={-K}$')
        axs[0].legend()
        # axs[0].set_xlabel('iteration')
        axs[0].set_ylabel(r'$Q$')
        axs[0].grid()
        axs[1].plot(fs, 'o--', ms=3, label='SQP')
        axs[1].legend()
        axs[1].set_xlabel('iteration')
        axs[1].set_ylabel(r'$-\log (\rho)$')
        axs[1].grid()
        fig.tight_layout()
        fig.savefig(f'mu_star_nu{nu}_K{-K}_sqp.pdf')

        # res = minimize(objf_fun, x0)
        mu = res.x
        return mu

    # mu_8 = get_is_mean(K=K, nu=nu, mesh=8)
    t0 = time()
    mu = get_is_mean(K=K, nu=nu, mesh=10)
    print(f'Time to find mu*: {time() - t0}')

    # t0 = time()
    # mu_ = get_is_mean_(K=K, nu=nu, mesh=10)
    # print(time() - t0)
    # print(f'K={K}, nu={nu}, mu={mu[:4]}')
    # mu_32 = get_is_mean(K=K, nu=nu, mesh=32)
    # mu_64 = get_is_mean(K=K, nu=nu, mesh=64)

    # mu = np.zeros(model.n_basis)
    # if nu == 2.5:
    #     if K == -5:
    #         # mu[:4] = np.array([-1.452, -0.004, -1.342, -0.219])
    #         # mu[:4] = np.array([1.148807, 0.40883358, -1.64000834, 0])
    #         # mu[:4] = np.array([-1.38445804,  0.59592335, -1.31027396, -0.68933119])
    #         # mu[:4] = np.array([-1.36526474,  0.69725196, -1.45419447, -1.06027723])
    #         mu[:4] = np.array([-1.60227065,  0.48839168, -1.22987965, -0.76775705])
    #         mu[:2] = np.array([1.99351387 ,  0.07418603])
    #     if K == -10:
    #         mu[:4] = np.array([-1.841, -0.01, -1.92, -0.578])
    #     if K == -20:
    #         # mu[:4] = np.array([-2.102, -0.609, -2.706, 0.743])
    #         mu[:4] = np.array([-2.18150989,  0.12172546, -2.96710763,  0.65092737])
    # elif nu == 'inf':
    #     if K == -5:
    #         mu[:4] = np.array([-1.406, -0.05, -1.161, -0.023])
    #     if K == -10:
    #         mu[:4] = np.array([-2.227, 0.054, -1.198, -0.113])
    #     if K == -20:
    #         mu[:4] = np.array([-2.63, -0.147, -1.593, 1.321])

    mu[4:] = 0
    sampler_is = Sampler(mu=mu)

    # stds = []
    # for i in range(len(mu)):
    #     mu_ = np.copy(mu)
    #     mu_[-i:] = 0
    #     sampler_is = Sampler(mu=mu_)
    #
    #     samp = sampler_is(10000)
    #     Qs = model.solve(samp, mesh_param=128)
    #     Ps = (Qs<K)*sampler_is.likelihood(samp)
    #     stds.append(np.std(Ps, ddof=1))
    #     print(np.mean(Ps))
    #     print(stds[-1])

    samp = sampler_is(10000)
    Qs = model.solve(samp, mesh_param=10)
    Ps = (Qs < K) * sampler_is.likelihood(samp)
    print(np.mean(Ps))
    print(np.mean(Qs < K))
    print(np.std(Ps, ddof=1))

    tols = [0.05/.75**2, 0.05/0.75, 0.05]
    # tols = [.05]
    err_split = 0.5
    # meshes = [16, 32, 64]
    # meshes = [10, 20, 40]
    works = []
    works_is = []
    estims = []
    estims_is = []
    vls = []
    vls_is = []
    for tol in tols:
        print(f'Running MLMC IS with K:{K}, nu:{nu}, tol:{tol}')
        name = f'nu{nu}_K{-K}/mlmc_is_nu{nu}_K{-K}_IS'
        mlmc_is = MultiLevel(qoi=qoi, estim=MonteCarlo, sampler=sampler_is, err_split=err_split,
                             init_samp_size=sample_size, rel_tol=tol, verb=True, plot=True,
                             name=name, min_mesh_param=1., coarsest_mesh=5, mesh_conv_samp_size=100000)

        # mlmc_is.eval(meshes=meshes)
        mlmc_is.eval(seed=0)
        vls_is.append(mlmc_is.vl)
        estims_is.append(mlmc_is.estimate)
        works_is.append(mlmc_is.works[-1])
        name = f'nu{nu}_K{-K}/mlmc_is_nu{nu}_K{-K}'
        mlmc = MultiLevel(qoi=qoi, estim=MonteCarlo, sampler=sampler, err_split=err_split,
                          init_samp_size=sample_size, rel_tol=tol, verb=True, plot=False,
                          name=name)
        # mlmc.eval(meshes=meshes)  # mlmc_is.meshes
        mlmc.eval(meshes=mlmc_is.meshes, seed=0)  # mlmc_is.meshes
        vls.append(mlmc.vl)
        estims.append(mlmc.estimate)
        works.append(mlmc.works[-1])

    # rate_O2 = 1.1*works[0]*4**np.arange(len(tols))
    # rate_O3 = 1.1*works[0]*6**np.arange(len(tols))
    # strong_rate = mlmc_is.strong_params[1]
    # weak_rate = mlmc_is.weak_params[1]

    z = np.polyfit(np.log(tols), np.log(works), 1)
    real_rate = z[0]
    real_const = np.exp(z[1])
    # print(f'Real conv.:{real_const:.3e} h^{real_rate:3f}')


    strong_rate = mlmc_is.strong_rate
    weak_rate = mlmc_is.weak_rate
    # strong_rate = 3
    # weak_rate = 2
    if strong_rate < 3:
        rate = -2 - (3 - strong_rate) / weak_rate
    else:
        rate = -2
    reg = 1.1 * works[0] * (.75**rate)**np.arange(len(tols))

    fig, ax = plt.subplots(figsize=(5, 5))
    ax.loglog(tols, works, 'o--', label='MLMC')
    ax.loglog(tols, works_is, 'o--', label='MLMC-IS')
    ax.loglog(tols, reg, 'o--', label=rf'$\mathcal{{O}}(TOL^{{ {rate:.2f} }})$')
    # ax.loglog(tols, reg_O2, 'o--', label=rf'$\mathcal{{O}}(TOL^{{ {-2} }})$')
    ax.set_title(rf'$\alpha={weak_rate:.2f}, \beta={strong_rate:.2f}, \gamma=3$')
    ax.set_xlabel('TOL')
    ax.set_ylabel('work')

    # ax.loglog(tols, rate_O2, 'o--', label=r'$\mathcal{O}(TOL^{-2})$')
    # ax.loglog(tols, rate_O3, 'o--', label=r'$\mathcal{O}(TOL^{-3})$')

    ax.grid()
    ax.legend()
    fig.tight_layout()
    fig.savefig(f'nu{nu}_K{-K}/is_work_tol_K{-K}_nu{nu}.pdf')

    fig, ax = plt.subplots(figsize=(5, 5))
    var_tol = err_split**2 * tol**2 / 1.65**2
    ax.axhline(np.sqrt(var_tol), c='k', ls='--', label=r'tol')
    ax.loglog(mlmc.works, np.sqrt(mlmc.rel_vars), 'o--', label='MLMC')
    ax.loglog(mlmc_is.works, np.sqrt(mlmc_is.rel_vars), 'o--', label='MLMC-IS')
    ax.legend()
    ax.grid()
    ax.set_xlabel('work')
    ax.set_ylabel('rel. std. error')
    fig.tight_layout()
    fig.savefig(f'nu{nu}_K{-K}/is_nu{nu}_K{-K}_std_err.pdf')

    mlmc_samp = np.array(mlmc.samp_sizes[1:])
    mlmc_is_samp = np.array(mlmc_is.samp_sizes[1:])

    # %%
    fig, ax = plt.subplots(figsize=(5, 5))
    colors = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5'][:len(mlmc.samp_sizes[0])]
    for i, smp in enumerate(mlmc_samp.T):
        ax.semilogy(smp, 'o--', c=colors[i])
    for i, smp in enumerate(mlmc_is_samp.T):
        ax.semilogy(smp, 's-.', c=colors[i])

    plts = [
        plt.semilogy([], [], 'ko--', label='MLMC'),
        plt.semilogy([], [], 'ks-.', label='MLMC-IS')
    ]

    plts_cs = [
        plt.semilogy([], [], f'{C}', label=f'$M_{i}$') for i, C in enumerate(colors)
    ]

    # legend1 = plt.legend(plts, loc=1)
    ax.legend()
    # ax.add_artist(legend1)

    ax.legend()
    ax.grid()
    ax.set_xlabel('steps')
    ax.set_ylabel('sample sizes')
    fig.tight_layout()
    fig.savefig(f'nu{nu}_K{-K}/is_nu{nu}_K{-K}_samp_sizes.pdf')

    fig, ax = plt.subplots(figsize=(5, 5))
    ax.semilogy(vls[-1], 'o--', label='MLMC')
    ax.semilogy(vls_is[-1], 'o--', label='MLMC-IS')
    ax.legend()
    ax.grid()
    ax.set_xlabel(r'$\ell$')
    ax.set_ylabel(r'$V_\ell$')
    fig.tight_layout()
    fig.savefig(f'nu{nu}_K{-K}/is_nu{nu}_K{-K}_vls.pdf')

    plt.close('all')


mlmc_is_problem(K=-5, nu=2.5, sample_size=2**5)
mlmc_is_problem(K=-10, nu=2.5, sample_size=2**5)
mlmc_is_problem(K=-20, nu=2.5, sample_size=2**5)

mlmc_is_problem(K=-5, nu='inf', sample_size=2**5)
mlmc_is_problem(K=-10, nu='inf', sample_size=2**5)
mlmc_is_problem(K=-20, nu='inf', sample_size=2**5)
