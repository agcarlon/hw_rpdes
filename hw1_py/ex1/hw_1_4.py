import numpy as np
from numpy.linalg import norm, eig, det, inv
import matplotlib.pyplot as plt
from IPython.core.debugger import set_trace
from scipy.stats import norm
from scipy.optimize import minimize


def make_contours(K=3):
    def fun(x1, x2):
        return 1/(2*np.pi)*np.maximum(np.exp(x1)+np.exp(x2) - K, 0)*np.exp(-(x1**2 + x2**2)/2)

    def inner_fun(x1, x2):
        return np.maximum(np.exp(x1)+np.exp(x2) - K, 0)

    def gaussian(x1, x2):
        return 1/(2*np.pi)*np.exp(-((x1-1)**2 + (x2-1)**2)/2)

    domain_x1 = [-4, 4]
    domain_x2 = [-4, 4]

    n_mesh = 100

    x1 = np.linspace(domain_x1[0], domain_x1[1], n_mesh)
    x2 = np.linspace(domain_x2[0], domain_x2[1], n_mesh)

    integrand = np.zeros((n_mesh, n_mesh))
    is_dist = np.zeros((n_mesh, n_mesh))

    for i in range(n_mesh):
        for j in range(n_mesh):
            integrand[i, j] = fun(x1[j], x2[i])
            # integrand_max[i, j] = inner_fun(x1[j], x2[i])
            is_dist[i, j] = gaussian(x1[j], x2[i])



    fig, ax = plt.subplots(1, 1, figsize=(8,6))
    cont = ax.contourf(x1, x2, integrand)
    ax.set_title(f'Exercise 1.4.4, K={K}')
    ax.set_xlabel('x1')
    ax.set_ylabel('x2')
    fig.colorbar(cont)
    fig.tight_layout()

    # cont_ = ax[1].contourf(x1, x2, is_dist)
    # ax[1].set_title(f'K={K}')
    # ax[1].set_xlabel('x1')
    # ax[1].set_ylabel('x2')

    # fig.colorbar(cont)
    # fig.colorbar(cont_)
    fig.savefig(f'hw_1_4_integrand_{K}_.pdf')


    # def fun_inner(x1, x2):
    #     return np.maximum(np.exp(x1)+np.exp(x2) - K, 0)

    # integrand = np.zeros((n_mesh, n_mesh))
    #
    # for i in range(n_mesh):
    #     for j in range(n_mesh):
    #         integrand[i, j] = fun_inner(x1[j], x2[i])


    # fig, ax = plt.subplots(figsize=(6,6))
    # cont = ax.contourf(x1, x2, integrand)
    # ax.set_title(f'K={K}')
    # ax.set_xlabel('x1')
    # ax.set_ylabel('x2')
    # fig.colorbar(cont)
    # fig.savefig(f'hw_1_4_integrand_max_{K}.pdf')

# make_contours(K=3)
# make_contours(K=6)


def monte_carlo(fun, xs):
    f_x = []
    # f_x_av = []
    # for x in xs:
        # f_x.append(fun(*x))
        # f_x_av.append(np.mean(f_x))
    f_x = fun(xs[:,0], xs[:,1])
    f_x_av = np.cumsum(f_x) / np.arange(1, len(xs)+1)
    return f_x, f_x_av


def make_monte_carlo_conv(K=3):
    # Here, we use naive Monte Carlo to estimate the integral

    def fun(x1, x2):
        return 1/(2*np.pi)*np.maximum(np.exp(x1)+np.exp(x2) - K, 0)*np.exp(-(x1**2 + x2**2)/2)

    def fun_inner(x1, x2):
        return np.maximum(np.exp(x1)+np.exp(x2) - K, 0)

    M = int(1e7)
    np.random.seed(0)
    xs = np.random.randn(M, 2)
    f_x, f_x_av = monte_carlo(fun_inner, xs)
    print(np.mean(f_x))
    print(f'std of f:{np.std(f_x)}')

    # Obtained numerically running until M=1e9
    if K == 3:
        true_val = 1.0912555035565639
    elif K == 6:
        true_val = 0.4176026244100602
    alpha = .01

    std_f_x = np.std(f_x)
    clt_constant = norm.ppf(1 - alpha/2)*std_f_x

    M_1 = 1
    M_2 = M

    err_1 = clt_constant/np.sqrt(M_1)
    err_2 = err_1*((M_2/M_1)**(-1/2))

    Ms = np.arange(1, M+1)
    stat_err = np.abs(f_x_av - true_val)

    # z = np.polyfit(np.log(Ms[1000:]), np.log(stat_err[1000:]), 1)
    # print(f'Linear regr, K={K}: rate:{z[0]}, const.:{np.exp(z[1])}')
    # poly = np.poly1d(z)
    # regr = np.exp(poly(np.log(Ms)))

    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(stat_err)
    # ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$\mathcal{O}(N^{-1/2})$')
    ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$C_\alpha \sigma M^{-1/2}$')
    # ax.loglog(Ms[1000:], regr[1000:], 'r--', label='Regression')
    ax.grid()
    ax.set_xlabel('Sample size')
    ax.set_ylabel(r'$|\hat{f} - E[f]|$')
    ax.legend()
    ax.set_title(f'Excercise 1.4.4, K={K}')
    fig.tight_layout()
    fig.savefig(f'Ex_1_4_4_K{K}.pdf')
    return std_f_x


def make_is_monte_carlo_conv(std_f_x, K=3):
    # Now with the shift dilation importance sampling
    # First we need to find the max of fun

    def fun(x1, x2):
        return 1/(2*np.pi)*np.maximum(np.exp(x1)+np.exp(x2) - K, 0)*np.exp(-(x1**2 + x2**2)/2)

    res = minimize(lambda x: -fun(x[0], x[1])**2, [0, 2])
    # The mode of (g rho)^2 is res.x
    mode = res.x
    print(mode)
    # mode = [1, 1]
    sig1, sig2 = 1.0, 1.0
    # sig1, sig2 = std_f_x, std_f_x
    # mode = np.zeros(2)
    # sigma = np.eye(2)*(np.exp(mode[0])+np.exp(mode[1]) - K)**2
    # sigma = np.eye(2)*std_f_x**2
    # sigma = np.eye(2)*1.
    # inv_sigma = inv(sigma)
    # det_sigma = det(sigma)

    def fun_inner(x1, x2):
        return np.maximum(np.exp(x1)+np.exp(x2) - K, 0)

    # def bivar_gaussian_pdf(x1, x2, mu, inv_sigma, det_sigma):
    #     x = np.array([x1, x2]).T
    #     res = x - mu[np.newaxis, :]
    #     return (2*np.pi)**(-1)*det_sigma**(-1/2)*np.exp(-1/2*((res @ inv_sigma)*res).sum(axis=1))
    #
    # def radon_nikodym(x1, x2):
    #     return bivar_gaussian_pdf(x1, x2, np.zeros(2), np.eye(2), 1) / bivar_gaussian_pdf(x1, x2, mode, inv_sigma, det_sigma)
    #
    # def fun_is(x1, x2):
    #     return fun_inner(x1, x2) * radon_nikodym(x1, x2)

    def fun_is(x1, x2):
        return fun_inner(x1, x2) * np.exp(-(x1**2 + x2**2)/2) / (np.exp(-((x1-mode[0])**2/sig1**2 + (x2-mode[1])**2/sig2**2)/2)/(sig1*sig2))

    M = int(1e7)
    np.random.seed(0)
    # xs = np.random.randn(M, 2) + mode
    # xs = np.random.multivariate_normal(mode, sigma, size=M)
    xs = np.random.multivariate_normal(mode, np.diag([sig1**2, sig2**2]), size=M)
    f_x, f_x_av = monte_carlo(fun_is, xs)
    print(np.mean(f_x))
    print(f'std of f:{np.std(f_x)}')
    # Obtained numerically running until M=1e9
    if K == 3:
        true_val = 1.0912555035565639
    elif K == 6:
        true_val = 0.4176026244100602

    alpha = .01

    M_1 = 1
    M_2 = M

    std_f_x_is = np.std(f_x)
    clt_constant_is = norm.ppf(1 - alpha)*std_f_x_is

    err_1_is = clt_constant_is/np.sqrt(M_1)
    err_2_is = err_1_is*((M_2/M_1)**(-1/2))

    clt_constant = norm.ppf(1 - alpha)*std_f_x

    err_1 = clt_constant/np.sqrt(M_1)
    err_2 = err_1*((M_2/M_1)**(-1/2))

    Ms = np.arange(1, M+1)
    stat_err = np.abs(f_x_av - true_val)

    # z = np.polyfit(np.log(Ms), np.log(stat_err), 1)
    # z = np.polyfit(np.log(Ms[1000:]), np.log(stat_err[1000:]), 1)
    # print(f'Linear regr, IS, K={K}: rate:{z[0]}, const.:{np.exp(z[1])}')
    # poly = np.poly1d(z)
    # regr = np.exp(poly(np.log(Ms)))

    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(stat_err)
    # ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$\mathcal{O}(N^{-1/2})$')
    ax.loglog([M_1, M_2],[err_1_is, err_2_is], 'r--', label=r'$C_\alpha \sigma_{IS} M^{-1/2}$')
    ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$C_\alpha \sigma M^{-1/2}$')
    # ax.loglog(Ms[1000:], regr[1000:], 'r--', label='Regression')
    ax.grid()
    ax.set_xlabel('Sample size')
    ax.set_ylabel(r'$|\hat{f} - E[f]|$')
    ax.legend()
    ax.set_title(f'Excercise 1.4.4, K={K}, Importance Sampling')
    fig.tight_layout()
    fig.savefig(f'Ex_1_4_4_K{K}_is.pdf')


std_f_x = make_monte_carlo_conv(K=3)
make_is_monte_carlo_conv(std_f_x, K=3)

std_f_x = make_monte_carlo_conv(K=6)
make_is_monte_carlo_conv(std_f_x, K=6)
