import numpy as np
from IPython.core.debugger import set_trace
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.special import erf

M = int(1e7)


def monte_carlo(fun, xs):
    # f_x = []
    # f_x_av = []
    # for x in xs:
        # f_x.append(fun(x))
        # f_x_av.append(np.mean(f_x))
    f_x = fun(xs)
    f_x_av = np.cumsum(f_x) / np.arange(1, len(xs)+1)
    return f_x, f_x_av


def fun_1(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 9/N
    return np.cos(2*np.pi*w_1 + c_n*np.sum(x, axis=1))


def fun_2(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 7.25/N
    return np.prod((c_n**(-2) + (x - w_1)**2)**(-1), axis=1)


def fun_3(x):
    # N = len(x)
    N = np.shape(x)[1]
    c_n = 1.85/N
    return (1+c_n*np.sum(x, axis=1))**(-N+1)


def fun_4(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 7.03/N
    return np.exp(-np.sum(c_n**2*(x - w_1)**2, axis=1))


def fun_5(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 2.04/N
    return np.exp(-np.sum(c_n*np.abs(x - w_1), axis=1))


def fun_6(x):
    N = np.shape(x)[1]
    c_i = 4.3/N
    w_1 = np.pi/4
    w_2 = np.pi/5
    mask = (x[:, 0] > w_1) | (x[:, 1] > w_2)
    output = np.exp(c_i*np.sum(x, axis=1))
    output[mask] = 0
    return output


def solve_prob(N, fun, exact, name, ex_number):
    np.random.seed(0)
    # xs = np.random.rand(M, N)
    xs = np.random.rand(M, N)

    f_x, f_x_av = monte_carlo(fun, xs)

    M_1 = 1
    M_2 = M

    alpha = .1

    std_f_x = np.std(f_x)
    # clt_constant = norm.ppf(1 - (1-alpha)/2)*std_f_x
    clt_constant = norm.ppf(1 - alpha/2)*std_f_x

    err_1 = clt_constant/np.sqrt(M_1)
    err_2 = err_1*((M_2/M_1)**(-1/2))

    Ms = np.arange(1, M+1)
    clt = err_1*(Ms**(-1/2))

    stat_err = np.abs(f_x_av - exact)

    Ms = np.arange(1, M+1)
    clt = err_1*(Ms**(-1/2))
    print(np.mean(stat_err < clt))

    z = np.polyfit(np.log(Ms), np.log(stat_err), 1)
    print(f'Linear regr, N={N}: rate:{z[0]}, const.:{np.exp(z[1])}')
    poly = np.poly1d(z)
    regr = np.exp(poly(np.log(Ms)))

    # set_trace()

    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(stat_err)
    ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$C_\alpha \sigma M^{-1/2}$')
    # ax.loglog(Ms, regr, 'r--', label='Regression')
    ax.grid()
    ax.set_xlabel('Sample size')
    ax.set_ylabel(r'$|\hat{f} - E[f]|$')
    ax.legend()
    ax.set_title(f'{name}, N={N}')
    fig.tight_layout()
    fig.savefig(f'fun_ex{ex_number}_N{N}_clt.pdf')
    # set_trace()

    # For the same confidence 1-alpha



    # X = np.random.randn(int(1e6), 100)
    # Ms_ = np.arange(1, int(1e6)+1)
    # X_ = np.cumsum(X, axis=0)/Ms_[:, np.newaxis]
    # np.mean(np.abs(X)<norm.ppf(1-alpha/2), axis=1)
    # np.mean(np.abs(X_)*np.sqrt(Ms_)[:, np.newaxis]<norm.ppf(1-alpha/2), axis=1)
    # np.std(np.abs(X_)*np.sqrt(Ms_), axis=1)

    # set_trace()
    # plt.show()
# #

def solve_prob_be(N, fun, exact, name, ex_number):
    np.random.seed(0)
    # xs = np.random.rand(M, N)
    xs = np.random.rand(M, N)

    f_x, f_x_av = monte_carlo(fun, xs)

    M_1 = 1
    M_2 = M

    alpha = .001

    std_f_x = np.std(f_x)
    # clt_constant = norm.ppf(1 - (1-alpha)/2)*std_f_x
    clt_constant = norm.ppf(1 - alpha/2)*std_f_x

    err_1_clt = clt_constant/np.sqrt(M_1)
    err_2_clt = err_1_clt*((M_2/M_1)**(-1/2))

    Ms = np.arange(1, M+1)
    clt = err_1_clt*(Ms**(-1/2))

    stat_err = np.abs(f_x_av - exact)

    Ms = np.arange(1, M+1)
    clt = err_1_clt*(Ms**(-1/2))
    # print(np.mean(stat_err < clt))

    C_be = 30.51175
    third_mom = (np.mean(np.abs(f_x - f_x.mean())**3))**(1/3)/std_f_x
    c0 = ((2*C_be*third_mom**3)/(alpha*np.sqrt(Ms)))**(1/3) - 1
    # set_trace()
    berry_esseen = c0*std_f_x*(Ms**(-1/2))

    # z = np.polyfit(np.log(Ms), np.log(stat_err), 1)
    # print(f'Linear regr, N={N}: rate:{z[0]}, const.:{np.exp(z[1])}')
    # poly = np.poly1d(z)
    # regr = np.exp(poly(np.log(Ms)))

    # set_trace()

    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(stat_err)
    ax.loglog([M_1, M_2],[err_1_clt, err_2_clt], 'k--', label=r'$C_\alpha \sigma M^{-1/2}$')
    # ax.loglog(Ms, regr, 'r--', label='Regression')
    ax.loglog(Ms, berry_esseen, 'k-.', label=r'$c_0(M) \sigma M^{-1/2}$')
    ax.grid()
    ax.set_xlabel('Sample size')
    ax.set_ylabel(r'$|\hat{f} - E[f]|$')
    ax.legend()
    ax.set_title(f'{name}, N={N}')
    fig.tight_layout()
    fig.savefig(f'fun_ex{ex_number}_N{N}_be.pdf')


name = 'Function 1 - Oscillatory'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 9/N
    exact_f1 = np.real(np.exp(2*np.pi*1j*w_1)*(1/(1j*c_n)*(np.exp(1j*c_n)-1))**N)
    solve_prob_be(N, fun_1, exact_f1, name, ex_number=1)
    solve_prob(N, fun_1, exact_f1, name, ex_number=1)


name = 'Function 2 - Product peak'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 7.25/N
    exact_f2 = (c_n*(np.arctan(c_n*(1-w_1)) + np.arctan(c_n*w_1)))**N
    solve_prob_be(N, fun_2, exact_f2, name, ex_number=2)
    solve_prob(N, fun_2, exact_f2, name, ex_number=2)


name = 'Function 3 - Corner peak'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 2.04/N
    # Obtained by QMC with M=2e6
    if N == 2:
        exact_f3 = 0.5415856698158932  # 0.5415830265945079 # M=1e9, seed ?
    elif N == 20:
        exact_f3 = 8.678102872197777e-06  # 8.686690333187793e-06
    solve_prob_be(N, fun_2, exact_f2, name, ex_number=3)
    solve_prob(N, fun_2, exact_f2, name, ex_number=3)


name = 'Function 4 - Gaussian'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 7.03/N
    exact_f4 = (np.sqrt(np.pi)/(2*c_n) * (erf(c_n*(1-w_1)) + erf(c_n*w_1)))**N
    solve_prob_be(N, fun_4, exact_f4, name, ex_number=4)
    solve_prob(N, fun_4, exact_f4, name, ex_number=4)


name = 'Function 5 - Continuous'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 2.04/N
    exact_f5 = (1/c_n*(2 - np.exp(-c_n*w_1) - np.exp(-c_n*(1-w_1))))**N
    solve_prob_be(N, fun_5, exact_f5, name, ex_number=5)
    solve_prob(N, fun_5, exact_f5, name, ex_number=5)


name = 'Function 6 - Discontinuous'
Ns = [2, 20]
for N in Ns:
    w_1 = np.pi/4
    w_2 = np.pi/5
    c_n = 4.3/N
    exact_f6 = c_n**-N*(np.exp(c_n*w_1) - 1) * \
        (np.exp(c_n*w_2) - 1)*(np.exp(c_n)-1)**np.maximum(N-2, 0)
    solve_prob_be(N, fun_6, exact_f6, name, ex_number=6)
    solve_prob(N, fun_6, exact_f6, name, ex_number=6)

# Here, we check the percentile of the times that MC exceeds the CLT bound,
# i.e., we check if the probability 1-alpha holds

def solve_prob_1_clt(N, reps):
    f_x_av = np.zeros((reps, M))
    for rep in range(reps):
        np.random.seed(rep)
        xs = np.random.rand(M, N)
        f_x, f_x_av[rep] = monte_carlo(fun_1, xs)
    w_1 = .5
    c_n = 9/N
    exact_f1 = np.real(np.exp(2*np.pi*1j*w_1)*(1/(1j*c_n)*(np.exp(1j*c_n)-1))**N)

    M_1 = 1
    M_2 = M

    alpha = .1

    std_f_x = np.std(f_x)
    # clt_constant = norm.ppf(1 - (1-alpha)/2)*std_f_x
    clt_constant = norm.ppf(1 - alpha/2)*std_f_x

    err_1 = clt_constant/np.sqrt(M_1)
    err_2 = err_1*((M_2/M_1)**(-1/2))

    Ms = np.arange(1, M+1)

    clt = err_1*(Ms**(-1/2))
    # vio = np.mean(np.abs(f_x_av - exact_f1)*np.sqrt(Ms)[np.newaxis,:]/std_f_x < norm.ppf(1-alpha/2), axis=0)

    percentiles = np.mean(np.abs(f_x_av - exact_f1) < clt, axis=0)

    third_mom = (np.mean(np.abs(f_x - f_x.mean())**3))**(1/3)/std_f_x

    berry_esseen = 1 - alpha - 2*30.51175*third_mom**3/((1+1.65)**3*np.sqrt(Ms))

    fig, ax = plt.subplots(figsize=(6, 4))
    ax.plot(percentiles, label=r'$P(|\mathcal{E})| < C_\alpha \sigma M^{-1/2})$')
    ax.plot([1, M], [1-alpha, 1-alpha], '--', label='CLT')
    ax.plot(Ms, berry_esseen, '--', label='Berry-Esseen')
    # ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$\mathcal{O}(N^{-1/2})$')
    # ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$C_\alpha \sigma M^{-1/2}$')
    ax.grid()
    ax.set_xlabel('Sample size')
    ax.set_ylabel(r'Probability')
    top = np.minimum(1., ax.get_ylim()[1])
    ax.set_ylim(top=top, bottom=.85)
    ax.legend()
    ax.set_title(f'Function 1 - Oscillatory, N={N}, Repetitions={reps}')
    fig.tight_layout()
    fig.savefig(f'fun_1_{N}_berry_esseen.pdf')


# solve_prob_1_clt(2, 10000)
# solve_prob_1_clt(7, 10000)
# solve_prob_1_clt(20, 10000)
