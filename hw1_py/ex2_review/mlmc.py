import numpy as np
from numpy.linalg import eig, norm, det, solve
import matplotlib.pyplot as plt
from time import time

from IPython.core.debugger import set_trace


class mlmc():
    def __init__(self, estim, sampler, covar_matrix, kl_contr=.9, rel_tol=0.1,
                 bias_fraction=.5, h0=1/32, pilot_samp=100000, verb=False):
        self.estim = estim
        self.sampler = sampler
        self.covar_matrix = covar_matrix
        self.kl_contr = kl_contr
        self.rel_tol = rel_tol
        self.bias_fraction = bias_fraction
        self.n_lvls = 6
        self.levels = []
        self.h0 = h0
        self.samples = []
        self.hs = []
        self.levels = []
        self.vs = []
        self.ms = []
        self.cs = []
        self.weak_consts = []
        self.strong_consts = []
        self.cost_consts = []
        self.pilot_samp = pilot_samp
        self.domain = [0, 1]
        self.verb = verb

        if verb:
            self.print = print
        else:
            self.print = lambda x: None

    def __call__(self):
        self.pilot_run()
        # self.n_lvls =

    def pilot_run(self):
        np.random.seed(0)
        hs = [self.h0*2**-(self.n_lvls-1)]
        n_el = int(np.ceil((self.domain[1] - self.domain[0])/hs[0]))
        samples = [self.sampler(self.pilot_samp, n_el)]
        xs = np.linspace(self.domain[0], self.domain[1], n_el+1)
        C = self.covar_matrix(xs[:-1]+hs[0]/2)
        coefs = self.karhunen_loeve(C, samples[-1], n_el, self.pilot_samp)
        t0 = time()
        levels = [self.estim(samples[-1], h=hs[0], coefs=coefs)]
        times = [(time() - t0) / self.pilot_samp]
        for i in range(self.n_lvls-2, -1, -1):
            # samples.append(self.sampler(self.pilot_samp))
            hs.append(self.h0 * 2**-i)
            # xs = np.linspace(self.domain[0], self.domain[1], n_el*2**i+1)
            # C = self.covar_matrix(xs[:-1]+hs[-1]/2)
            # coefs = self.karhunen_loeve(C, samples[-1], n_el*2**i, self.pilot_samp)
            stride = 2**(self.n_lvls - 1 - i)
            t0 = time()
            levels.append(self.estim(samples[-1], h=hs[-1], coefs=coefs[:, ::stride]))
            times.append((time() - t0) / self.pilot_samp)

        g_estim = np.mean(levels[0])
        # weak rate of conv.
        biases = np.abs(np.mean(levels[1:] - g_estim, axis=1))
        z = np.polyfit(np.log(hs[1:]), np.log(biases), 1)
        # alpha
        weak_rate = z[0]
        # C_w
        weak_const = np.exp(z[1])
        self.print(f'Weak conv.:{weak_const:.3e} h^{weak_rate:3f}')
        self.weak_consts = [weak_const, weak_rate]

        # strong rate of conv.
        stat_err = np.mean((levels[1:] - g_estim)**2, axis=1)
        z = np.polyfit(np.log(hs[1:]), np.log(stat_err), 1)
        # beta
        strong_rate = z[0]
        # C_s
        strong_const = np.exp(z[1])
        self.print(f'Strong conv.:{strong_const:.3e} h^{strong_rate:3f}')
        self.strong_consts = [strong_const, strong_rate]

        # Cost analysis
        z = np.polyfit(np.log(hs), np.log(times), 1)
        # -d gamma
        cost_rate = z[0]
        # C_c
        cost_const = np.exp(z[1])
        self.cost_consts = [cost_const, cost_rate]
        self.print(f'Cost conv.:{cost_const:.3e} h^{cost_rate:3f}')
        fig, axs = plt.subplots(3, 1, figsize=(6, 6), sharex=True)
        axs[0].loglog(hs[1:], biases, 'o-', label='weak conv.')
        axs[0].loglog(hs[1:], weak_const*hs[1:]**weak_rate, '-.', label='Regression')
        axs[0].set_ylabel(r'$E[|Q_h - Q|]$')
        axs[0].grid()
        axs[0].legend()
        axs[1].loglog(hs[1:], stat_err, 'o-', label='strong conv.')
        axs[1].loglog(hs[1:], strong_const*hs[1:]**strong_rate, '-.', label='Regression')
        axs[1].set_ylabel(r'$E[(Q_h - Q)^2]$')
        axs[1].grid()
        axs[1].legend()
        axs[2].loglog(hs, times, 'o-', label='cost conv.')
        axs[2].loglog(hs, cost_const*hs**cost_rate, '-.', label='Regression')
        axs[2].set_ylabel(r't (s)')
        axs[2].grid()
        axs[2].legend()
        axs[2].set_xlabel(r'$h$')
        fig.tight_layout()
        fig.savefig('conv_rates.pdf')
        set_trace()

    def karhunen_loeve(self, C, w, n_el, mc_size):
        eig_vals, eig_vecs = eig(C)

        # N = np.minimum(N, np.sum(eig_vals > 0))

        weights = (eig_vals**.5).real * np.max(np.abs(eig_vecs), axis=0)
        weights[np.isnan(weights)] = 0

        idxs = np.flip(np.argsort(weights))
        weights_ = weights[idxs] / np.nansum(weights)
        N = np.where(np.cumsum(weights_) > self.kl_contr)[0][0] + 1

        eig_vals = eig_vals[idxs[:N]].real
        eig_vecs_ = eig_vecs[:, idxs[:N]].real
        # set_trace()
        kappa = np.zeros((mc_size, n_el))
        for i in range(mc_size):
            for n in range(N):
                kappa[i, :] += eig_vals[n]**.5 * w[i, n] * eig_vecs_[:, n]
        coefs = np.exp(kappa)
        # cov = np.cov(coefs.T)
        # cov_ = np.cov(kappa.T)
        # print(w)
        # print(kappa)
        # plt.plot(eig_vecs_[:, 0])
        # plt.show()
        return coefs
