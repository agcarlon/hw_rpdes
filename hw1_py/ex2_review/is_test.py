import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

K = -4
mc_size = 1000000

realP = norm.cdf(K)
realP

# %%
X = np.random.randn(mc_size)
P = np.mean(X<K)
P

# %%
mu = K
sigma = 1
Y = X*sigma + mu
mc = X<K
P_ = np.mean(mc)
P_

# %%
lkls = norm.pdf(Y)/(1/sigma*norm.pdf((Y - mu)/sigma))
mcis = (Y<K)*lkls
Pis = np.mean(mcis)
Pis


# %%
print(np.std(mcis))
print(np.mean(Y<K))
print(np.std(mc))

# %%

ms = np.arange(1, mc_size+1)
fig, ax = plt.subplots()
ax.loglog(np.abs(np.cumsum(mc)/ms - realP), label='MC')
ax.loglog(np.abs(np.cumsum(mcis)/ms - realP), label='MC-IS')
ax.legend()
ax.grid()
fig.show()
