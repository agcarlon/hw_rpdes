import numpy as np
# from hw_2 import eval_Q, fem_sparse
from poisson_model import eval_Q_MC, fem_sparse, karhunen_loeve, covariance_matrix
from scipy.stats import norm
from IPython.core.debugger import set_trace
from scipy.optimize import minimize
# import qmcpy as qp
# from tqdm import tqdm
import matplotlib.pyplot as plt


I = 10
h = 1/I
N = 9
n_N = 9


err_tol = .05
alpha = 0.05
kl_contr = .95
C_alpha = norm.ppf(1 - alpha/2)

# Qs, h = eval_Q(I=I, N=N, mc_size=mc_size, nu=nu, seed=0)
#
# print((Qs<K).mean())


def get_is_mean(K=-5, nu=2.5, mu=0):
    def objf_fun(w):
        h = 1/I
        xs = np.linspace(0, 1, I+1)
        W = np.zeros((1, N))
        W[0, :n_N] = w
        cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
        coefs, N_ = karhunen_loeve(cov, W, I, 1, kl_contr=kl_contr)
        u, h = fem_sparse(I, coefs)
        Qs = h*np.sum(u, axis=1)
        objf = (Qs < K)*norm.pdf(w).prod()
        return -objf[0]

    np.random.seed(0)

    def get_starting_point(mu=0):
        mc_size = 128
        k = 0
        while True:
            # sobol = qp.Sobol(N, seed=k)
            # normal = qp.Gaussian(sobol, covariance=1)
            # w = normal.gen_samples(mc_size) + mu
            h = 1/I
            xs = np.linspace(0, 1, I+1)
            cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
            w = np.random.randn(mc_size, n_N)
            W = np.zeros((mc_size, N))
            W[:, :n_N] = w
            coefs, N_ = karhunen_loeve(cov, W, I, mc_size, kl_contr=kl_contr)
            u, h = fem_sparse(I, coefs)
            Qs = h*np.sum(u, axis=1)
            if np.any(Qs < K):
                idx = np.argmin(Qs)
                x = W[idx, :n_N]
                break
            k += 1
        return x

    # if K == -5:
    #     mu = np.array([-0.685,  1.266, -1.345, -0.13, -1.697])
    #     return mu
    # elif K == -10:
    #     mu = np.array([-1.467, 2.712, -2.881, -0.278, -3.635])
    #     return mu
    # elif K == -20:
    #     mu = np.array([-2.248, 4.159, -4.417, -0.426, -5.574])
    #     return mu

    # Plot of domain
    # x = np.linspace(-2, -4, 100)
    # objf = []
    # for x_ in x:
    #     objf.append(objf_fun(x_))
    #
    # plt.plot(x, objf)
    # plt.show()

    # n_dim = 20
    # x = np.linspace(-4, -1, n_dim)
    # y = np.linspace(-2, 2, n_dim)
    # objf = np.zeros((n_dim, n_dim))
    # for i in range(n_dim):
    #     for j in range(n_dim):
    #         objf[j, i] = objf_fun([x[i], y[j]])
    #
    #
    # fig, ax = plt.subplots()
    # cntr = ax.contourf(x, y, objf)
    # ax.set_xlabel('x0')
    # ax.set_ylabel('x1')
    # plt.colorbar(cntr)
    # plt.show()

    # np.random.seed(0)
    # x0 = get_starting_point(mu=mu)
    # print(x0)
    # # x0 = np.array([-0.782,  1.446, -1.536, -0.148, -1.938])
    # res = minimize(objf_fun, x0, method='Nelder-Mead')
    # # res = minimize(objf_fun, x0)
    # plt.plot(*res.x, 'x')
    # np.random.seed(1)
    # x1 = get_starting_point(mu=mu)
    # print(x1)
    # # x0 = np.array([-0.782,  1.446, -1.536, -0.148, -1.938])
    # res_ = minimize(objf_fun, x1, method='Nelder-Mead')
    # # res_ = minimize(objf_fun, x1)
    # ax.plot(*res_.x, 'x')
    # print(f'Finding mu*: {res_.success}')
    # fig.savefig('integrand.pdf')
    # plt.show()
    # res = minimize(objf_fun, x0)
    x0 = get_starting_point(mu=mu)
    res = minimize(objf_fun, x0, method='Nelder-Mead')
    print(f'Finding mu*: {res.success}')
    mu = res.x
    return mu


def eval_Q_is(mu, K, I=10, N=10, mc_size=1024, seed=0, nu=2.5):
    np.random.seed(seed)
    to_sample = np.copy(mc_size)
    mu_ = np.zeros(N)
    mu_[:n_N] = mu
    Qs = []
    stds = []
    Ms = []
    lkls = []
    # sobol = qp.Sobol(N, seed=seed)
    # normal = qp.Gaussian(sobol, covariance=1)
    # W = normal.gen_samples(mc_size)
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    sigma = 1
    while True:
        W = np.random.randn(to_sample, N)
        W_ = W*sigma + mu_
        # u, h = fem(I, N, w, to_sample, nu)
        coefs, N_ = karhunen_loeve(cov, W_, I, to_sample, kl_contr=kl_contr)
        u, h = fem_sparse(I, coefs)
        lkl = norm.pdf(W_).prod(axis=1) / (1/sigma*norm.pdf((W_ - mu_)/sigma)).prod(axis=1)
        lkls = np.hstack([lkls, lkl])
        Qs = np.hstack([Qs, h*np.sum(u, axis=1)])
        out = (Qs < K)*lkls
        stds.append(np.std(out))
        Ms.append(mc_size)
        # print(f'M: {mc_size}')
        if C_alpha*np.std(out)/np.sqrt(mc_size)/np.mean(out) < err_tol:
            print(f'M: {mc_size}')
            print(f'P: {np.mean(out)}')
            print(f'std: {stds[-1]}')
            break
        to_sample = np.copy(mc_size)
        mc_size *= 2
    # print(out.mean())
    # print(np.mean(Qs_ < K))

    # testing
    # u, h = fem_sparse(I, N, W, mc_size, nu)
    # lkl = norm.pdf(W).prod(axis=1) / norm.pdf(W - mu).prod(axis=1)
    # Qs = h*np.sum(u, axis=1)
    # out = (Qs < K)
    # print(out.mean())
    return out, np.array(stds), Ms


def compute_is(mu, K=-5, nu=2.5, reps=50):
    # Ms = [128, 256, 512]
    # Ms = [2**8, 2**10, 2**12]
    # Ms = [2**4, 2**7, 2**10]
    # for rep in tqdm(range(reps)):
    Qs, stds, Ms = eval_Q_is(mu, K, I=I, N=N, mc_size=128, seed=0, nu=nu)
    Qs_, stds_, Ms_ = eval_Q_is(mu*0, K, I=I, N=N, mc_size=128, seed=0, nu=nu)
    M = len(Qs)
    Ms = np.array(Ms)
    Qs_av = np.cumsum(Qs) / np.arange(1, M+1)
    M_ = len(Qs_)
    Ms_ = np.array(Ms_)
    Qs_av_ = np.cumsum(Qs_) / np.arange(1, M_+1)
    # print(np.mean(test))
    stat_err = np.abs((Qs_av[:-10] - Qs_av[-1])/Qs_av[-1])
    stat_err_ = np.abs((Qs_av_[:-10] - Qs_av[-1])/Qs_av[-1])
    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(stat_err_, 'C0', label='Rel. error')
    ax.loglog(stat_err, 'C1', label='Rel. error with IS')
    ax.loglog(Ms_, C_alpha*stds_/Ms_**.5/np.abs(Qs_av[-1]), 'o--',c='C2',ms=3, label=fr'CLT')
    ax.loglog(Ms, C_alpha*stds/Ms**.5/np.abs(Qs_av[-1]), 'o--', c='C3', ms=3, label=fr'CLT for IS')
    ax.grid()
    ax.legend()
    ax.set_xlabel('$M$')
    ax.set_ylabel(r'$|P_M - P|/P$')
    ax.set_title(fr'$K=${K}, $\nu=${nu}, $C_{{\alpha}}$={C_alpha:.2f}')
    fig.tight_layout()
    fig.savefig(f'stat_err_reg_is_{nu}_K{-K}.pdf')
    plt.close('all')


n_N_ = 4

nu = 2.5
mu5 = get_is_mean(K=-5, nu=nu)
# mu5 = np.array([-2.75, 0])
print(mu5)
mu5[n_N_:] = 0

# eval_Q_is(mu5, K=-5, nu=nu)
compute_is(mu5, K=-5, nu=nu)

mu10 = get_is_mean(K=-10, mu=mu5, nu=nu)
print(mu10)
mu10[n_N_:] = 0

# eval_Q_is(mu10, K=-10)
compute_is(mu10, K=-10, nu=nu)

mu20 = get_is_mean(K=-20, mu=mu10, nu=nu)
print(mu20)
mu20[n_N_:] = 0
# eval_Q_is(mu20, K=-20)
compute_is(mu20, K=-20, nu=nu)


nu = 'inf'
mu5 = get_is_mean(K=-5, nu=nu)
print(mu5)
mu5[n_N_:] = 0
# eval_Q_is(mu5, K=-5)
compute_is(mu5, K=-5, nu=nu)

mu10 = get_is_mean(K=-10, mu=mu5, nu=nu)
print(mu10)
mu10[n_N_:] = 0
# eval_Q_is(mu10, K=-10)
compute_is(mu10, K=-10, nu=nu)

mu20 = get_is_mean(K=-20, mu=mu10, nu=nu)
print(mu20)
mu20[n_N_:] = 0
# eval_Q_is(mu20, K=-20)
compute_is(mu20, K=-20, nu=nu)

set_trace()
