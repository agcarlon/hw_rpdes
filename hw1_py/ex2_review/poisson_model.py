import numpy as np
from numpy.linalg import eig, norm, det, solve
from IPython.core.debugger import set_trace
import matplotlib.pyplot as plt
import sobol_seq as ss
import qmcpy as qp
from scipy.sparse import diags as sp_diags
from scipy.sparse.linalg import spsolve
from joblib import Parallel, delayed


def force(x):
    return 4*np.pi**2*np.cos(2*np.pi*x)


def covariance_matrix(xs, nu=1.5, sigma2=2, rho=.1):
    I = len(xs)
    C = np.zeros((I, I))
    aux = np.abs(xs[:, np.newaxis] - xs[np.newaxis,:])/rho
    if nu == .5:
        # for i in range(I):
        #     for j in range(I):
        #         C[i, j] = sigma2*np.exp(-np.abs(xs[i]-xs[j])/rho)
        C = sigma2*np.exp(-aux)
    elif nu == 1.5:
        # for i in range(I):
        #     for j in range(I):
        #         C[i, j] = sigma2*(1 + np.sqrt(3)*np.abs(xs[i]-xs[j])/rho) * \
        #             np.exp(-np.sqrt(3)*np.abs(xs[i]-xs[j])/rho)
        C = sigma2*(1 + np.sqrt(3)*aux) * np.exp(-np.sqrt(3)*aux)
    elif nu == 2.5:
        # for i in range(I):
            # for j in range(I):
                # aux = np.abs(xs[i]-xs[j])/rho
                # C[i, j] = sigma2*(1 + np.sqrt(5)*aux + np.sqrt(3)*aux**2) * \
                    # np.exp(-np.sqrt(5)*aux)
        C = sigma2*(1 + np.sqrt(5)*aux + np.sqrt(3)*aux**2) * \
            np.exp(-np.sqrt(5)*aux)
    if nu == 'inf':
        # for i in range(I):
            # for j in range(I):
                # C[i, j] = sigma2*np.exp(-np.abs(xs[i]-xs[j])**2/(2*rho**2))
        C = sigma2 * np.exp(-aux**2/4)
    return C


def karhunen_loeve(C, w, n_el, mc_size, kl_contr=.95):
    eig_vals, eig_vecs = eig(C)

    # N = np.minimum(N, np.sum(eig_vals > 0))

    weights = (eig_vals**.5).real * np.max(np.abs(eig_vecs), axis=0)
    weights[np.isnan(weights)] = 0

    idxs = np.flip(np.argsort(weights))
    weights_ = weights[idxs] / np.nansum(weights)
    N = np.where(np.cumsum(weights_) > kl_contr)[0][0] + 1
    eig_vals = eig_vals[idxs[:N]].real
    eig_vecs_ = eig_vecs[:, idxs[:N]].real
    kappa = np.zeros((mc_size, n_el))
    for i in range(mc_size):
        for n in range(N):
            kappa[i, :] += eig_vals[n]**.5 * w[i, n] * eig_vecs_[:, n]
    coefs = np.exp(kappa)
    # cov = np.cov(coefs.T)
    # cov_ = np.cov(kappa.T)
    # print(w)
    # print(kappa)
    # plt.plot(eig_vecs_[:, 0])
    # plt.show()
    return coefs, N


def fem(I, N, w, mc_size=1, nu=1.5):
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    A = np.zeros((I+1, I+1, mc_size))
    C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    coefs = karhunen_loeve(C, N, w, I, mc_size)
    for i in range(1, I):
        A[i, i-1] = -coefs[:, i-1]/h**2
        A[i, i] = (coefs[:, i-1] + coefs[:, i])/h**2
        A[i, i+1] = -coefs[:, i]/h**2
    # Dirichlet BC
    fv = np.array([force(x) for x in xs[1:-1]])
    # Dirichlet BC
    # fv[0], fv[-1] = 0, 0
    u = np.zeros((mc_size, I+1))
    for n in range(mc_size):
        u[n, 1:-1] = solve(A[1:-1, 1:-1, n], fv)
    # u = np.vstack([solve(A[:, :, n], fv) for n in range(mc_size)])
    # plt.plot(u[0])
    # plt.show()
    # plt.plot(np.mean(u, axis=0))
    return u, h


def solve_system(coefs, h, fv):
    off = -coefs[1:-1]/h**2
    mid = np.convolve(coefs, [1, 1])[1:-1]/h**2
    A_ = sp_diags([off, mid, off], [-1, 0, 1])
    return spsolve(A_.tocsr(), fv)


def fem_sparse(I, coefs, nu=2.5):
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    # C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    # coefs = karhunen_loeve(C, N, w, I, mc_size)
    fv = np.array([force(x) for x in xs[1:-1]])
    u = np.zeros((len(coefs), I+1))

    # Parallel
    with Parallel(n_jobs=78) as par:
        u_ = par(delayed(solve_system)(coefs_, h, fv) for coefs_ in coefs)
    u[:, 1:-1] = np.vstack(u_)

    # Non parallel
    # for n in range(mc_size):
    #     off = -coefs[n, 1:-1]/h**2
    #     mid = np.convolve(coefs[n], [1, 1])[1:-1]/h**2
    #     A_ = sp_diags([off, mid, off], [-1, 0, 1])
    #     u[n, 1:-1] = spsolve(A_.tocsr(), fv)
    return u, h


def eval_Q_QMC(I=100, N=20, mc_size=1000, seed=0, nu=1.5):
    Qs = np.zeros(mc_size)
    np.random.seed(seed)
    # for j in range(mc_size):
    #     w = sampler(N)
    #     u, h = fem(I, model_1, w)
    #     Qs[j] = h*np.sum(u)
    # w = np.random.randn(mc_size, N)
    # w = ss.i4_sobol_generate_std_normal(N, mc_size)
    sobol = qp.Sobol(N, seed=seed)
    normal = qp.Gaussian(sobol, covariance=1)
    w = normal.gen_samples(mc_size)
    # u, h = fem(I, N, w, mc_size, nu)
    u, h = fem_sparse(I, N, w, mc_size, nu)
    Qs = h*np.sum(u, axis=1)
    return Qs, h


def eval_Q_MC(I=100, N=20, mc_size=1000, seed=0, nu=1.5):
    Qs = np.zeros(mc_size)
    np.random.seed(seed)
    # for j in range(mc_size):
    #     w = sampler(N)
    #     u, h = fem(I, model_1, w)
    #     Qs[j] = h*np.sum(u)
    # w = np.random.randn(mc_size, N)
    # w = ss.i4_sobol_generate_std_normal(N, mc_size)
    w = np.random.randn(mc_size, N)
    # u, h = fem(I, N, w, mc_size, nu)
    u, h = fem_sparse(I, N, w, mc_size, nu)
    Qs = h*np.sum(u, axis=1)
    return Qs, h
