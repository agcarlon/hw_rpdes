import numpy as np
from numpy.linalg import eig, det, solve
from poisson_model import fem_sparse, covariance_matrix
from functools import partial
import matplotlib.pyplot as plt

from IPython.core.debugger import set_trace

    # C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    # coefs = karhunen_loeve(C, N, w, I, mc_size)



def eval_q(w, h, coefs, nu=2.5):
    mc_size = len(w)
    u, h_ = fem_sparse(int(1/h), coefs, mc_size, nu=nu)
    Qs = h*np.sum(u, axis=1)
    return Qs

def sampler(n, d):
    return np.random.randn(n, d)


def karhunen_loeve(C, w, n_el, mc_size, kl_contr=.9):
    eig_vals, eig_vecs = eig(C)

    # N = np.minimum(N, np.sum(eig_vals > 0))

    weights = (eig_vals**.5).real * np.max(np.abs(eig_vecs), axis=0)
    weights[np.isnan(weights)] = 0

    idxs = np.flip(np.argsort(weights))
    weights_ = weights[idxs] / np.nansum(weights)
    N = np.where(np.cumsum(weights_) > kl_contr)[0][0] + 1

    eig_vals = eig_vals[idxs[:N]].real
    eig_vecs_ = eig_vecs[:, idxs[:N]].real
    # ()
    kappa = np.zeros((mc_size, n_el))
    for i in range(mc_size):
        for n in range(N):
            kappa[i, :] += eig_vals[n]**.5 * w[i, n] * eig_vecs_[:, n]
    coefs = np.exp(kappa)
    # cov = np.cov(coefs.T)
    # cov_ = np.cov(kappa.T)
    # print(w)
    # print(kappa)
    # plt.plot(eig_vecs_[:, 0])
    # plt.show()
    return coefs


def get_bias(nu):
    I = 512
    h = 1/I
    mc_size = 100000
    reps = 5
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    np.random.seed(0)
    sample = sampler(mc_size, I)

    coefs = karhunen_loeve(cov, sample, I, mc_size)
    Qs = []
    hs = []
    for rep in [0, 2, 3, 4, 5]:
        stride = 2**rep
        u, h = fem_sparse(int(I/2**rep), coefs[:, ::stride])
        Qs.append(np.mean(h*np.sum(u, axis=1)))
        hs.append(h)

    print(f'Q:{Qs[0]}')
    bias = np.abs(Qs[1:] - Qs[0])
    z = np.polyfit(np.log(hs[1:]), np.log(bias), 1)
    rate = z[0]
    const = np.exp(z[1])
    print(f'Linear regr, rate:{rate}, const.:{const}')

    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    ax.loglog(hs[1:], bias, 'o--')
    ax.set_xlabel(r'$h$')
    ax.set_ylabel('bias')
    ax.grid()
    plt.savefig(f'bias_{nu}.pdf')

    tol = np.abs(Qs[0]/10)
    h_ = (tol/(2*const))**(1/rate)
    print(f'h found: {h_}')
    h__ = 1/np.ceil(1/h_)
    print(f'closest h: {h__}')
    return h__, tol


def adpt_monte_carlo(nu, h, tol):
    C_alpha = 2.58
    I = int(1/h)
    mc_size = 100
    to_sample = 100
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    np.random.seed(0)
    Qs = []
    clt_errs = []
    Qs_means = []
    mc_sizes = []
    while True:
        sample = sampler(to_sample, I)
        coefs = karhunen_loeve(cov, sample, I, to_sample)
        u, h_ = fem_sparse(I, coefs)
        Qs = np.hstack([Qs, h*np.sum(u, axis=1)])
        Qs_means.append(np.mean(Qs))
        std = np.std(Qs, ddof=1)
        clt_err = C_alpha*std/np.sqrt(mc_size)
        clt_errs.append(clt_err)
        mc_sizes.append(mc_size)
        if clt_err < tol/2:
            break
        to_sample = np.copy(mc_size)
        mc_size *= 2

    print(f'M: {mc_size}')
    set_trace()
    stat_err = np.abs(np.cumsum(Qs[:-5])/np.arange(1, 1+mc_size-5) - np.mean(Qs))
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    ax.loglog(stat_err/np.abs(np.mean(Qs)), '-', label='Error')
    ax.loglog(mc_sizes, clt_errs/ np.abs(np.mean(Qs)), 'o--', label=fr'CLT, $C_{{\alpha}}$={C_alpha}')
    ax.set_xlabel(r'sample size')
    ax.set_ylabel('relative statistical error')
    ax.grid()
    ax.legend()
    fig.tight_layout()
    plt.savefig(f'stat_err_{nu}.pdf')
    set_trace()

# h, tol = get_bias(nu=2.5)
# adpt_monte_carlo(nu=2.5, h=h, tol=tol)

# h, tol = get_bias(nu='inf')
# adpt_monte_carlo(nu='inf', h=h, tol=tol)

def two_levels(nu):
    if nu==2.5:
        tol = .175
        MC_M = 3200
    else:
        tol = .157
        MC_M = 6400
    C_alpha = 2.58
    h0 = 0.2
    h1 = 0.1
    I = int(1/h1)
    mc_size = 1000
    reps = 5
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h1/2, nu=nu)

    np.random.seed(0)
    sample = sampler(mc_size, I)

    coefs = karhunen_loeve(cov, sample, I, mc_size)
    u0, h0 = fem_sparse(int(I/2), coefs[:, ::2])
    Q0 = h0*np.sum(u0, axis=1)

    V0 = np.var(Q0, ddof=1)
    np.random.seed(1)
    sample = sampler(mc_size, I)

    coefs = karhunen_loeve(cov, sample, I, mc_size)

    u1_, h1_ = fem_sparse(int(I/2), coefs[:, ::2])
    Q1_ = h1_*np.sum(u1_, axis=1)

    u1, h1 = fem_sparse(int(I), coefs)
    Q1 = h1*np.sum(u1, axis=1)


    V1 = np.var(Q1 - Q1_, ddof=1)

    C0 = h0**-3
    C1 = h1_**-3 + h1**-3

    M0 = np.ceil(4*C_alpha**2/tol**2 * np.sqrt(V0/C0) * (np.sqrt(V0*C0) + np.sqrt(V1*C1))).astype('int')
    M1 = np.ceil(4*C_alpha**2/tol**2 * np.sqrt(V1/C1) * (np.sqrt(V0*C0) + np.sqrt(V1*C1))).astype('int')
    print(f'V0:{V0}, V1:{V1}')
    print(f'M0:{M0}, M1:{M1}')
    work = M0*C0 + M1*C1
    print(f'Total MLMC work:{work}')
    print(f'Total MC work:{MC_M*0.111**-3}')
    np.random.seed(0)
    sample = sampler(M0, I)

    coefs = karhunen_loeve(cov, sample, I, M0)
    u0, h0 = fem_sparse(int(I/2), coefs[:, ::2])
    Q0 = h0*np.sum(u0, axis=1)

    V0 = np.var(Q0, ddof=1)
    np.random.seed(1)
    sample = sampler(M1, I)

    coefs = karhunen_loeve(cov, sample, I, M1)

    u1_, h1_ = fem_sparse(int(I/2), coefs[:, ::2])
    Q1_ = h1_*np.sum(u1_, axis=1)

    u1, h1 = fem_sparse(int(I), coefs)
    Q1 = h1*np.sum(u1, axis=1)

    V1 = np.var(Q1 - Q1_, ddof=1)
    V_MC = np.var(Q1, ddof=1)

    Q = np.mean(Q0) + np.mean(Q1 - Q1_)

    rel_err = C_alpha*np.sqrt(V0/M0+V1/M1) / Q
    print(rel_err)

    works_ml = []
    works_mc = []
    tols = tol*2**np.arange(5)
    for tol_ in tols:
        M0 = np.ceil(4*C_alpha**2/tol_**2 * np.sqrt(V0/C0) * (np.sqrt(V0*C0) + np.sqrt(V1*C1))).astype('int')
        M1 = np.ceil(4*C_alpha**2/tol_**2 * np.sqrt(V1/C1) * (np.sqrt(V0*C0) + np.sqrt(V1*C1))).astype('int')
        works_ml.append(M0*C0 + M1*C1)
        M_MC_ = np.ceil(4*C_alpha**2*V_MC/tol_**2)
        works_mc.append(M_MC_*0.1**-3)

    fig, ax = plt.subplots(figsize=(5, 5))
    ax.loglog(tols, works_mc, 'o--', label='Crude MC')
    ax.loglog(tols, works_ml, 'o--', label='MLMC, L=2')
    ax.set_title(rf'$\nu=${nu}, $C_{{\alpha}}$={C_alpha}')
    ax.set_xlabel('$tol$')
    ax.set_ylabel('work')
    ax.legend()
    ax.grid()
    fig.tight_layout()
    fig.savefig(f'work_mlmc_{nu}.pdf')


two_levels(nu=2.5)
two_levels(nu='inf')
