import numpy as np
from numpy.linalg import eig, norm, det, solve
from IPython.core.debugger import set_trace
import matplotlib.pyplot as plt


def force(x):
    return 4*np.pi**2*np.cos(2*np.pi*x)


# Model 1 for conductivity a
def model_1(x, w):
    N = np.shape(w)[0]
    # sigma ensures coercivity ||a|| > coerc
    coerc = .5  # between 0 and 1
    sigma = (1 - coerc)/np.sqrt(3)
    sigma = 1/2
    x_ = np.linspace(0, 1, N+1)
    part = np.argmax(x < x_) - 1
    cond = 1 + sigma*w[part]
    return cond


def sampler(size):
    return np.random.uniform(-np.sqrt(3), np.sqrt(3), size=size)


def fem(I, model, w, mc_size=1):
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    A = np.zeros((I+1, I+1, mc_size))
    for i in range(1, I):
        A[i, i-1] = -model((xs[i] + xs[i-1])/2, w)/h**2
        A[i, i] = (model((xs[i] + xs[i-1])/2, w) + model((xs[i+1] + xs[i])/2, w))/h**2
        A[i, i+1] = -model((xs[i] + xs[i+1])/2, w)/h**2
    # Dirichlet BC
    A[0][0] = 1
    A[-1][-1] = 1
    fv = np.array([force(x) for x in xs])
    # Dirichlet BC
    fv[0], fv[-1] = 0, 0
    u = np.vstack([solve(A[:, :, n], fv) for n in range(mc_size)])
    # u = solve(A, fv)
    return u, h


def eval_Q(sampler, model, I=100, N=20, mc_size=1000, seed=0):
    Qs = np.zeros(mc_size)
    np.random.seed(seed)
    # for j in range(mc_size):
    #     w = sampler(N)
    #     u, h = fem(I, model_1, w)
    #     Qs[j] = h*np.sum(u)
    w = sampler((N, mc_size))
    u, h = fem(I, model, w, mc_size)
    Qs = h*np.sum(u, axis=1)
    return Qs, h


mc_size = 10

# Qs, h = eval_Q(sampler=sampler, model=model_1, I=100, N=20, mc_size=mc_size)
# np.save('ex_2_1_qs', Qs)
# Qs = np.load('ex_2_1_qs.npy', allow_pickle=True)
# Qs_av = np.cumsum(Qs) / np.arange(1, mc_size+1)
# err = np.abs(Qs_av[:-10] - Qs_av[-1])
# plt.loglog(err)
# plt.xlabel('Sample size')
# plt.ylabel('Error')
# plt.grid()
# plt.savefig('ex_2_1_mc.pdf')
# plt.show()

# Qs_ref, h = eval_Q(sampler, model_1, I=40, N=40, mc_size=100000, seed=1)
# Q_true = np.mean(Qs_ref)
# print(Q_true)
#
# Qs, h = eval_Q(sampler, model_1, I=10, N=10, mc_size=15)
# print((Qs.mean() - Q_true)/Q_true)


# Richardon extrapolation to get bias constant
def get_bias_constant():
    N = 100
    np.random.seed(0)
    steps = 10
    hs = []
    Qs = []
    for st in range(steps):
        # u, h = fem(N*2**st, model_1, w, mc_size)
        # Q = h*np.sum(u)
        Q, h = eval_Q(sampler, model_1, I=N*2**st, mc_size=mc_size, N=N)
        hs.append(h)
        print(Q.std())
        Qs.append(Q.mean())

    z = np.polyfit(np.log(hs[:-1]), np.log(np.abs(Qs[:-1] - Qs[-1])), 1)
    print(f'Linear regr, rate:{z[0]}, const.:{np.exp(z[1])}')
    # Linear regr, rate:2.1304162686929766, const.:8.332388092804992
    # Linear regr, rate:2.081745397947558, const.:8.12905007189675
    set_trace()
    # set_trace()
    # rate:2.03523143318136, const.:3.679777710270832
    # M_1 = 1
    # M_2 = mc_size



    # std_f_x = np.std(Q)
    # # clt_constant = norm.ppf(1 - (1-alpha)/2)*std_f_x
    # clt_constant = 1.65*std_f_x
    #
    # err_1 = clt_constant/np.sqrt(M_1)
    # err_2 = err_1*((M_2/M_1)**(-1/2))
    # # ax.loglog(Ms, regr, 'r--', label='Regression')
    # fig, ax = plt.subplots(figsize=(6, 4))
    # ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$C_\alpha \sigma M^{-1/2}$')
    # ax.loglog(hs[:-1], np.abs(Qs[:-1] - Qs[-1]))
    # ax.grid()
    # ax.set_xlabel('Sample size')
    # ax.set_ylabel(r'$|\hat{f} - E[f]|$')
    # ax.legend()
    # ax.set_title(f'Model 1, N={N}')
    # fig.tight_layout()
    # fig.savefig(f'1_1.pdf')
    # plt.show()
    # rate = 2
    # # Richardson
    # Ks = []
    # for st in range(steps-1):
    #     Ks.append((Qs[st+1] - Qs[st])/(hs[st]**rate*(1 - 1/(2**rate))))
    # # Ks[-1] = 3.5783988644884817
    # # exp(z[1]) = 7.833113609433932
    # K = Ks[-1]
    # tol = np.abs(Qs[-1])*.1
    # h_ = np.sqrt(tol/2/K)
    set_trace()



get_bias_constant()

def plot_conv():
    # mc_size = int(1e4)
    mc_size = 1114
    N = 40
    Q, h = eval_Q(sampler, model_1, I=N, mc_size=mc_size, N=N)
    Q_av = np.cumsum(Q) / np.arange(1, mc_size+1)
    # rate:2.03523143318136, const.:3.679777710270832
    M_1 = 1
    M_2 = mc_size



    std_f_x = np.std(Q)
    # clt_constant = norm.ppf(1 - (1-alpha)/2)*std_f_x
    clt_constant = 1.65*std_f_x

    err_1 = clt_constant/np.sqrt(M_1)
    err_2 = err_1*((M_2/M_1)**(-1/2))
    # ax.loglog(Ms, regr, 'r--', label='Regression')
    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog([M_1, M_2],[err_1, err_2], 'k--', label=r'$c_0 \sigma M^{-1/2}$')
    ax.loglog(np.abs(Q_av[:-1] - Q_av[-1]))
    ax.grid()
    ax.set_xlabel('Sample size')
    ax.set_ylabel(r'$|Q_M(u_h) - E[Q(u_h)]|$')
    ax.legend()
    ax.set_title(f'Model 1, N={N}')
    fig.tight_layout()
    fig.savefig(f'1_1_final.pdf')
    plt.show()

    Q, h = eval_Q(sampler, model_1, I=N*2, mc_size=mc_size*10, N=N*2)
    print((Q_av[-1] - np.mean(Q))/np.mean(Q))
# plot_conv()


def get_error():
    seeds = np.arange(1000)
    mc_size = 12
    N = 10
    Q_avs = []
    for seed in seeds:
        Q, h = eval_Q(sampler, model_1, I=N, mc_size=mc_size, N=N, seed=seed)
        # Q_av = np.cumsum(Q) / np.arange(1, mc_size+1)
        Q_avs.append(np.mean(Q))
        # rate:2.03523143318136, const.:3.679777710270832
        M_1 = 1
        M_2 = mc_size
    # Q, h = eval_Q(sampler, model_1, I=N*2, mc_size=mc_size*10, N=N*2)
    Q_ref = -1.0969736694141734
    Qs = np.array(Q_avs)
    set_trace()
    rel_errs = np.abs((Qs - Q_ref)/Q_ref)
    print(rel_errs)
    success_rate = np.mean(rel_errs < .1)*100
    fig, axs = plt.subplots(1, 1, figsize=(6, 4))
    axs.hist(rel_errs, 20)
    axs.axvline(.1, color='k', label=r'$10\%$')
    axs.set_xlabel('Relative error')
    axs.set_ylabel('Frequency')
    axs.set_title(f'1000 Repetitions, N={N}. Success rate: {success_rate:.2f}%')
    axs.legend()
    axs.grid()
    fig.tight_layout()
    fig.savefig(f'rel_err_{N}.pdf')
    plt.show()

# get_error()

def control_var():
    N = 10
    mc_size = 1000
    Q_0, h_0 = eval_Q(sampler, model_1, I=int(N/2), mc_size=mc_size, N=N, seed=0)
    Q_1, h_1 = eval_Q(sampler, model_1, I=N, mc_size=mc_size, N=N, seed=0)
    V0 = np.var(Q_0)
    V1 = np.var(Q_1 - Q_0)
    C0 = h_0**-3
    C1 = h_1**-3 + h_0**-3

    # tol = .109
    alpha = 2.20
    # tol_s = tol * (1 - (1+2*alpha/3)**-1)
    var_est = np.var(Q_1, ddof=1)/12

    mc_works = []
    ml_works = []
    tols = np.sqrt(var_est)/10**np.arange(5)

    for i in range(5):
        tol_var = np.sqrt(var_est)/10**i
        aux = np.sqrt(V0*C0) + np.sqrt(V1*C1)
        M0 = np.ceil(tol_var**-2*np.sqrt(V0/C0)*aux)
        M1 = np.ceil(tol_var**-2*np.sqrt(V1/C1)*aux)

        ml_work = M0*C0 + M1*C1
        ml_works.append(ml_work)

        # plain MC
        M = np.ceil((np.std(Q_1)/tol_var)**2)
        mc_work = M*h_1**-3

        mc_works.append(mc_work)

        # Sanity check
        var_ml = V0/M0 + V1/M1
        var_mc = np.var(Q_1)/M
        set_trace()


    fig, axs = plt.subplots(1, 1, figsize=(6, 4))
    axs.loglog(tols, mc_works, '--o', label='Monte Carlo')
    axs.loglog(tols, ml_works, '--o', label='Two level Monte Carlo')
    axs.set_xlabel('Tolerance')
    axs.set_ylabel('Work')
    # axs.set_title(f'1000 Repetitions, N={N}. Success rate: {success_rate:.2f}%')
    axs.legend()
    axs.grid()
    fig.tight_layout()
    fig.savefig(f'mlmc_work_conv.pdf')
    plt.show()

    set_trace()
    # sampling from each estimator
    Q_MCs = []
    Q_MLs = []
    for seed in range(1000):
        Q_0_, h_0 = eval_Q(sampler, model_1, I=int(N/2), mc_size=int(M0), N=N, seed=seed)
        Q_1_p, h_1 = eval_Q(sampler, model_1, I=int(N/2), mc_size=int(M1), N=N, seed=seed)
        Q_1_n, h_1 = eval_Q(sampler, model_1, I=N, mc_size=int(M1), N=N, seed=seed)

        Q_, h_1 = eval_Q(sampler, model_1, I=N, mc_size=int(M), N=N, seed=seed)

        Q_MCs.append(np.mean(Q_))
        Q_MLs.append(np.mean(Q_0_) + np.mean(Q_1_n - Q_1_p))

    Q_ref = -1.0969736694141734

    set_trace()
control_var()
