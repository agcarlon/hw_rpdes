import numpy as np
from numpy.linalg import eig, det, solve
from poisson_model import fem_sparse, covariance_matrix
from functools import partial
import matplotlib.pyplot as plt
import qmcpy as qp

from IPython.core.debugger import set_trace

    # C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    # coefs = karhunen_loeve(C, N, w, I, mc_size)



def eval_q(w, h, coefs, nu=2.5):
    mc_size = len(w)
    u, h_ = fem_sparse(int(1/h), coefs, mc_size, nu=nu)
    Qs = h*np.sum(u, axis=1)
    return Qs

def sampler(n, d, seed=0):
    n_ = 2**(np.ceil(np.log2(n)))
    sobol = qp.Sobol(d, seed=seed)
    normal = qp.Gaussian(sobol, covariance=1)
    w = normal.gen_samples(n_)[:int(n)]
    return w


def karhunen_loeve(C, w, n_el, mc_size, kl_contr=.9):
    eig_vals, eig_vecs = eig(C)

    # N = np.minimum(N, np.sum(eig_vals > 0))

    weights = (eig_vals**.5).real * np.max(np.abs(eig_vecs), axis=0)
    weights[np.isnan(weights)] = 0

    idxs = np.flip(np.argsort(weights))
    weights_ = weights[idxs] / np.nansum(weights)
    N = np.where(np.cumsum(weights_) > kl_contr)[0][0] + 1

    eig_vals = eig_vals[idxs[:N]].real
    eig_vecs_ = eig_vecs[:, idxs[:N]].real
    # ()
    kappa = np.zeros((mc_size, n_el))
    for i in range(mc_size):
        for n in range(N):
            kappa[i, :] += eig_vals[n]**.5 * w[i, n] * eig_vecs_[:, n]
    coefs = np.exp(kappa)
    # cov = np.cov(coefs.T)
    # cov_ = np.cov(kappa.T)
    # print(w)
    # print(kappa)
    # plt.plot(eig_vecs_[:, 0])
    # plt.show()
    return coefs


def get_bias(nu):
    I = 512
    h = 1/I
    mc_size = 2**16
    reps = 5
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    np.random.seed(0)
    sample = sampler(mc_size, I)

    coefs = karhunen_loeve(cov, sample, I, mc_size)
    Qs = []
    hs = []
    for rep in [0, 2, 3, 4, 5]:
        stride = 2**rep
        u, h = fem_sparse(int(I/2**rep), coefs[:, ::stride])
        Qs.append(np.mean(h*np.sum(u, axis=1)))
        hs.append(h)

    print(f'Q:{Qs[0]}')
    bias = np.abs(Qs[1:] - Qs[0])
    z = np.polyfit(np.log(hs[1:]), np.log(bias), 1)
    rate = z[0]
    const = np.exp(z[1])
    print(f'Linear regr, rate:{rate}, const.:{const}')

    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    ax.loglog(hs[1:], bias, 'o--')
    ax.set_xlabel(r'$h$')
    ax.set_ylabel('bias')
    ax.grid()
    plt.savefig(f'bias_{nu}.pdf')

    tol = np.abs(Qs[0]/10)
    h_ = (tol/(2*const))**(1/rate)
    print(f'h found: {h_}')
    h__ = 1/np.ceil(1/h_)
    print(f'closest h: {h__}')
    return h__, tol


def adpt_monte_carlo(nu, h, tol):
    C_alpha = 2.58
    I = int(1/h)
    mc_size = 8
    reps = 10
    to_sample = mc_size
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    np.random.seed(0)
    Qs = np.array([]).reshape(reps, -1)
    clt_errs = []
    Qs_means = []
    mc_sizes = []
    while True:
        Qs_ = []
        for rep in range(reps):
            total_sample = sampler(mc_size, I, seed=rep)
            sample = total_sample[-to_sample:]
            coefs = karhunen_loeve(cov, sample, I, to_sample)
            u, h_ = fem_sparse(I, coefs)
            Qs_.append(h*np.sum(u, axis=1))
        Qs = np.hstack([Qs, np.array(Qs_)])
        Qs_means.append(np.mean(Qs, axis=1))
        std = np.std(Qs_means[-1], ddof=1)
        clt_err = C_alpha*std/np.sqrt(reps)
        clt_errs.append(clt_err)
        mc_sizes.append(mc_size)
        if clt_err < tol/2:
            break
        to_sample = np.copy(mc_size)
        mc_size *= 2

    print(f'M: {mc_size}')
    mc_sizes = np.array(mc_sizes)
    stat_err = np.abs(np.cumsum(Qs[:, :-5].mean(axis=0))/np.arange(1, 1+mc_size-5) - np.mean(Qs))
    sample_sizes = np.arange(len(stat_err))*reps
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    ax.loglog(sample_sizes, stat_err/np.abs(np.mean(Qs)), '-', label='Error')
    ax.loglog(reps*mc_sizes, clt_errs/ np.abs(np.mean(Qs)), 'o--', label=fr'CLT, $C_{{\alpha}}$={C_alpha}')
    ax.set_xlabel(r'sample size')
    ax.set_ylabel('relative statistical error')
    ax.grid()
    ax.legend()
    fig.tight_layout()
    plt.savefig(f'stat_err_{nu}.pdf')

# h, tol = get_bias(nu=2.5)
# adpt_monte_carlo(nu=2.5, h=h, tol=tol)
# adpt_monte_carlo(nu=2.5, h=1/9, tol=.1745)

# h, tol = get_bias(nu='inf')
# adpt_monte_carlo(nu='inf', h=1/9, tol=.1575)

def two_levels(nu):
    if nu==2.5:
        tol = .175
        MC_M = 32
    else:
        tol = .157
        MC_M = 64
    C_alpha = 2.58
    # h0 = 0.1
    # h1 = 0.05
    h0 = 0.2
    h1 = 0.1
    I = int(1/h1)
    mc_size = 128
    reps = 20
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h1/2, nu=nu)

    np.random.seed(0)
    Q0s = []
    for rep in range(reps):
        sample = sampler(mc_size, I, seed=rep)
        coefs = karhunen_loeve(cov, sample, I, mc_size)
        u0, h0 = fem_sparse(int(I/2), coefs[:, ::2])
        Q0 = h0*np.sum(u0, axis=1)
        Q0s.append(Q0)
    Q0s = np.array(Q0s)
    Ms = np.arange(1, mc_size+1)
    Q0s_av = np.cumsum(Q0s, axis=1) / Ms
    var = np.var(Q0s_av, axis=0, ddof=1)
    z = np.polyfit(np.log(Ms), np.log(var), 1)
    rate0 = z[0]
    V0 = np.exp(z[1])
    np.random.seed(1)
    Q1s = []
    Q_QMC = []
    for rep in range(reps):
        sample = sampler(mc_size, I, seed=rep)
        coefs = karhunen_loeve(cov, sample, I, mc_size)
        u1_, h1_ = fem_sparse(int(I/2), coefs[:, ::2])
        Q1_ = h1_*np.sum(u1_, axis=1)
        u1, h1 = fem_sparse(int(I), coefs[:, ::1])
        Q1 = h1*np.sum(u1, axis=1)
        Q1s.append(Q1 - Q1_)
        Q_QMC.append(Q1)
    Q1s = np.array(Q1s)
    Ms = np.arange(1, mc_size+1)
    Q1s_av = np.cumsum(Q1s, axis=1) / Ms
    var = np.var(Q1s_av, axis=0, ddof=1)
    z = np.polyfit(np.log(Ms), np.log(var), 1)
    rate1 = z[0]
    V1 = np.exp(z[1])

    C0 = h0**-3
    C1 = h1_**-3 + h1**-3

    print(f'rate lvl 0: {rate0}')
    print(f'rate lvl 1: {rate1}')

    worst_rate = np.maximum(rate0, rate1)
    gamma = -(worst_rate + 1)/2

    tolV = tol/C_alpha/2

    aux1 = tolV**(-2/(1+2*gamma))

    aux2 = V0/reps*(C0*reps**2/V0)**(1 - 1/(2*(1+gamma)))
    aux2 += V1/reps*(C1*reps**2/V1)**(1 - 1/(2*(1+gamma)))
    aux2 = aux2**(1/(1+2*gamma))

    M0 = aux1*aux2*(C0*reps**2/V0)**(-1/(2*(1+gamma)))
    M1 = aux1*aux2*(C1*reps**2/V1)**(-1/(2*(1+gamma)))

    tol_test = V0*M0**(-1-2*gamma)/reps + V1*M1**(-1-2*gamma)/reps

    M0 = np.ceil(M0).astype('int')
    M1 = np.ceil(M1).astype('int')


    # M0_ = np.ceil(4*C_alpha**2/tol**2 * np.sqrt(V0/C0) * (np.sqrt(V0*C0) + np.sqrt(V1*C1))).astype('int')
    # M1_ = np.ceil(4*C_alpha**2/tol**2 * np.sqrt(V1/C1) * (np.sqrt(V0*C0) + np.sqrt(V1*C1))).astype('int')
    print(f'V0:{V0}, V1:{V1}')
    print(f'M0:{M0}, M1:{M1}')
    work = M0*C0*reps + M1*C1*reps
    print(f'Total MLMC work:{work}')

    Q_QMC = np.array(Q_QMC)
    Q_QMC_av = np.cumsum(Q_QMC, axis=1) / Ms
    var_QMC = np.var(Q_QMC_av, axis=0, ddof=1)
    z = np.polyfit(np.log(Ms), np.log(var_QMC), 1)
    rate_QMC = z[0]
    # rate_QMC = worst_rate
    V_QMC = np.exp(z[1])
    print(f'V QMC:{V_QMC}, rate:{rate_QMC}')
    C_QMC = h1**-3
    M_QMC = (V_QMC/reps/tolV**2)**(-1/rate_QMC)
    print(f'M QMC:{M_QMC}')
    work_qmc = C_QMC*reps*M_QMC
    print(f'Work QMC: {work_qmc}')

    np.random.seed(0)
    Q0s = []
    for rep in range(reps):
        sample = sampler(M0, I, seed=rep)
        coefs = karhunen_loeve(cov, sample, I, M0)
        u0, h0 = fem_sparse(int(I/2), coefs[:, ::2])
        Q0 = h0*np.sum(u0, axis=1)
        Q0s.append(Q0)
    Q0s = np.array(Q0s)
    Ms = np.arange(1, M0+1)
    Q0s_av = np.cumsum(Q0s, axis=1) / Ms
    var0 = np.var(Q0s_av, axis=0, ddof=1)
    z = np.polyfit(np.log(Ms), np.log(var0), 1)
    # rate0 = z[0]
    # V0 = np.exp(z[1])
    np.random.seed(1)
    Q1s = []
    Q_QMC = []
    for rep in range(reps):
        sample = sampler(M1, I, seed=rep)
        coefs = karhunen_loeve(cov, sample, I, M1)
        u1_, h1_ = fem_sparse(int(I/2), coefs[:, ::2])
        Q1_ = h1_*np.sum(u1_, axis=1)
        u1, h1 = fem_sparse(int(I), coefs[:, ::1])
        Q1 = h1*np.sum(u1, axis=1)
        Q_QMC.append(Q1)
        Q1s.append(Q1 - Q1_)
    Q1s = np.array(Q1s)
    Ms = np.arange(1, M1+1)
    Q1s_av = np.cumsum(Q1s, axis=1) / Ms
    var1 = np.var(Q1s_av, axis=0, ddof=1)
    z = np.polyfit(np.log(Ms), np.log(var1), 1)
    # rate1 = z[0]
    # V1 = np.exp(z[1])

    Q = np.mean(Q0) + np.mean(Q1 - Q1_)
    rel_err = C_alpha*np.sqrt(var0[-1]/reps + var1[-1]/reps) / Q
    print(rel_err)

    Q_QMC = np.array(Q_QMC)
    Q_QMC_av = np.cumsum(Q_QMC, axis=1) / Ms
    var_QMC = np.var(Q_QMC_av, axis=0, ddof=1)
    z = np.polyfit(np.log(Ms), np.log(var_QMC), 1)
    # rate_QMC = z[0]
    # V_QMC = np.exp(z[1])

    works_ml = []
    works_mc = []

    tols = tolV*2**np.arange(4)
    for tol_ in tols:
        aux1 = tol_**(-2/(1+2*gamma))

        aux2 = V0/reps*(C0*reps**2/V0)**(1 - 1/(2*(1+gamma)))
        aux2 += V1/reps*(C1*reps**2/V1)**(1 - 1/(2*(1+gamma)))
        aux2 = aux2**(1/(1+2*gamma))

        M0 = aux1*aux2*(C0*reps**2/V0)**(-1/(2*(1+gamma)))
        M1 = aux1*aux2*(C1*reps**2/V1)**(-1/(2*(1+gamma)))

        tol_test = V0*M0**(-1-2*gamma)/reps + V1*M1**(-1-2*gamma)/reps

        M0 = np.ceil(M0)
        M1 = np.ceil(M1)

        works_ml.append(reps*M0*C0 + reps*M1*C1)

        M_MC = (V_QMC/reps/tol_**2)**(-1/rate_QMC)
        test_MC = V_QMC/reps*M_MC**rate_QMC
        M_MC = np.ceil(M_MC)
        works_mc.append(reps*M_MC*h1**-3)

    fig, ax = plt.subplots(figsize=(5, 5))
    ax.loglog(tols, works_mc, 'o--', label='Crude RQMC')
    ax.loglog(tols, works_ml, 'o--', label='MLRQMC, L=2')
    ax.set_title(rf'$\nu=${nu}, $C_{{\alpha}}$={C_alpha}')
    ax.set_xlabel(r'$tol_{\sigma}$')
    ax.set_ylabel('work')
    ax.legend()
    ax.grid()
    fig.tight_layout()
    fig.savefig(f'work_mlmc_{nu}.pdf')
    set_trace()


two_levels(nu=2.5)
two_levels(nu='inf')
