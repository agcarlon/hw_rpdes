import numpy as np
# from hw_2 import eval_Q, fem_sparse
from poisson_model import eval_Q_MC, fem_sparse, karhunen_loeve, covariance_matrix, force
from scipy.stats import norm
from IPython.core.debugger import set_trace
from scipy.optimize import minimize
# import qmcpy as qp
from tqdm import tqdm
import matplotlib.pyplot as plt
import qmcpy as qp
from numpy.linalg import eig, det, solve


I = 10
h = 1/I
N = 9
n_N = 9


err_tol = .05
alpha = 0.05
kl_contr = .99
C_alpha = norm.ppf(1 - alpha/2)

# Qs, h = eval_Q(I=I, N=N, mc_size=mc_size, nu=nu, seed=0)
#
# print((Qs<K).mean())


def eval_Q_is(K, mu=0, I=10, mc_size=1024, seed=0, nu=2.5, tol=0.1):
    np.random.seed(seed)
    to_sample = np.copy(mc_size)
    mu_ = np.zeros(N)
    mu_[:n_N] = mu
    Qs = []
    stds = []
    Ms = []
    lkls = []
    # sobol = qp.Sobol(N, seed=seed)
    # normal = qp.Gaussian(sobol, covariance=1)
    # W = normal.gen_samples(mc_size)
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    sigma = 1
    while True:
        W = np.random.randn(to_sample, N)
        W_ = W*sigma + mu_
        # u, h = fem(I, N, w, to_sample, nu)
        coefs, N_ = karhunen_loeve(cov, W_, I, to_sample, kl_contr=kl_contr)
        u, h = fem_sparse(I, coefs)
        lkl = norm.pdf(W_).prod(axis=1) / (1/sigma*norm.pdf((W_ - mu_)/sigma)).prod(axis=1)
        lkls = np.hstack([lkls, lkl])
        Qs = np.hstack([Qs, h*np.sum(u, axis=1)])
        out = (Qs < K)*lkls
        stds.append(np.std(out))
        Ms.append(mc_size)
        # print(f'M: {mc_size}')
        if C_alpha*np.std(out)/np.sqrt(mc_size)/np.mean(out) < tol/2:
            print(f'M: {mc_size}')
            print(f'P: {np.mean(out)}')
            print(f'std: {stds[-1]}')
            break
        to_sample = np.copy(mc_size)
        mc_size *= 2
    # print(out.mean())
    # print(np.mean(Qs_ < K))

    # testing
    # u, h = fem_sparse(I, N, W, mc_size, nu)
    # lkl = norm.pdf(W).prod(axis=1) / norm.pdf(W - mu).prod(axis=1)
    # Qs = h*np.sum(u, axis=1)
    # out = (Qs < K)
    # print(out.mean())
    return Qs, out, np.array(stds), Ms


def ldt(X, K):
    ## P(X<K) < min_t E[exp(-t X)] / exp(t K)
    def objf(t):
        return np.mean(np.exp(-t*X)) * np.exp(t*K)

    res = minimize(objf, 1)
    t = res.x
    p_ = res.fun
    return p_, t


# K=-5
nu='inf'
# h=0.1
mc_size=128
tol=0.1  # relative
# Qs, Ps, stds, Ms = eval_Q_is(K, nu=nu, I=10, mc_size=mc_size, tol=tol)
# X = Qs - np.mean(Qs)
# K_ = K - np.mean(Qs)
# P5, t5 = ldt(Qs, -5)
# P10, t10 = ldt(Qs, -10)
# P20, t20 = ldt(Qs, -20)

xs = np.linspace(0, 1, 10)
h = xs[1] - xs[0]
# f_L2 = np.sum(h*force(xs)**2)**.5
f_L2 = (16*np.pi**4/2)**.5
q0 = f_L2/2
cov = covariance_matrix(xs, nu=nu)
eig_vals, eig_vecs = eig(cov)

weights = (eig_vals**.5).real * np.max(np.abs(eig_vecs), axis=0)
weights[np.isnan(weights)] = 0

idxs = np.flip(np.argsort(weights))
weights_ = weights[idxs] / np.nansum(weights)
N = np.where(np.cumsum(weights_) > kl_contr)[0][0] + 1
eig_vals = eig_vals[idxs[:N]].real
eig_vecs_ = eig_vecs[:, idxs[:N]].real
b_norms = weights[idxs[:N]].real

samp = np.random.randn(1, N)

Q_bound = q0 * np.exp(np.sum([b*np.abs(samp) for b in b_norms]))

Xs = np.random.randn(1000000)
Ys = np.abs(Xs)
Xis = Ys - Ys.mean()

def cum_gen_func(t):
    return np.log(2*np.exp(t**2/2-t*np.sqrt(2/np.pi))*norm.cdf(t))
    # return np.log(2*np.exp(t**2/2)*norm.cdf(t))
    # return np.log(np.mean(np.exp(t*Xis)))


# ts = np.linspace(-20, 20, 100)


# empirical = [np.log(np.mean(np.exp(t*Xis))) for t in ts]
# analytical = [cum_gen_func(t) for t in ts]


# plt.plot(ts, empirical, label='empirical')
# plt.plot(ts, analytical, label='analytical')
# plt.xlabel('thetas')
# plt.legend()
# plt.show()


def rate_function(x, K):
    def objf(t):
        cum_gen_sum = np.sum([cum_gen_func(t*b) for b in b_norms])
        return -(t*x - cum_gen_sum)

    # ts = np.linspace(-100, 10, 100)
    # funs = [objf(t) for t in ts]
    # plt.plot(ts, funs)
    # set_trace()
    # plt.xlabel(r'$\theta$')
    # plt.ylabel(r'$-\theta x + \sum \Lambda (\theta \|b_n\|_{\infty})$')
    # plt.savefig(f'rate_func{-K}.pdf')
    # plt.show()
    res = minimize(objf, 1)
    return -res.fun

# Xs = np.linspace(-3, 3, 20)
#
# Is = [rate_function(x, K=-5) for x in Xs]
# plt.plot(Xs, Is)
# plt.show()



b_sums = np.sum([b*np.sqrt(2/np.pi) for b in b_norms])

samp = np.random.randn(1000000, N)
samp = np.abs(samp) - np.sqrt(2/np.pi)

def chernoff(K):
    def objf(t):
        # num = 1
        # for i, b in enumerate(b_norms):
            # num *= (2*np.exp((t*b)**2/2 - (t*b)*np.sqrt(2/np.pi))*norm.cdf((t*b)))

        sums = (b_norms*samp).sum(axis=1)
        num = np.mean(np.exp(t*sums))
        den = np.exp(t*(np.log(-K/q0) - b_sums))
        return num/den

    set_trace()
    res = minimize(objf, 1)
    return res.fun

# bound_K5_ = chernoff(-5)

K = -5
bound_K5 = np.exp(-rate_function(np.log(-K/q0) - b_sums, K))


K = -10
bound_K10 = np.exp(-rate_function(np.log(-K/q0), K) - b_sums)


K = -20
bound_K20 = np.exp(-rate_function(np.log(-K/q0), K) - b_sums)
set_trace()
