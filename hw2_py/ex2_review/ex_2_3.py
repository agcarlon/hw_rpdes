import numpy as np
# from hw_2 import eval_Q, fem_sparse
from poisson_model import eval_Q_MC, fem_sparse, karhunen_loeve, covariance_matrix
from scipy.stats import norm
from IPython.core.debugger import set_trace
from scipy.optimize import minimize
# import qmcpy as qp
from tqdm import tqdm
import matplotlib.pyplot as plt
import qmcpy as qp


I = 10
h = 1/I
N = 9
n_N = 9


err_tol = .05
alpha = 0.05
kl_contr = .95
C_alpha = norm.ppf(1 - alpha/2)

# Qs, h = eval_Q(I=I, N=N, mc_size=mc_size, nu=nu, seed=0)
#
# print((Qs<K).mean())


def sampler(n, d, seed=0):
    n_ = 2**(np.ceil(np.log2(n)))
    sobol = qp.Sobol(d, seed=seed)
    normal = qp.Gaussian(sobol, covariance=1)
    w = normal.gen_samples(n_)[:int(n)]
    return w


def get_is_mean(K=-5, nu=2.5, mu=0):
    def objf_fun(w):
        h = 1/I
        xs = np.linspace(0, 1, I+1)
        W = np.zeros((1, N))
        W[0, :n_N] = w
        cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
        coefs, N_ = karhunen_loeve(cov, W, I, 1, kl_contr=kl_contr)
        u, h = fem_sparse(I, coefs)
        Qs = h*np.sum(u, axis=1)
        objf = (Qs < K)*norm.pdf(w).prod()
        return -objf[0]

    np.random.seed(0)

    def get_starting_point(mu=0):
        mc_size = 128
        k = 0
        while True:
            # sobol = qp.Sobol(N, seed=k)
            # normal = qp.Gaussian(sobol, covariance=1)
            # w = normal.gen_samples(mc_size)
            w = sampler(mc_size, N, seed=k)
            h = 1/I
            xs = np.linspace(0, 1, I+1)
            cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
            # w = np.random.randn(mc_size, n_N) + mu
            W = np.zeros((mc_size, N))
            W[:, :n_N] = w
            coefs, N_ = karhunen_loeve(cov, W, I, mc_size, kl_contr=kl_contr)
            u, h = fem_sparse(I, coefs)
            Qs = h*np.sum(u, axis=1)
            if np.any(Qs < K):
                idx = np.argmin(Qs)
                x = W[idx, :n_N]
                break
            k += 1
        return x

    # if K == -5:
    #     mu = np.array([-0.685,  1.266, -1.345, -0.13, -1.697])
    #     return mu
    # elif K == -10:
    #     mu = np.array([-1.467, 2.712, -2.881, -0.278, -3.635])
    #     return mu
    # elif K == -20:
    #     mu = np.array([-2.248, 4.159, -4.417, -0.426, -5.574])
    #     return mu

    x0 = get_starting_point(mu=mu)
    # x0 = np.array([-0.782,  1.446, -1.536, -0.148, -1.938])
    res = minimize(objf_fun, x0, method='Nelder-Mead')
    print(f'Finding mu*: {res.success}')
    # res = minimize(objf_fun, x0)
    mu = res.x
    return mu


def eval_Q_is(mu, K, I=10, N=10, mc_size=1024, seed=0, nu=2.5):
    np.random.seed(seed)
    to_sample = np.copy(mc_size)
    mu_ = np.zeros(N)
    mu_[:n_N] = mu
    Qs = []
    stds = []
    Ms = []
    lkls = []
    # sobol = qp.Sobol(N, seed=seed)
    # normal = qp.Gaussian(sobol, covariance=1)
    # W = normal.gen_samples(mc_size)
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    sigma = 1
    while True:
        # W = np.random.randn(to_sample, N)
        W = sampler(mc_size, N, seed=0)
        W_ = W[-to_sample:]*sigma + mu_
        # u, h = fem(I, N, w, to_sample, nu)
        coefs, N_ = karhunen_loeve(cov, W_, I, to_sample, kl_contr=kl_contr)
        u, h = fem_sparse(I, coefs)
        lkl = norm.pdf(W_).prod(axis=1) / (1/sigma*norm.pdf((W_ - mu_)/sigma)).prod(axis=1)
        lkls = np.hstack([lkls, lkl])
        Qs = np.hstack([Qs, h*np.sum(u, axis=1)])
        out = (Qs < K)*lkls
        stds.append(np.std(out))
        Ms.append(mc_size)
        print(f'M: {mc_size}')
        if C_alpha*np.std(out)/np.sqrt(mc_size)/np.mean(out) < err_tol:
            print(f'M: {mc_size}')
            print(f'P: {np.mean(out)}')
            print(f'std: {stds[-1]}')
            break
        to_sample = np.copy(mc_size)
        mc_size *= 2
    # print(out.mean())
    # print(np.mean(Qs_ < K))

    # testing
    # u, h = fem_sparse(I, N, W, mc_size, nu)
    # lkl = norm.pdf(W).prod(axis=1) / norm.pdf(W - mu).prod(axis=1)
    # Qs = h*np.sum(u, axis=1)
    # out = (Qs < K)
    # print(out.mean())
    return out, np.array(stds), Ms


def adpt_qmc(mu, K, nu, h, mc_size, tol, reps):
    # C_alpha = 2.58
    I = int(1/h)
    # mc_size = 8
    # reps = 10
    to_sample = mc_size
    xs = np.linspace(0, 1, I+1)
    cov = covariance_matrix(xs[:-1] + h/2, nu=nu)
    np.random.seed(0)
    Ps = np.array([]).reshape(reps, -1)
    sigma = 1
    clt_errs = []
    stds = []
    # lkls = []
    Ps_means = []
    Ms = []
    while True:
        Ps_ = []
        for rep in range(reps):
            total_sample = sampler(mc_size, n_N, seed=rep)
            sample = total_sample[-to_sample:] + mu
            coefs, N_ = karhunen_loeve(cov, sample, I, to_sample, kl_contr=kl_contr)
            u, h_ = fem_sparse(I, coefs)
            lkl = norm.pdf(sample).prod(axis=1) / (1/sigma*norm.pdf((sample - mu)/sigma)).prod(axis=1)
            # lkls = np.hstack([lkls, lkl])
            Ps_.append((h*np.sum(u, axis=1) < K)*lkl)
        Ps = np.hstack([Ps, np.array(Ps_)])
        Ps_means.append(np.mean(Ps, axis=1))
        std = np.std(Ps_means[-1], ddof=1)
        stds.append(std)
        Ms.append(mc_size)
        clt_err = C_alpha*std/np.sqrt(reps)
        clt_errs.append(clt_err)
        if clt_err/np.mean(Ps) < tol/2:
            print(f'M: {mc_size}')
            print(f'P: {np.mean(Ps)}')
            print(f'std: {std}')
            break
        to_sample = np.copy(mc_size)
        mc_size *= 2

    Ms = np.array(Ms)
    stat_err = np.abs(np.cumsum(Ps[:, :-5].mean(axis=0))/np.arange(1, 1+mc_size-5) - np.mean(Ps))
    sample_sizes = np.arange(len(stat_err))*reps
    # fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    # ax.loglog(sample_sizes, stat_err/np.abs(np.mean(Ps)), '-', label='Error')
    # ax.loglog(reps*Ms, clt_errs / np.abs(np.mean(Ps)), 'o--', label=fr'CLT, $C_{{\alpha}}$={C_alpha}')
    # ax.set_xlabel(r'sample size')
    # ax.set_ylabel('relative statistical error')
    # ax.grid()
    # ax.legend()
    # fig.tight_layout()
    # plt.savefig(f'is_err_{nu}_K{-K}.pdf')
    return Ps, np.array(stds), Ms


def compute_is(mu, K=-5, nu=2.5, reps=10):
    # Ms = [128, 256, 512]
    # Ms = [2**8, 2**10, 2**12]
    # Ms = [2**4, 2**7, 2**10]
    # for rep in tqdm(range(reps)):
    # Qs, stds, Ms = eval_Q_is(mu, K, I=I, N=N, mc_size=128, seed=0, nu=nu)
    # Qs_, stds_, Ms_ = eval_Q_is(mu*0, K, I=I, N=N, mc_size=128, seed=0, nu=nu)
    Ps, stds, Ms = adpt_qmc(mu, K, h=.1, tol=.1, mc_size=128, nu=nu, reps=reps)
    Ps_, stds_, Ms_ = adpt_qmc(mu*0, K, h=.1, tol=.1, mc_size=128, nu=nu, reps=reps)
    Ps = np.mean(Ps, axis=0)
    Ps_ = np.mean(Ps_, axis=0)
    M = Ms[-1]
    Ms = np.array(Ms)
    Ps_av = np.cumsum(Ps) / np.arange(1, M+1)
    M_ = Ms_[-1]
    Ms_ = np.array(Ms_)
    Ps_av_ = np.cumsum(Ps_) / np.arange(1, M_+1)
    # print(np.mean(test))
    stat_err = np.abs((Ps_av[:-10] - Ps_av[-1])/Ps_av[-1])
    stat_err_ = np.abs((Ps_av_[:-10] - Ps_av[-1])/Ps_av[-1])
    clt_err_ = C_alpha*stds_/np.sqrt(reps)/np.abs(Ps_av_[-1])
    clt_err = C_alpha*stds/np.sqrt(reps)/np.abs(Ps_av[-1])
    samps_ = np.arange(len(stat_err_))*reps
    samps = np.arange(len(stat_err))*reps
    fig, ax = plt.subplots(figsize=(6, 4))
    ax.axhline(0.05, ls='--', c='k', lw=1, label=r'rel. tol of $5\%$')
    ax.loglog(samps_, stat_err_, 'C0', label='Rel. error')
    ax.loglog(samps, stat_err, 'C1', label='Rel. error with IS')
    ax.loglog(Ms_*reps, clt_err_, 'o--',c='C2',ms=3, label=fr'CLT')
    ax.loglog(Ms*reps, clt_err, 'o--', c='C3', ms=3, label=fr'CLT for IS')
    ax.grid()
    ax.legend()
    ax.set_xlabel('$M$')
    ax.set_ylabel(r'$|P_M - P|/P$')
    ax.set_title(fr'$K=${K}, $\nu=${nu}, $C_{{\alpha}}$={C_alpha:.2f}')
    fig.tight_layout()
    fig.savefig(f'stat_err_reg_is_{nu}_K{-K}.pdf')
    plt.close('all')


n_N_ = 4

nu = 2.5
mu5 = get_is_mean(K=-5, nu=nu)
mu5[n_N_:] = 0
print(mu5)

# eval_Q_is(mu5, K=-5, nu=nu)
compute_is(mu5, K=-5, nu=nu)

mu10 = get_is_mean(K=-10, mu=mu5, nu=nu)
mu10[n_N_:] = 0
print(mu10)

# eval_Q_is(mu10, K=-10)
compute_is(mu10, K=-10, nu=nu)

mu20 = get_is_mean(K=-20, mu=mu10, nu=nu)
mu20[n_N_:] = 0
print(mu20)
# eval_Q_is(mu20, K=-20)
compute_is(mu20, K=-20, nu=nu)


nu = 'inf'
print('K=-5, nu=inf')
mu5 = get_is_mean(K=-5, nu=nu)
mu5[n_N_:] = 0
print(mu5)
# eval_Q_is(mu5, K=-5)
compute_is(mu5, K=-5, nu=nu)

print('K=-10, nu=inf')
mu10 = get_is_mean(K=-10, mu=mu5, nu=nu)
mu10[n_N_:] = 0
print(mu10)
# eval_Q_is(mu10, K=-10)
compute_is(mu10, K=-10, nu=nu)

print('K=-20, nu=inf')
mu20 = get_is_mean(K=-20, mu=mu10, nu=nu)
mu20[n_N_:] = 0
print(mu20)
# eval_Q_is(mu20, K=-20)
compute_is(mu20, K=-20, nu=nu)

set_trace()
