import numpy as np
from IPython.core.debugger import set_trace
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.special import erf
import sobol_seq

M = int(1e5)


def monte_carlo(fun, xs):
    # f_x = []
    # f_x_av = []
    # for x in xs:
    # f_x.append(fun(x))
    # f_x_av.append(np.mean(f_x))
    f_x = fun(xs)
    f_x_av = np.cumsum(f_x) / np.arange(1, len(xs)+1)
    return f_x, f_x_av


def fun_1(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 9/N
    return np.cos(2*np.pi*w_1 + c_n*np.sum(x, axis=1))


def fun_2(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 7.25/N
    return np.prod((c_n**(-2) + (x - w_1)**2)**(-1), axis=1)


def fun_3(x):
    # N = len(x)
    N = np.shape(x)[1]
    c_n = 1.85/N
    return (1+c_n*np.sum(x, axis=1))**(-N+1)


def fun_4(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 7.03/N
    return np.exp(-np.sum(c_n**2*(x - w_1)**2, axis=1))


def fun_5(x):
    # N = len(x)
    N = np.shape(x)[1]
    w_1 = .5
    c_n = 2.04/N
    return np.exp(-np.sum(c_n*np.abs(x - w_1), axis=1))


def fun_6(x):
    N = np.shape(x)[1]
    c_i = 4.3/N
    w_1 = np.pi/4
    w_2 = np.pi/5
    mask = (x[:, 0] > w_1) | (x[:, 1] > w_2)
    output = np.exp(c_i*np.sum(x, axis=1))
    output[mask] = 0
    return output


xs_2 = sobol_seq.i4_sobol_generate(2, M)
xs_20 = sobol_seq.i4_sobol_generate(20, M)


def solve_prob_1_rqmc(N, reps, fun, exact, name, ex_number):
    if N == 2:
        xs_ = xs_2
    elif N == 20:
        xs_ = xs_20
    shifts = np.random.rand(reps, N)
    f_x_av = np.zeros((reps, M))
    for rep in range(reps):
        xs = np.mod(xs_ + shifts[rep], 1)
        f_x, f_x_av[rep] = monte_carlo(fun, xs)
        # f_x = fun(xs)
        # f_x_av[rep] = np.mean(f_x)

    f_x_av_av = np.cumsum(f_x_av[:, -1]) / np.arange(1, reps+1)
    stat_err = np.abs(f_x_av_av - exact)

    alpha = .01

    Ms = [10, 100, 1000, 10000]
    std_inner = []
    for M_ in Ms:
        std_inner.append(np.std(f_x_av[:, M_], ddof=1))

    z = np.polyfit(np.log(Ms), np.log(std_inner), 1)
    rate = np.maximum(z[0], -1)
    gamma = -rate - .5
    const = np.exp(z[1])
    print(f'ex. {ex_number}, N={N}: sigma=O(M^{rate:.3f}), gamma:{gamma:.3f}, const.:{const:.3f}')
    # poly = np.poly1d(z)
    # regr = np.exp(poly(np.log(Ms)))

    M1, M2 = 10, 10000
    err1 = std_inner[0]*3
    err2 = err1/(M1/M2)**rate
    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(Ms, std_inner, 'o--')
    ax.loglog([M1, M2],[err1, err2] , 'k--', label=rf'$\mathcal{{O}}(M^{{ {rate:.2f} }})$')
    ax.grid()
    ax.legend()
    ax.set_xlabel('$M$')
    ax.set_ylabel('$\sigma$')
    fig.tight_layout()
    fig.savefig(f'fun_ex{ex_number}_N{N}_rmc_2.pdf')

    # C_alpha = norm.ppf(1 - alpha/2)
    # clt_constant = C_alpha*std_inner
    #
    # err_1 = clt_constant/np.sqrt(M_1)
    # err_2 = err_1*((M_2/M_1)**(-1/2))
    #
    # fig, ax = plt.subplots(figsize=(6, 4))
    # ax.loglog(stat_err)
    # ax.loglog([M_1, M_2], [err_1, err_2], 'k-.',
    #           label=r'$C_\alpha \sigma m^{-1/2}$')
    # # ax.loglog(Ms, regr, 'r--', label='Regression')
    # ax.grid()
    # ax.set_xlabel(r'$m$')
    # ax.set_ylabel(r'$|\hat{g} - g|$')
    # ax.legend()
    # ax.set_title(f'{name}, $N=${N}, $C_{{ \\alpha }}=${C_alpha:.2f}')
    # fig.tight_layout()
    # fig.savefig(f'fun_ex{ex_number}_N{N}_rmc_inner.pdf')

    stat_err = np.abs(np.mean(f_x_av, axis=0) - exact)

    std_f_x = np.std(f_x)
    # clt_constant = norm.ppf(1 - (1-alpha)/2)*std_f_x
    C_alpha = norm.ppf(1 - alpha/2)
    clt_constant = C_alpha*const

    err_1 = clt_constant
    # err_mc = err_1*((M*reps)**(-1/2))
    err_qmc = err_1*(reps**(-1/2))*(M**rate)

    samp_size = np.arange(1, reps*M+1, reps)

    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(samp_size, stat_err)
    # ax.loglog([1, M*reps], [err_1, err_mc], 'k--',
    #           label=r'$\mathcal{O} ( (M S)^{-1/2} )$ (Crude MC)')
    ax.loglog([1, M*reps], [err_1, err_qmc], 'k-.',
              label=rf'$ C_{{\alpha}} C_{{\sigma}} S^{{-1/2}} M^{{ {rate:.2f} }}$')
    # ax.loglog(Ms, regr, 'r--', label='Regression')
    ax.grid()
    ax.set_xlabel(r'sample size')
    ax.set_ylabel(r'$|\hat{g} - g|$')
    ax.legend()
    ax.set_title(f'{name}, $N=${N}, $C_{{ \\alpha }}=${C_alpha:.2f}, $\gamma=${gamma:.2f}')
    fig.tight_layout()
    fig.savefig(f'fun_ex{ex_number}_N{N}_rmc.pdf')
    plt.close('all')

name = 'Function 1 - Oscillatory'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 9/N
    exact_f1 = np.real(np.exp(2*np.pi*1j*w_1) *
                       (1/(1j*c_n)*(np.exp(1j*c_n)-1))**N)
    solve_prob_1_rqmc(N, 50, fun_1, exact_f1, name, ex_number=1)


name = 'Function 2 - Product peak'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 7.25/N
    exact_f2 = (c_n*(np.arctan(c_n*(1-w_1)) + np.arctan(c_n*w_1)))**N
    solve_prob_1_rqmc(N, 50, fun_2, exact_f2, name, ex_number=2)

name = 'Function 3 - Corner peak'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 2.04/N
    # Obtained by QMC with M=2e6
    if N == 2:
        exact_f3 = 0.5415856698158932  # 0.5415830265945079 # M=1e9, seed ?
    elif N == 20:
        exact_f3 = 8.678102872197777e-06  # 8.686690333187793e-06
    solve_prob_1_rqmc(N, 50, fun_3, exact_f3, name, ex_number=3)

name = 'Function 4 - Gaussian'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 7.03/N
    exact_f4 = (np.sqrt(np.pi)/(2*c_n) * (erf(c_n*(1-w_1)) + erf(c_n*w_1)))**N
    solve_prob_1_rqmc(N, 50, fun_4, exact_f4, name, ex_number=4)


name = 'Function 5 - Continuous'
Ns = [2, 20]
for N in Ns:
    w_1 = .5
    c_n = 2.04/N
    exact_f5 = (1/c_n*(2 - np.exp(-c_n*w_1) - np.exp(-c_n*(1-w_1))))**N
    solve_prob_1_rqmc(N, 50, fun_5, exact_f5, name, ex_number=5)


name = 'Function 6 - Discontinuous'
Ns = [2, 20]
for N in Ns:
    w_1 = np.pi/4
    w_2 = np.pi/5
    c_n = 4.3/N
    exact_f6 = c_n**-N*(np.exp(c_n*w_1) - 1) * \
        (np.exp(c_n*w_2) - 1)*(np.exp(c_n)-1)**np.maximum(N-2, 0)
    solve_prob_1_rqmc(N, 50, fun_6, exact_f6, name, ex_number=6)


# solve_prob_1_clt(2, 100)
# solve_prob_1_clt(7, 10000)
# solve_prob_1_clt(20, 10000)
