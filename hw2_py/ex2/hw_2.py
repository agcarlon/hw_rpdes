import numpy as np
from numpy.linalg import eig, norm, det, solve
from IPython.core.debugger import set_trace
import matplotlib.pyplot as plt
import sobol_seq as ss
import qmcpy as qp
from scipy.sparse import diags as sp_diags
from scipy.sparse.linalg import spsolve
from joblib import Parallel, delayed


def force(x):
    return 4*np.pi**2*np.cos(2*np.pi*x)


def covariance_matrix(xs, nu=1.5, sigma2=2, rho=.1):
    I = len(xs)
    C = np.zeros((I, I))
    if nu == .5:
        for i in range(I):
            for j in range(I):
                C[i, j] = sigma2*np.exp(-np.abs(xs[i]-xs[j])/rho)
    elif nu == 1.5:
        for i in range(I):
            for j in range(I):
                C[i, j] = sigma2*(1 + np.sqrt(3)*np.abs(xs[i]-xs[j])/rho) * \
                    np.exp(-np.sqrt(3)*np.abs(xs[i]-xs[j])/rho)
    elif nu == 2.5:
        for i in range(I):
            for j in range(I):
                aux = np.abs(xs[i]-xs[j])/rho
                C[i, j] = sigma2*(1 + np.sqrt(5)*aux + np.sqrt(3)*aux**2) * \
                    np.exp(-np.sqrt(5)*aux)
    if nu == 'inf':
        for i in range(I):
            for j in range(I):
                C[i, j] = sigma2*np.exp(-np.abs(xs[i]-xs[j])**2/(2*rho**2))
    return C


def karhunen_loeve(C, N, w, I, mc_size):
    eig_vals, eig_vecs = eig(C)

    N = np.minimum(N, np.sum(eig_vals > 0))

    weights = eig_vals*np.max(np.abs(eig_vecs), axis=0)**2
    idxs = np.argsort(weights)[-N:]

    eig_vals = eig_vals[idxs].real
    eig_vecs_ = eig_vecs[:, idxs].real
    # set_trace()
    kappa = np.zeros((mc_size, I))
    for i in range(mc_size):
        for n in range(N):
            kappa[i, :] += eig_vals[n]**.5 * w[i, n] * eig_vecs_[:, n]
    coefs = np.exp(kappa)
    # cov = np.cov(coefs.T)
    # cov_ = np.cov(kappa.T)
    # print(w)
    # print(kappa)
    # plt.plot(eig_vecs_[:, 0])
    # plt.show()
    return coefs


def fem(I, N, w, mc_size=1, nu=1.5):
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    A = np.zeros((I+1, I+1, mc_size))
    C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    coefs = karhunen_loeve(C, N, w, I, mc_size)
    for i in range(1, I):
        A[i, i-1] = -coefs[:, i-1]/h**2
        A[i, i] = (coefs[:, i-1] + coefs[:, i])/h**2
        A[i, i+1] = -coefs[:, i]/h**2
    # Dirichlet BC
    fv = np.array([force(x) for x in xs[1:-1]])
    # Dirichlet BC
    # fv[0], fv[-1] = 0, 0
    u = np.zeros((mc_size, I+1))
    for n in range(mc_size):
        u[n, 1:-1] = solve(A[1:-1, 1:-1, n], fv)
    # u = np.vstack([solve(A[:, :, n], fv) for n in range(mc_size)])
    # plt.plot(u[0])
    # plt.show()
    # plt.plot(np.mean(u, axis=0))
    return u, h


def solve_system(coefs, h, fv):
    off = -coefs[1:-1]/h**2
    mid = np.convolve(coefs, [1, 1])[1:-1]/h**2
    A_ = sp_diags([off, mid, off], [-1, 0, 1])
    return spsolve(A_.tocsr(), fv)


def fem_sparse(I, N, w, mc_size=1, nu=1.5):
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    coefs = karhunen_loeve(C, N, w, I, mc_size)
    fv = np.array([force(x) for x in xs[1:-1]])
    u = np.zeros((mc_size, I+1))

    # Parallel
    with Parallel(n_jobs=78) as par:
        u_ = par(delayed(solve_system)(coefs_, h, fv) for coefs_ in coefs)
    u[:, 1:-1] = np.vstack(u_)

    # Non parallel
    # for n in range(mc_size):
    #     off = -coefs[n, 1:-1]/h**2
    #     mid = np.convolve(coefs[n], [1, 1])[1:-1]/h**2
    #     A_ = sp_diags([off, mid, off], [-1, 0, 1])
    #     u[n, 1:-1] = spsolve(A_.tocsr(), fv)
    return u, h


def eval_Q(I=100, N=20, mc_size=1000, seed=0, nu=1.5):
    Qs = np.zeros(mc_size)
    np.random.seed(seed)
    # for j in range(mc_size):
    #     w = sampler(N)
    #     u, h = fem(I, model_1, w)
    #     Qs[j] = h*np.sum(u)
    # w = np.random.randn(mc_size, N)
    # w = ss.i4_sobol_generate_std_normal(N, mc_size)
    sobol = qp.Sobol(N, seed=seed)
    normal = qp.Gaussian(sobol, covariance=1)
    w = normal.gen_samples(mc_size)
    # u, h = fem(I, N, w, mc_size, nu)
    u, h = fem_sparse(I, N, w, mc_size, nu)
    Qs = h*np.sum(u, axis=1)
    return Qs, h


def run_estimate(I=10, N=5, mc_size=32, nu=2.5):
    rel_err = 1.
    if nu == 2.5:
        C = 3.08
        rate = -.9
    elif nu == 'inf':
        C = 1.052
        rate = -.693
    while rel_err > .1:
        Qs, h = eval_Q(I=I, N=N, mc_size=mc_size, nu=nu)
        std_err = C*mc_size**rate
        rel_err = np.abs(std_err/np.mean(Qs))
        print(rel_err)
        if rel_err > .1:
            mc_size *= 2

# run_estimate(nu=2.5)
# run_estimate(nu='inf')

# Task 2.4.4
def plot_spectral(I, nu=.5):
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    eig_vals, eig_vecs = eig(C)
    weights = np.sqrt(eig_vals)*np.max(np.abs(eig_vecs), axis=0)
    fig, axs = plt.subplots(1, 1, figsize=(5, 5))
    axs.semilogy(weights, 'o', ms=3)
    axs.set_xlabel(r'$n$')
    axs.set_ylabel('Spectral contribution')
    axs.set_title(fr'$I={I}, \nu={nu}$')
    axs.grid()
    fig.tight_layout()
    # plt.show()
    fig.savefig(f'spectral_{nu}.pdf')

# plot_spectral(100, nu=.5)
# plot_spectral(100, nu=1.5)
# plot_spectral(100, nu=2.5)
# plot_spectral(100, nu='inf')

# nu=1.5 Linear regr, rate:2.130730106983522, const.:5.4060757881651815
def get_bias_constant_(I=2048, N=5, mc_size=1, steps=3, nu=1.5):
    np.random.seed(0)
    w = np.random.randn(mc_size, N)
    h = 1/I
    xs = np.linspace(0, 1, I+1)
    C = covariance_matrix(xs[:-1]+h/2, nu=nu)
    hs = []
    Qs = []
    coefs = karhunen_loeve(C, N, w, I, mc_size)
    # set_trace()
    for st in range(steps):
        I_ = int(I/2**st)
        h = 1/I_
        hs.append(h)
        coefs_ = coefs[:, ::st+1]
        # coefs_[:] = 1
        A = np.zeros((I_+1, I_+1, mc_size))
        xs = np.linspace(0, 1, I_+1)
        fv = np.array([force(x) for x in xs[1:-1]])
        for i in range(1, I_):
            A[i, i-1] = -coefs_[:, i-1]/h**2
            A[i, i] = (coefs_[:, i-1] + coefs_[:, i])/h**2
            A[i, i+1] = -coefs_[:, i]/h**2
        # Dirichlet BC
        u = np.zeros((mc_size, I_+1))
        for n in range(mc_size):
            u[n, 1:-1] = solve(A[1:-1, 1:-1, n], fv)
        plt.plot(u[0])
        plt.show()
        Qs.append(np.mean(h*np.sum(u, axis=1)))
        print(Qs[-1])

    bias = np.abs(Qs[1:] - Qs[0])
    z = np.polyfit(np.log(hs[1:]), np.log(bias), 1)
    rate = z[0]
    const = np.exp(z[1])
    print(f'Linear regr, rate:{rate}, const.:{const}')

    hs_ = [hs[1], hs[-1]]
    bias_reg = [const*h_**rate for h_ in hs_]
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    ax.loglog(hs[1:], bias, 'o--')
    ax.loglog(hs_, bias_reg, '-.', label='Regression')
    ax.set_xlabel('$h$')
    ax.set_ylabel('Error')
    # ax.set_title(f'$\nu=${nu}')
    ax.legend()
    ax.grid()
    fig.tight_layout()
    fig.savefig('bias.pdf')
    # plt.show()
    return u, h


def get_bias_constant():
    N = 5
    mc_size = 2**20 # 100000
    steps = 3
    hs = []
    Qs = []
    ref, h_ = eval_Q(I=1296, mc_size=mc_size, N=N, seed=0, nu=2.5)
    ref = ref.mean()
    print(ref)
    for st in range(steps):
        # u, h = fem(N*2**st, model_1, w, mc_size)
        # Q = h*np.sum(u)
        Q, h = eval_Q(I=16*3**st, mc_size=mc_size, N=N, seed=0, nu=2.5)
        hs.append(h)
        print(Q.mean())
        Qs.append(Q.mean())

    bias = np.abs(Qs - ref)
    z = np.polyfit(np.log(hs), np.log(bias), 1)
    rate = z[0]
    const = np.exp(z[1])
    print(f'Linear regr, rate:{rate}, const.:{const}')

    hs_ = [hs[0], hs[-1]]
    bias_reg = [const*h_**rate for h_ in hs_]
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    ax.loglog(hs, bias, 'o--')
    ax.loglog(hs_, bias_reg, '-.', label='Regression')
    ax.set_xlabel('$h$')
    ax.set_ylabel('Error')
    # ax.set_title(f'$\nu=${nu}')
    ax.legend()
    ax.grid()
    fig.tight_layout()
    fig.savefig('bias.pdf')
    # set_trace()

# get_bias_constant()

def richardson():
    N = 5
    I = 64
    mc_size = 2**14
    Q_h, h = eval_Q(I=I, mc_size=mc_size, N=N, seed=0)
    print(Q_h.mean())
    Q_h2, h2 = eval_Q(I=I*2, mc_size=mc_size, N=N, seed=0)
    print(Q_h2.mean())
    const = (Q_h.mean() - Q_h2.mean())/(h**2 - h2**2)
    print(const)
    set_trace()

# richardson()

def get_stat_err_const(reps=50, nu=2.5):
    Qs, hs = [], []
    Ms = [32, 64, 128, 256, 512, 1024]
    for rep in range(reps):
        Q, h = eval_Q(I=10, N=5, mc_size=1024, seed=rep, nu=nu)
        print(np.mean(Q))
        Qs.append(Q)
        hs.append(h)
    Qs = np.array(Qs)
    std_inner = []
    for M in Ms:
        Qs_ = Qs[:, :M]
        Q_ = np.mean(Qs_, axis=1)
        std_inner.append(np.std(Q_, ddof=1))

    z = np.polyfit(np.log(Ms), np.log(std_inner), 1)
    rate = np.maximum(z[0], -1)
    gamma = -rate - .5
    const = np.exp(z[1])
    print(f'Sigma, N={5}: sigma=O(M^{rate:.3f}), gamma:{gamma:.3f}, const.:{const:.3f}')
    M1, M2 = 32, 1024
    err1 = std_inner[0]*3
    err2 = err1/(M1/M2)**rate
    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(Ms, std_inner, 'o--')
    ax.loglog([M1, M2],[err1, err2] , 'k--', label=rf'$\mathcal{{O}}(M^{{ {rate:.2f} }})$')
    ax.grid()
    ax.legend()
    ax.set_xlabel('$M$')
    ax.set_ylabel('$\sigma$')
    ax.set_title(fr'$\nu=${nu}')
    fig.tight_layout()
    fig.savefig(f'stat_err_reg_{nu}.pdf')
    set_trace()


# get_stat_err_const(nu=2.5)
# get_stat_err_const(nu='inf')
# print((Qs.mean() - Q_true)/Q_true)



# def control_var():
#     N = 10
#     mc_size = 1000
#     Q_0, h_0 = eval_Q(I=int(N/2), mc_size=mc_size, N=N, seed=0)
#     Q_1, h_1 = eval_Q(I=N, mc_size=mc_size, N=N, seed=0)
#     V0 = np.var(Q_0)
#     V1 = np.var(Q_1 - Q_0)
#     C0 = h_0**-3
#     C1 = h_1**-3 + h_0**-3
#
#     # tol = .109
#     alpha = 2.20
#     # tol_s = tol * (1 - (1+2*alpha/3)**-1)
#     var_est = np.var(Q_1, ddof=1)/12
#
#     mc_orks = []
#     ml_works = []
#     tols = np.sqrt(var_est)/10**np.arange(5)
#
#     for i in range(5):
#         tol_var = np.sqrt(var_est)/10**i
#         aux = np.sqrt(V0*C0) + np.sqrt(V1*C1)
#         M0 = np.ceil(tol_var**-2*np.sqrt(V0/C0)*aux)
#         M1 = np.ceil(tol_var**-2*np.sqrt(V1/C1)*aux)
#
#         ml_work = M0*C0 + M1*C1
#         ml_works.append(ml_work)
#
#         # plain MC
#         M = np.ceil((np.std(Q_1)/tol_var)**2)
#         mc_work = M*h_1**-3
#
#         mc_works.append(mc_work)
#
#         # Sanity check
#         var_ml = V0/M0 + V1/M1
#         var_mc = np.var(Q_1)/M
#         set_trace()
#
#
#     fig, axs = plt.subplots(1, 1, figsize=(6, 4))
#     axs.loglog(tols, mc_works, '--o', label='Monte Carlo')
#     axs.loglog(tols, ml_works, '--o', label='Two level Monte Carlo')
#     axs.set_xlabel('Tolerance')
#     axs.set_ylabel('Work')
#     # axs.set_title(f'1000 Repetitions, N={N}. Success rate: {success_rate:.2f}%')
#     axs.legend()
#     axs.grid()
#     fig.tight_layout()
#     fig.savefig(f'mlmc_work_conv.pdf')
#     plt.show()
#
#     set_trace()
#     # sampling from each estimator
#     Q_MCs = []
#     Q_MLs = []
#     for seed in range(1000):
#         Q_0_, h_0 = eval_Q(sampler, model_1, I=int(N/2), mc_size=int(M0), N=N, seed=seed)
#         Q_1_p, h_1 = eval_Q(sampler, model_1, I=int(N/2), mc_size=int(M1), N=N, seed=seed)
#         Q_1_n, h_1 = eval_Q(sampler, model_1, I=N, mc_size=int(M1), N=N, seed=seed)
#
#         Q_, h_1 = eval_Q(sampler, model_1, I=N, mc_size=int(M), N=N, seed=seed)
#
#         Q_MCs.append(np.mean(Q_))
#         Q_MLs.append(np.mean(Q_0_) + np.mean(Q_1_n - Q_1_p))
#
#     Q_ref = -1.0969736694141734
#
#     set_trace()
# control_var()
