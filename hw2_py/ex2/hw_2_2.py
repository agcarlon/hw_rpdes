import numpy as np
from hw_2 import eval_Q
from IPython.core.debugger import set_trace
import matplotlib.pyplot as plt

# Estimating the variance of the difference


def get_stat_err_const(reps=50, nu=2.5):
    Qs, hs = [], []
    Ms = [32, 64, 128, 256, 512, 1024]
    for rep in range(reps):
        Q0, h0 = eval_Q(I=10, N=5, mc_size=1024, seed=rep, nu=nu)
        Q1, h1 = eval_Q(I=20, N=5, mc_size=1024, seed=rep, nu=nu)
        print(np.mean(Q1-Q0))
        Qs.append(Q1-Q0)
    Qs = np.array(Qs)
    std_inner = []
    for M in Ms:
        Qs_ = Qs[:, :M]
        Q_ = np.mean(Qs_, axis=1)
        std_inner.append(np.std(Q_, ddof=1))

    z = np.polyfit(np.log(Ms), np.log(std_inner), 1)
    rate = np.maximum(z[0], -1)
    gamma = -rate - .5
    const = np.exp(z[1])
    print(f'Sigma, N={5}: sigma=O(M^{rate:.3f}), gamma:{gamma:.3f}, const.:{const:.3f}')
    M1, M2 = 32, 1024
    err1 = std_inner[0]*3
    err2 = err1/(M1/M2)**rate
    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(Ms, std_inner, 'o--')
    ax.loglog([M1, M2],[err1, err2] , 'k--', label=rf'$\mathcal{{O}}(M^{{ {rate:.2f} }})$')
    ax.grid()
    ax.legend()
    ax.set_xlabel('$M$')
    ax.set_ylabel('$\sigma$')
    ax.set_title(fr'$\nu=${nu}')
    fig.tight_layout()
    fig.savefig(f'stat_err_reg_mlqmc_{nu}.pdf')
    set_trace()

# get_stat_err_const(nu=2.5)
# Sigma, N=5: sigma=O(M^-0.879), gamma:0.379, const.:4.914

# get_stat_err_const(nu='inf')
# Sigma, N=5: sigma=O(M^-0.726), gamma:0.226, const.:2.182

def run_tlqmc(nu=2.5):
    tol = .148
    C_ = 2.58
    if nu == 2.5:
        C0, C1, rate = 2.075, 1.702, -.899
        M0, M1 = 306, 120
    else:
        C0, C1, rate = 1.46, 2.18, -.73
        M0, M1 = 812, 527
    Q0, h0 = eval_Q(I=5, N=5, mc_size=1024, seed=0, nu=nu)
    Q0_, h0 = eval_Q(I=5, N=5, mc_size=1024, seed=1, nu=nu)
    Q1_, h1 = eval_Q(I=10, N=5, mc_size=1024, seed=1, nu=nu)

    Q0 = Q0[:M0]
    Q0_ = Q0_[:M1]
    Q1_ = Q1_[:M1]

    est = np.mean(Q0) + np.mean(Q1_ - Q0_)
    std_err2 = C_**2*(C0*M0**(2*rate) + C1*M1**(2*rate))

    print(std_err2**.5/est)
    set_trace()

# run_tlqmc(nu=2.5)
run_tlqmc(nu='inf')
