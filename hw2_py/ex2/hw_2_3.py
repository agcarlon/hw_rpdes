import numpy as np
from hw_2 import eval_Q, fem_sparse
from scipy.stats import norm
from IPython.core.debugger import set_trace
from scipy.optimize import minimize
import qmcpy as qp
from tqdm import tqdm
import matplotlib.pyplot as plt


I = 10
N = 5

# Qs, h = eval_Q(I=I, N=N, mc_size=mc_size, nu=nu, seed=0)
#
# print((Qs<K).mean())

def get_is_mean(K=-5, nu=2.5, mu=0):
    def objf_fun(w):
        u, h = fem_sparse(I, N, w[np.newaxis, :], 1, nu=nu)
        Qs = h*np.sum(u, axis=1)
        objf = (Qs < K)*norm.pdf(w).prod()
        return -objf[0]

    np.random.seed(0)
    def get_starting_point(mu=0):
        mc_size = 128
        k = 0
        while True:
            sobol = qp.Sobol(N, seed=k)
            normal = qp.Gaussian(sobol, covariance=1)
            w = normal.gen_samples(mc_size) + mu
            # u, h = fem(I, N, w, mc_size, nu)
            u, h = fem_sparse(I, N, w, mc_size, nu)
            Qs = h*np.sum(u, axis=1)
            if np.any(Qs < K):
                idx = np.argmin(Qs)
                x = w[idx]
                break
            k += 1
        return x

    # if K == -5:
    #     mu = np.array([-0.685,  1.266, -1.345, -0.13, -1.697])
    #     return mu
    # elif K == -10:
    #     mu = np.array([-1.467, 2.712, -2.881, -0.278, -3.635])
    #     return mu
    # elif K == -20:
    #     mu = np.array([-2.248, 4.159, -4.417, -0.426, -5.574])
    #     return mu

    x0 = get_starting_point(mu=mu)
    # x0 = np.array([-0.782,  1.446, -1.536, -0.148, -1.938])

    res = minimize(objf_fun, x0, method='Nelder-Mead')
    # res = minimize(objf_fun, x0)
    mu = res.x
    return mu


def eval_Q_is(mu, K, I=10, N=5, mc_size=1024, seed=0, nu=2.5):
    Qs = np.zeros(mc_size)
    np.random.seed(seed)
    sobol = qp.Sobol(N, seed=seed)
    normal = qp.Gaussian(sobol, covariance=1)
    W = normal.gen_samples(mc_size)
    W_ = W + mu
    # u, h = fem(I, N, w, mc_size, nu)
    u_, h = fem_sparse(I, N, W_, mc_size, nu)
    lkl = norm.pdf(W_).prod(axis=1) / norm.pdf(W_ - mu).prod(axis=1)
    Qs_ = h*np.sum(u_, axis=1)
    out = (Qs_ < K)*lkl
    # print(out.mean())
    # print(np.mean(Qs_ < K))

    # testing
    # u, h = fem_sparse(I, N, W, mc_size, nu)
    # lkl = norm.pdf(W).prod(axis=1) / norm.pdf(W - mu).prod(axis=1)
    # Qs = h*np.sum(u, axis=1)
    # out = (Qs < K)
    # print(out.mean())
    return out, h


def compute_is(mu, K=-5, nu=2.5, reps=50):
    tests, hs = [], []
    alpha = 0.01
    tol = 1.34e-3
    C_alpha = 2.58
    # Ms = [128, 256, 512]
    # Ms = [2**8, 2**10, 2**12]
    Ms = [2**4, 2**7, 2**10]
    for rep in tqdm(range(reps)):
        test, h = eval_Q_is(mu, K, I=10, N=5, mc_size=Ms[-1], seed=rep, nu=nu)
        # print(np.mean(test))
        tests.append(test)
        hs.append(h)
    tests = np.array(tests)
    std_inner = []
    for M in Ms:
        tests_ = tests[:, :M]
        test_ = np.mean(tests_, axis=1)
        std_inner.append(np.std(test_, ddof=1))

    z = np.polyfit(np.log(Ms), np.log(std_inner), 1)
    rate = np.maximum(z[0], -1)
    gamma = -rate - .5
    const = np.exp(z[1])
    # sigma=O(M^-0.649), gamma:0.149, const.:0.549
    # Sigma, N=5: sigma=O(M^-0.825), gamma:0.325, const.:11.457
    print(f'Sigma, N={5}: sigma=O(M^{rate:.3f}), gamma:{gamma:.3f}, const.:{const:.3f}')
    M_ = np.ceil((2*C_alpha*const/tol)**(1/.5+gamma))
    print(f'M* for tol:{tol} and Calpha:{C_alpha} is M*={M_}')
    print(f'P_M = {np.mean(tests)}')
    M1, M2 = Ms[0], Ms[-1]
    err1 = std_inner[0]*3
    err2 = err1/(M1/M2)**rate
    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(Ms, std_inner, 'o--')
    ax.loglog([M1, M2],[err1, err2] , 'k--', label=rf'$\mathcal{{O}}(M^{{ {rate:.2f} }})$')
    ax.grid()
    ax.legend()
    ax.set_xlabel('$M$')
    ax.set_ylabel('$\sigma$')
    ax.set_title(fr'$K=${K}, $\nu=${nu}')
    fig.tight_layout()
    fig.savefig(f'stat_err_reg_is_{nu}_K{-K}.pdf')

    tests_av = np.cumsum(np.mean(tests,axis=0)) / np.arange(1,Ms[-1]+1)
    stat_err = np.abs(tests_av[:-1000] - tests_av[-1])

    # clt_constant = norm.ppf(1 - (1-alpha)/2)*std_f_x
    C_alpha = norm.ppf(1 - alpha/2)
    clt_constant = C_alpha*const

    err_1 = clt_constant
    err_qmc = err_1*(reps**(-1/2))*(M**rate)

    samp_size = np.arange(1, reps*len(stat_err)+1, reps)

    fig, ax = plt.subplots(figsize=(6, 4))
    ax.loglog(samp_size, stat_err)
    ax.loglog([1, M*reps], [err_1, err_qmc], 'k-.', label=rf'$ C_{{\alpha}} C_{{\sigma}} S^{{-1/2}} M^{{ {rate:.2f} }}$')
    ax.grid()
    ax.set_xlabel(r'sample size')
    ax.set_ylabel(r'$|P_M - P|$')
    ax.legend()
    ax.set_title(f'$K=${K}, $C_{{ \\alpha }}=${C_alpha:.2f}, $\gamma=${gamma:.2f}')
    fig.tight_layout()
    fig.savefig(f'is_rmc_{nu}_K{-K}.pdf')
    plt.close('all')

nu = 2.5
mu5 = get_is_mean(K=-5, nu=nu)
print(mu5)
eval_Q_is(mu5, K=-5)
compute_is(mu5, K=-5, nu=nu)

mu10 = get_is_mean(K=-10, mu=mu5, nu=nu)
print(mu10)
eval_Q_is(mu10, K=-10)
compute_is(mu10, K=-10, nu=nu)

mu20 = get_is_mean(K=-20, mu=mu10, nu=nu)
print(mu20)
eval_Q_is(mu20, K=-20)
compute_is(mu20, K=-20, nu=nu)


nu = 'inf'
mu5 = get_is_mean(K=-5, nu=nu)
print(mu5)
eval_Q_is(mu5, K=-5)
compute_is(mu5, K=-5, nu=nu)

mu10 = get_is_mean(K=-10, mu=mu5, nu=nu)
print(mu10)
eval_Q_is(mu10, K=-10)
compute_is(mu10, K=-10, nu=nu)

mu20 = get_is_mean(K=-20, mu=mu10, nu=nu)
print(mu20)
eval_Q_is(mu20, K=-20)
compute_is(mu20, K=-20, nu=nu)

set_trace()
